# Copyright (c) 2016 Renat R. Dusaev <crank@qcrypt.org>
# Author: Renat R. Dusaev <crank@qcrypt.org>
# 
# Permission is hereby granted, free of charge, to any person obtaining a copy of
# this software and associated documentation files (the "Software"), to deal in
# the Software without restriction, including without limitation the rights to
# use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
# the Software, and to permit persons to whom the Software is furnished to do so,
# subject to the following conditions:
# 
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
# 
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
# FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
# COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
# IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
# CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

# ROOT framework is a major data analysis toolkit used in many applications
# inside of StromaV
find_package( ROOT
    COMPONENTS Dict Eve Gui Ged Geom RGL EG REQUIRED )

#
# Find the StromaV framework
find_package( StromaV REQUIRED )
message( STATUS "StromaV library found: ${StromaV_LIBRARY_DIR}" )
# Check the required StromaV options
if( NOT StromaV_RPC_PROTOCOLS )
    message( FATAL_ERROR "Required RPC_PROTOCOLS option was disabled"
        "in StromaV build. Unable to proceed." )
endif( NOT StromaV_RPC_PROTOCOLS )
if( NOT StromaV_ANALYSIS_ROUTINES )
    message( FATAL_ERROR "Required ANALYSIS_ROUTINES option was disabled"
        "in StromaV build. Unable to proceed." )
endif( NOT StromaV_ANALYSIS_ROUTINES )
if( NOT StromaV_ALIGNMENT_ROUTINES )
        message( FATAL_ERROR "Required ALIGNMENT_ROUTINES option was disabled"
        "in StromaV build. Unable to proceed." )
endif( NOT StromaV_ALIGNMENT_ROUTINES )

#
# Find the AFNA64 middleware
find_package( afNA64 REQUIRED )

#
# Find Boost
find_package( Boost ${Boost_FORCE_VERSION}
              COMPONENTS program_options
                         system
                         thread
                         unit_test_framework
                         iostreams
              REQUIRED )

#
# Find genfit (TODO: make opt?)
find_package( GenFit REQUIRED )

#
# Look for Geant4 if need by sV/afNA64:
if( StromaV_GEANT4_MC_MODEL )
    find_package( Geant4 )
endif()

