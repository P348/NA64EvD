/*
 * Copyright (c) 2016 Renat R. Dusaev <crank@qcrypt.org>
 * Author: Renat R. Dusaev <crank@qcrypt.org>
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy of
 * this software and associated documentation files (the "Software"), to deal in
 * the Software without restriction, including without limitation the rights to
 * use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
 * the Software, and to permit persons to whom the Software is furnished to do so,
 * subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
 * FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
 * COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
 * IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
 * CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

# include <goo_xform.tcc>
# include <cassert>

class TEveRGBAPalette;

namespace sV {
namespace evd {

/**@class PaletteUpdater
 * @brief Class implementing auxiliary functions for TEveRGBAPalette controls.
 *
 * Implements additional palette control: adaptive upper/lower limits changes,
 * delegation, and clamping of limits. Based on goo::aux::Range class.
 * */
class PaletteUpdater {
public:
    /// Contains a set of palette updater parameters. Used by stringified
    /// options parsing/outputting functions and PaletteUpdater constructor.
    struct Parameters {
        TEveRGBAPalette * palettePtr;
        char isSoft,        ///< -1 - uninit, 0 - strict, 1 - soft
             isDynamic;     ///< -1 - uninit, 0 - fixed, 1 - dynamic
        float rigidness;    ///< <0 - uninit, > 0 && < 1 -- ok 
        bool ownsPalette;   ///< Should be palette deleted by ~PaletteUpdater()?
        float lowerLimit,
              upperLimit;
        Parameters();

        /// Returns true if parsing succeed and parameters seems to be correct.
        /// Does not expect information about palette ownership.
        static bool parse_optstring( const std::string &, Parameters & );
        /// Returns whether the parameter set is complete and valid.
        static bool is_valid( const Parameters & );
        /// Produces the valid string description of provided parameters.
        /// Does not provide information about palette ownership.
        static std::string to_string( const Parameters & );
        // TODO: check that this ^^^ generates a valid output.
    };
private:
    /// Ptr to palette instance (association).
    TEveRGBAPalette * _palettePtr;
    /// Wether limits updating by update() method is disabled.
    bool _limitsLocked;
    /// Ptr to goo's range instance.
    ::goo::aux::RigidRange<double> * _rangePtr;
    /// Whether the range instance "rigid" or not.
    bool _isRangeRigid,
         _doDeletePaletteOnDtr;
public:
    /// Dft ctr; palette must be set further.
    PaletteUpdater( double rigidness=.8 );
    /// Ctr for provided palette updater instance.
    PaletteUpdater( TEveRGBAPalette *, double rigidness=.8 );
    /// Ctr uses the parameters set struct.
    PaletteUpdater( const Parameters & );
    /// Dft dtr.
    ~PaletteUpdater();

    /// Locks palette --- prevents limits changing via update().
    void lock_palette() { _limitsLocked = true; }
    /// Returns wether palette updating is locked.
    bool is_locked() const { return _limitsLocked; }
    /// Unlocks palette --- limits now can be changed via update().
    void unlock_palette() { _limitsLocked = false; }

    /// Palette instance getter (mutable).
    virtual TEveRGBAPalette & palette()
        { assert( _palettePtr ); return *_palettePtr; }
    /// Palette instance getter (constant).
    virtual const TEveRGBAPalette & palette() const
        { assert( _palettePtr ); return *_palettePtr; }
    /// Palette instance setter.
    virtual void palette( TEveRGBAPalette * );

    /// Takes the value into consideration.
    virtual bool extend_to( double val );

    /// Makes a "rigid" range instance.
    void set_range_rigid( double threshold=0.8 );

    /// Returns true if range is "rigid".
    bool range_is_rigid() const { return _isRangeRigid; }

    void enable_average_all() { _rangePtr->do_consider_all(true); }
    void disable_average_all() { _rangePtr->do_consider_all(false); }
};  // class PaletteUpdater

}  // namespace evd
}  // namespace sV

