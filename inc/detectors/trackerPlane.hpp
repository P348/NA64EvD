/*
 * Copyright (c) 2016 Renat R. Dusaev <crank@qcrypt.org>
 * Author: Renat R. Dusaev <crank@qcrypt.org>
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy of
 * this software and associated documentation files (the "Software"), to deal in
 * the Software without restriction, including without limitation the rights to
 * use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
 * the Software, and to permit persons to whom the Software is furnished to do so,
 * subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
 * FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
 * COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
 * IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
 * CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

# ifndef H_STROMA_V_EVENT_DISPLAY_MICROMEGA_H
# define H_STROMA_V_EVENT_DISPLAY_MICROMEGA_H

# if 0

# include "iCoordPlanar.hpp"
# include <StromaV/alignment/CompoundDetector.hpp>

namespace sV {
namespace evd {

class PaletteUpdater;

/**@class TrackerPlane
 * @brief Primitive tracker plane representation.
 *
 * Represents detectors placed orthogonaly to incident particles
 * expected tracks (GEMs, Micromegas). Drawn as rectangular
 * region. All the tracking and visualization things are implemented
 * by parent sV::evd::iCoordinatePlane class.
 *
 * @ingroup evd
 */
class TrackerPlane : public iCoordinatePlane,
                     public alignment::CompoundDetector {
    static const std::map<const std::string, std::array<float, 2> > _dtrSizeConst;
    static TEvePointSet * trackPointSet;
private:
    /// Cache variable, used to iterate over points;
    uint32_t _lastPointIdx;
protected:
    uint32_t get_last_point_no() const { return _lastPointIdx; }
    bool _V_next_point( const Hit &, Point & ) override;

    virtual bool _V_reset_hits() override;
public:
    TrackerPlane( const std::string & ctrName,
                  const std::string & detectorName,
                  PaletteUpdater * palettePtr=&(goo::app<evd::Application>().common_APV_palette_updater()) );
};  // class TrackerPlane

}  // namespace evd
}  // namespace sV

# endif

# endif  // H_STROMA_V_EVENT_DISPLAY_MICROMEGA_H

