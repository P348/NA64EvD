/*
 * Copyright (c) 2016 Renat R. Dusaev <crank@qcrypt.org>
 * Author: Renat R. Dusaev <crank@qcrypt.org>
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy of
 * this software and associated documentation files (the "Software"), to deal in
 * the Software without restriction, including without limitation the rights to
 * use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
 * the Software, and to permit persons to whom the Software is furnished to do so,
 * subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
 * FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
 * COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
 * IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
 * CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

# ifndef H_STROMA_V_EVENT_DISPLAY_COORDINATE_PLANE_H
# define H_STROMA_V_EVENT_DISPLAY_COORDINATE_PLANE_H

# if 0
/**@file ed_iCoordPlanar.hpp
 * @brief Header file for iCoordinatePlane class.
 * @ingroup eventDisplay
 *
 * Describes coordinate plane class interface class which depends on
 * alignment routines.
 * */

# include <list>
# include <StromaV/alignment/DrawableDetector.hpp>
# include <StromaV/alignment/ReceptiveDetector.hpp>

class TEveLine;

namespace sV {
namespace evd {

/**@brief Simple interface class for representing coordinate plane
 * tracking detectors like GEMs, Micromegas, etc.
 * @ingroup evd
 *
 * Any particular detector family should implement its own particular
 * subclass with common static point set.
 *
 * TODO: for persistency display think about drawing colored histogram
 * plot just in the scene:
 *      https://root.cern.ch/doc/master/classTH2GL.html
 */
class iCoordinatePlane : public alignment::DrawableDetector,
                         public alignment::ReceptiveDetector,
                         public alignment::TrackReceptiveVolume<2> {
public:
    /// Shortcut for local coordinates type.
    typedef alignment::TrackReceptiveVolume<2>::LocalCoordinates Point;
private:
    /// For iCoordinatePlane only bounding line is drawn.
    TEveLine * _boundingLine;
    /// When true, the hits x,y coordinates supposed to be in range [0..1].
    bool _isHitNormed;
protected:
    /// Draws a rectangular frame.
    virtual void _V_draw_detector( DrawableDetector::Container * sc ) override;
    /// Returns true if hit was change by the _V_treat_new_hit().
    virtual bool _V_draw_hit() override;
    /// Considers new hit data.
    virtual bool _V_treat_new_hit( const Hit & ) override;
    /// Should be called at the end of each event treatment.
    virtual bool _V_reset_hits() override;
    /// Should set next point extracted from hit, or return false
    /// if no meaningful data were found in hit.
    virtual bool _V_next_point( const Hit &, Point & ) = 0;
    /// Returns bounding box from border line.
    virtual const TAttBBox * _V_bbox() const override;
public:
    /// Constructor for specific-named detector.
    iCoordinatePlane( const std::string & ctrName,
                      const std::string & detectorName,
                      const float rectangle[2],
                      bool isHitNormed ) :
            iDetector( ctrName, detectorName, true, true, false ),
            DrawableDetector( ctrName, detectorName ),
            ReceptiveDetector( ctrName, detectorName ),
            _isHitNormed(isHitNormed) {
        const float mins[2] = {-rectangle[0]/2, -rectangle[1]/2},
                    maxs[2] = { rectangle[0]/2,  rectangle[1]/2}
                    ;
        set_ranges( mins, maxs );
        recalculate_scales();
    }
    /// Does nothing.
    ~iCoordinatePlane() {}

    /// Shortcut for XForm first dimension getter.
    float width() const
        { return get_limit(XForm<2, float>::max, 0) - get_limit(XForm<2, float>::min, 0); }

    /// Shortcut for XForm first dimension getter.
    float height() const
        { return get_limit(XForm<2, float>::max, 1) - get_limit(XForm<2, float>::min, 1); }
};  // class iCoordinatePlane

}  // namespace evd
}  // namespace sV

# endif
# endif  // H_STROMA_V_EVENT_DISPLAY_COORDINATE_PLANE_H

