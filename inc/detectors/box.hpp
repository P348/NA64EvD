/*
 * Copyright (c) 2016 Renat R. Dusaev <crank@qcrypt.org>
 * Author: Renat R. Dusaev <crank@qcrypt.org>
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy of
 * this software and associated documentation files (the "Software"), to deal in
 * the Software without restriction, including without limitation the rights to
 * use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
 * the Software, and to permit persons to whom the Software is furnished to do so,
 * subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
 * FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
 * COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
 * IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
 * CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

# ifndef H_STROMA_V_EVENT_DISPLAY_SINGLE_BOX_H
# define H_STROMA_V_EVENT_DISPLAY_SINGLE_BOX_H

# if 0
# include <StromaV/alignment/DrawableDetector.hpp>
# include <StromaV/alignment/ReceptiveDetector.hpp>

class TEveBox;

namespace sV {
namespace evd {

class Box : public alignment::DrawableDetector,
            public alignment::ReceptiveDetector  {
protected:
    virtual void _V_draw_detector( DrawableDetector::Container * ) override;
    virtual bool _V_draw_hit() override;
    virtual bool _V_treat_new_hit( const Hit & ) override;
    virtual bool _V_reset_hits() override;
private:
    TEveBox * _box;
public:
    Box( const std::string & ctrName, const std::string & detectorName ) :
                alignment::iDetector( ctrName, detectorName, true, true, false ),
                DrawableDetector( ctrName, detectorName ),
                ReceptiveDetector( ctrName, detectorName ) {}
    ~Box() {}
};  // class Box

}  // namespace evd
}  // namespace sV
# endif

# endif  // H_STROMA_V_EVENT_DISPLAY_SINGLE_BOX_H

