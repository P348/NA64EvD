/*
 * Copyright (c) 2016 Renat R. Dusaev <crank@qcrypt.org>
 * Author: Renat R. Dusaev <crank@qcrypt.org>
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy of
 * this software and associated documentation files (the "Software"), to deal in
 * the Software without restriction, including without limitation the rights to
 * use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
 * the Software, and to permit persons to whom the Software is furnished to do so,
 * subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
 * FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
 * COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
 * IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
 * CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

# ifndef H_STROMA_V_EVENT_DISPLAY_ECAL_H
# define H_STROMA_V_EVENT_DISPLAY_ECAL_H

# include <StromaV/alignment/DrawableDetector.hpp>
# include <StromaV/alignment/ReceptiveDetector.hpp>
# include <StromaV/alignment/CompoundDetector.hpp>
# include <StromaV/alignment/TrackingVolume.tcc>
# include <na64_event.pb.h>

# include <TEveDigitSet.h>

# include <unordered_map>
# include <list>

class TEveBoxSet;
class TEveFrameBox;
class TEveRGBAPalette;

namespace sV {
namespace evd {

/**@class ECAL
 * @brief Electromagnetic calorimeter representation for event display.
 *
 * Electromagnetic calorimeter is represented as drawable compound detector,
 * receptive to hits and providing some tracking information. It consists of
 * preshower and main part which are square boxes with transversal 6×6
 * segmentation.
 *
 * @ingroup evd, detectors
 */
class ECAL : public alignment::DrawableDetector,
             public alignment::ReceptiveAssembly<na64::events::Measurement>,
             public alignment::CompoundDetector,
             public alignment::TrackReceptiveVolume<3> {
public:
    typedef alignment::ReceptiveAssembly<na64::events::Measurement> Parent;

    class ECAL_Cell : public Parent::ReceptivePart {
    protected:
        /// Ptr to digit appropriate to particular cell.
        TEveDigitSet::DigitBase_t * _digitPtr;
        /// Ptr to digit's amplitude value instance.
        float * _cAmplitudePtr;
        /// Origins (center) of the cell in local frame.
        float _originX, _originY;
    protected:
        virtual bool _V_recache_summary(
                    const ::sV::events::DetectorSummary & summary_ ) override;
    public:
        ECAL_Cell( const std::string & ctrName,
                   const std::string & detectorName,
                   TEveDigitSet::DigitBase_t * digitPtr,
                   float * cAmplitudePtr,
                   float originX, float originY );  // TODO!
        /// Returns true, if there was non-zero digits.
        bool reset() {
            *_cAmplitudePtr = 0.;
            if( _digitPtr->fValue ) {
                _digitPtr->fValue = 0;
                return true;
            }
            return false;
        }
    };

    /// Cache of single hit with per-event scope.
    struct HitCache {
        float amplitudes[6][6];
    };


    static constexpr size_t nCellsX = 6,
                            nCellsY = 6,
                            nCellsPreshower = 16,
                            nCellsECAL = 150;
    static constexpr double cellWidth_mm = 38.2,
                            cellHeight_mm = 38.2,
                            ScnPlDepth_mm = 1.55,
                            CnvPlDepth_mm = 1.5,
                            PaperDepth_mm = .14,
                            cellDepth_mm = ScnPlDepth_mm + CnvPlDepth_mm + PaperDepth_mm,
                            width = nCellsX*cellWidth_mm,
                            height = nCellsY*cellHeight_mm,
                            depthPreshower = nCellsPreshower*cellDepth_mm,
                            depthECAL = nCellsECAL*cellDepth_mm,
                            fullDepth_mm = depthPreshower + depthECAL;
private:
    /// TEve box sets.
    TEveBoxSet * _boxSetPreshower,
               * _boxSetECAL;
    /// PMT addressing map cache.
    //XXX:std::unordered_map<AFR_DetSignature, DigitCache> _pmtMap;
    /// Hit cache instances.
    HitCache _cHitCachePreshower,
             _cHitCacheECAL;
    /// Cache of previous hits for averaged values calculation.
    std::list<HitCache> _prevHitsPreshower,
                        _prevHitsECAL;
    /// Per-event persistency logical caches.
    bool _hasHitInPreshower,
         _hasHitInMainPart;
    /// Per-event persistency logical caches.
    float _otherPartXCache,
          _otherPartYCache,
          _otherPartEDep,
          _lastHitCachedZ;
protected:
    /// Private procedure drawing preshower part.
    TEveElement * _draw_preshower(
                          double width,
                          double height,
                          double depth,
                          double cellSideX,
                          double cellSideY );
    /// Private procedure drawing main part.
    TEveElement * _draw_ECAL(
                     double width,
                     double height,
                     double depth,
                     double cellSideX,
                     double cellSideY );
    /// Draws entire detector (drawable interface implem).
    virtual void _V_draw_detector( DrawableDetector::Container * ) override;
    /// Draws hit (receptive interface implem).
    virtual bool _V_draw_hit() override;
    /// Sets up caches according to the hit information provided (receptive interface implem).
    //virtual bool _V_treat_new_hit( const ReceptiveDetector::Hit & ) override;
    /// Resets caches at the end of event treatment (receptive interface implem).
    virtual bool _V_reset_summary() override;
    /// Private procedure caching hit.
    virtual float _cache_current_hit( bool doAveraging,
                                      HitCache & cHitCache,
                                      std::list<HitCache> & caches );
    /// Updates track point (for track receptive interface).
    virtual void _update_point_in( bool isInPreshower, float x, float y, float edep );

    /// Does update of track point at the end of event treatment.
    virtual void _V_hit_setting_finale();
public:
    /// Std ctr.
    ECAL( const std::string & ctrName, const std::string & detectorName ) :
                iDetector( ctrName, detectorName, true, true, true ),
                //alignment::ReceptiveAssembly<na64::events::Measurement>( ctrName, detectorName ),
                DrawableDetector( ctrName, detectorName ),
                //ReceptiveDetector( ctrName, detectorName ),
                ReceptiveAssembly<na64::events::Measurement>(ctrName, detectorName),
                CompoundDetector( ctrName, detectorName, { "ECAL0", "ECAL1", "ECALSUM" } ),
                _boxSetPreshower(nullptr),
                _boxSetECAL(nullptr),
                _hasHitInPreshower(false),
                _hasHitInMainPart(false),
                _otherPartXCache(std::numeric_limits<float>::quiet_NaN()),
                _otherPartYCache(std::numeric_limits<float>::quiet_NaN()),
                _otherPartEDep(  std::numeric_limits<float>::quiet_NaN()) {
        // Pre-init caches:
        bzero( &_cHitCacheECAL, sizeof(_cHitCacheECAL) );
        bzero( &_cHitCachePreshower, sizeof(_cHitCachePreshower) );
        // initialize XForm. Note that we consider only half of the length
        // along Z coordinate for each part:
        const float mins[3] = {-width/2, -height/2, -depthPreshower/2},
                    maxs[3] = { width/2,  height/2,       depthECAL/2}
                    ;
        set_ranges( mins, maxs );
        recalculate_scales();
    }
    /// Does nothing unusual.
    ~ECAL() {}
};  // class ECAL

}  // namespace evd
}  // namespace sV

# endif  // H_STROMA_V_EVENT_DISPLAY_ECAL_H

