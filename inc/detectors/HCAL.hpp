/*
 * Copyright (c) 2016 Renat R. Dusaev <crank@qcrypt.org>
 * Author: Renat R. Dusaev <crank@qcrypt.org>
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy of
 * this software and associated documentation files (the "Software"), to deal in
 * the Software without restriction, including without limitation the rights to
 * use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
 * the Software, and to permit persons to whom the Software is furnished to do so,
 * subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
 * FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
 * COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
 * IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
 * CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

# ifndef H_STROMA_V_EVENT_DISPLAY_HCAL_H
# define H_STROMA_V_EVENT_DISPLAY_HCAL_H

# if 0
# include <StromaV/alignment/DrawableDetector.hpp>
# include <StromaV/alignment/ReceptiveDetector.hpp>
# include <afNA64/na64_uevent.hpp>

# include <unordered_map>
# include <list>

# include <TEveDigitSet.h>

class TEveBoxSet;
class TEveFrameBox;
class TEveRGBAPalette;
class TEvePointSet;

namespace sV {
namespace evd {

class HCAL : public alignment::DrawableDetector,
             public alignment::CachedPayloadReceptiveDetector<na64::events::Measurement> {
public:
    typedef alignment::CachedPayloadReceptiveDetector<na64::events::Measurement>
            Parent;

    class HCAL_Cell : public Parent::ReceptivePart {
    private:
        TEveDigitSet::DigitBase_t * digitPtr;
        float * cAmplitudePtr;
        float originX, originY;
    };

    struct HitCache {
        float mwX, mwY;
        float amplitudes[3][3];
    };
    struct PointDigitCache {
        TEveDigitSet::DigitBase_t * digitPtr;
    };
private:
    double _cst_cellSizeX_mm,
           _cst_cellSizeY_mm,
           _cst_width,
           _cst_height,
           _cst_depth
           ;

    TEveBoxSet * _boxSetHCAL;
    TEvePointSet * _pointSetHCAL;
    std::unordered_map<DetectorMinor, DigitCache> _pmtMap;
    std::list<PointDigitCache> _pointsCache;

    HitCache _cHitCache;
    std::list<HitCache> _prevHits;
protected:
    virtual void _V_draw_detector( DrawableDetector::Container * ) override;
    virtual bool _V_draw_hit() override;
    virtual bool _V_recache_summary( const PackedSummary & ) override;
    virtual bool _V_reset_summary() override;
    /// After this method invokation, _cHitCache will contain averaged value.
    /// Returns max among averaged.
    virtual float _cache_current_hit( bool doAveraging );
public:
    HCAL( const std::string & ctrName, const std::string & detectorName ) :
                iDetector( ctrName, detectorName, true, true, false ),
                DrawableDetector( ctrName, detectorName ),
                ReceptiveDetector( ctrName, detectorName ),
                _boxSetHCAL(nullptr)
                { bzero( &_cHitCache, sizeof(_cHitCache) ); }
    ~HCAL() {}
};  // class HCAL

}  // namespace evd
}  // namespace sV
# endif

# endif  // H_STROMA_V_EVENT_DISPLAY_ECAL_H

