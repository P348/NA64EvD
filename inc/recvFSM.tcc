/*
 * Copyright (c) 2016 Renat R. Dusaev <crank@qcrypt.org>
 * Author: Renat R. Dusaev <crank@qcrypt.org>
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy of
 * this software and associated documentation files (the "Software"), to deal in
 * the Software without restriction, including without limitation the rights to
 * use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
 * the Software, and to permit persons to whom the Software is furnished to do so,
 * subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
 * FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
 * COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
 * IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
 * CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

# ifndef H_STROMA_V_AUX_CONSERVATIVE_RECEIVER_MIXIN_H
# define H_STROMA_V_AUX_CONSERVATIVE_RECEIVER_MIXIN_H

# include <cstdio>
# include <iostream>
# include <boost/statechart/event.hpp>
# include <boost/statechart/asynchronous_state_machine.hpp>
# include <boost/statechart/custom_reaction.hpp>
# include <boost/statechart/state.hpp>
# include <boost/bind.hpp>
# include <boost/thread.hpp>
# include <boost/mpl/list.hpp>
# include <boost/statechart/transition.hpp>
# include <boost/statechart/exception_translator.hpp>

# include <goo_exception.hpp>

namespace sc = boost::statechart;
namespace mpl = boost::mpl;

namespace sV {
namespace aux {
namespace fsm {

/**@class ReceiverFSM
 * @brief Class representing receiving FSM for multicast client applications.
 * 
 * Conservative receiver interface implements transition logic for
 * connection-driven applications for asynchroneous fetch of data.
 */
template<typename ConnectionPayloadT>
class ReceiverFSM {
public:
    typedef ConnectionPayloadT ConnectionPayload;
    typedef ReceiverFSM<ConnectionPayload> Self;
public:
    // Machine
    struct MachineConnection;
    // States
    struct StateActive;
    struct StateIdle;
    struct StateListening;
    struct StatePlaying;
    struct StateHold;
    // Events:
    struct EvConnect    : sc::event<EvConnect> {
        ConnectionPayload payload;
        EvConnect(ConnectionPayload cP) : payload(cP) {}
    };
    struct EvPause      : sc::event<EvPause> {};
    struct EvPlay       : sc::event<EvPause> {};
    struct EvDisconnect : sc::event<EvDisconnect> {};
    // FSM
    /////
    struct MachineConnection : public sc::asynchronous_state_machine<MachineConnection, StateActive > {
        ReceiverFSM & receiver;
        MachineConnection( typename MachineConnection::my_context ctx,
                           ReceiverFSM * recvrPtr );
        ~MachineConnection() {}
    };
    // Major state
    struct StateActive : public sc::state<StateActive, MachineConnection, mpl::list<StateIdle, StatePlaying> > {
        StateActive( typename StateActive::my_context ctx ) : StateActive::my_base(ctx) {}
    };
    // States implementation
    ///////////////////////
    struct StateIdle : public sc::state<StateIdle, typename StateActive::template orthogonal<0> > {
        typedef mpl::list< sc::custom_reaction<EvConnect>,
                           sc::custom_reaction<sc::exception_thrown> > reactions;

        StateIdle( typename StateIdle::my_context ctx ) : StateIdle::my_base(ctx) {
            this->outermost_context().receiver._V_on_idle(); }

        sc::result react( const EvConnect & evConnect ) {
            this->outermost_context().receiver._V_resolve_connection_payload( evConnect.payload );
            return this->template transit<StateListening>();
        }

        sc::result react( const sc::exception_thrown & ) {
            try {
                throw;  // re-throw exception.
            } catch( const goo::Exception & e ) {
                this->outermost_context().receiver._V_on_malfunction( e );
                return this->template transit<StateIdle>();
            } catch( ... ) {
                return this->template forward_event();  // fwd exception to outern context
            }
        }
    };
    struct StateListening : public sc::state<StateListening, typename StateActive::template orthogonal<0> > {
        typedef mpl::list<
            sc::custom_reaction<sc::exception_thrown>,
            sc::transition<EvDisconnect,    StateIdle> > reactions;

        StateListening( typename StateListening::my_context ctx ) : StateListening::my_base(ctx) {
            this->outermost_context().receiver._V_on_listening(); }

        sc::result react( const sc::exception_thrown & ) {
            try {
                throw;
            } catch( const goo::Exception & e ) {
                this->outermost_context().receiver._V_on_malfunction( e );
                return this->template transit<StateIdle>();
            } catch( ... ) {
                return this->template forward_event();  // fwd exception to outern context
            }
        }
    };  // struct StateListening
    struct StateHold : public sc::state<StateHold, typename StateActive::template orthogonal<1> > {
        typedef mpl::list<
            sc::transition<EvPlay,          StatePlaying> > reactions;

        StateHold( typename StateHold::my_context ctx ) : StateHold::my_base(ctx) {
            this->outermost_context().receiver._V_on_play(); }
    };  // struct StateHold
    struct StatePlaying : public sc::state<StatePlaying, typename StateActive::template orthogonal<1> > {
        typedef mpl::list<
            sc::transition<EvPause,        StateHold> > reactions;

        StatePlaying( typename StatePlaying::my_context ctx ) : StatePlaying::my_base(ctx) {
            this->outermost_context().receiver._V_on_pause(); }
    };  // struct StatePlaying
protected:
    //const MachineConnection & _machineCRef;
    sc::fifo_scheduler<> _scheduler;
    sc::fifo_scheduler<>::processor_handle _pHandle;
    /// Should be started outside of class
    boost::thread * _smThread;
protected:
    virtual void _V_on_idle() = 0 ;
    virtual void _V_on_listening() = 0 ;
    virtual void _V_on_pause() = 0 ;
    virtual void _V_on_play() = 0;
    virtual void _V_resolve_connection_payload( const ConnectionPayload & ) = 0;
    virtual void _V_on_malfunction( const goo::Exception & details ) = 0 ;
public:
    /// Initiates scheduler and gets ready to connection.
    ReceiverFSM( bool initiateImmediately=true ) :
            _scheduler(true),
            _pHandle(_scheduler.create_processor<MachineConnection>(this)),
            _smThread(new boost::thread( boost::bind( &sc::fifo_scheduler<>::operator(),
                                            &_scheduler, 0 ))) {
        if( initiateImmediately ) {
            initiate_fsm();
        }
    }

    void initiate_fsm() {
        _scheduler.initiate_processor( _pHandle );
    }

    /// Terminates listening thread (may take a time --- todo?).
    ~ReceiverFSM() {
        _scheduler.terminate();
        if( _smThread ) {
            _smThread->join();
        }
        delete _smThread;
    }
};  // class ReceiverFSM

template<typename ConnectionPayloadT>
ReceiverFSM<ConnectionPayloadT>::MachineConnection::MachineConnection( typename MachineConnection::my_context ctx,
                                                                       ReceiverFSM * recvrPtr ) :
            MachineConnection::my_base(ctx),
            receiver(*recvrPtr) {
    //sc::state_machine<sV::aux::fsm::ReceiverFSM<ConnectionPayloadT>::MachineConnection,
    //                  sV::aux::fsm::ReceiverFSM<ConnectionPayloadT>::StateIdle,
    //                  std::allocator<void>,
    //                  boost::statechart::null_exception_translator>::initiate();
}

}  // namespace fsm
}  // namespace aux
}  // namespace sV

# endif   // H_STROMA_V_AUX_CONSERVATIVE_RECEIVER_MIXIN_H

