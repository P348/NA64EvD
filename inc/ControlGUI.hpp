/*
 * Copyright (c) 2016 Renat R. Dusaev <crank@qcrypt.org>
 * Author: Renat R. Dusaev <crank@qcrypt.org>
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy of
 * this software and associated documentation files (the "Software"), to deal in
 * the Software without restriction, including without limitation the rights to
 * use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
 * the Software, and to permit persons to whom the Software is furnished to do so,
 * subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
 * FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
 * COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
 * IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
 * CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

# ifndef H_STROMA_V_EVENT_DISPLAY_CONTROL_GUI_SHUTTER_H
# define H_STROMA_V_EVENT_DISPLAY_CONTROL_GUI_SHUTTER_H

# include <TQObject.h>
# include <RQ_OBJECT.h>

class TGFrame;
class TGMainFrame;
class TGGroupFrame;
class TGHorizontalFrame;
class TGVerticalFrame;
class TGCheckButton;
class TGTextButton;
class TGNumberEntry;
class TGPictureButton;
class TGLabel;
class TGTextEntry;
class TGHProgressBar;
class TTimer;
class TGPicture;

class ControlGUI : public TQObject {
    RQ_OBJECT("ControlGUI")
private:
    TGVerticalFrame * _frmVWrapperPtr,
                    * _frmVDisplayControlPtr,
                    * _frmVConnectionControlPtr,
                    * _frmVPlayback
                    ;
    TGGroupFrame * _gfrmDisplayControlPtr,
                 * _gfrmPlaybackControlPtr,
                 * _gfrmConnectionControlPtr,
                 * _gfrmHBufferOccupancy
                 ;

    TGHorizontalFrame * _frmHPlaybackButtons,
                      * _frmHPersistency,
                      * _frmHMinimalDelay,
                      * _frmHTHostPtr,
                      * _frmHMulticastPtr,
                      * _frmHPortPtr,
                      * _frmHConnect,
                      * _frmHBufferOccupancy
                      ;

    TGCheckButton * _cbtnEnablePersistancyPtr,
                  * _cbtnDoDrawAveragedPtr,
                  * _cbtnDoDrawTracksPtr
                  ;

    TGNumberEntry * _nfldPersistency,
                  * _nfldMinDelay,
                  * _nfldPort
                  ;

    TGPictureButton * _btnPrevious,
                    * _btnPlayPause,
                    * _btnNext
                    ;
    TGLabel * _lblDelayPtr,
            * _lblHostPtr,
            * _lblMCastPtr,
            * _lblPortPtr,
            * _lblConnectionStatus
            ;
    TGTextEntry * _inpHostAddrPtr,
                * _inpMCastAddrPtr
                ;
    TGTextButton * _btnConnect
                 ;
    TGHProgressBar * _pbarBufferOccupancyPtr;

    TTimer * _updatingTimer;

    const TGPicture * _picPlayPause[2];
private:
    void _layout_all( TGMainFrame * frm );
    void _layout_event_display_widgets();
    void _layout_playback_widgets();
    void _layout_connection_widgets();
public:
    ControlGUI( TGMainFrame * frm );
    ~ControlGUI();

    /// Does not generate any event.
    void set_buffer_capacity_range( size_t n );

    /// Only appearance. Does not changes any underline state variables.
    void set_initial_state(
            size_t bufferOccupancy=0,
            bool persistencyOn=false,
            unsigned short nPersistency=0,
            bool doDrawAveraged=false,
            bool doDrawTracks=true,
            unsigned short minDelayNSec=1,
            std::string defaultHost="0.0.0.0",
            int portNo=30001,
            std::string mCastAddr="239.255.0.1"
        );

    /// Starts periodical update according to event queue.
    void start_periodical_update( Long_t mSecDelay=0 );

    /// Stops periodical update timer.
    void stop_periodical_update();

    Long_t min_delay_msec();

    // Getters
    /////////

    std::string connection_host_address() const;
    std::string connection_multicast_address() const;
    int connection_port_no() const;
    bool do_draw_averaged() const;
    size_t persistency() const;

    // Drivers
    /////////

    void set_status_label( const char *, unsigned char );
    void set_buffer_occupancy( size_t n );

    void playback_control_enable();
    void playback_control_disable();
    void playback_previous_enable();
    void playback_previous_disable();
    void playback_play_button_set_picture_pause();
    void playback_play_button_set_picture_play();
    void playback_next_enable();
    void playback_next_disable();

    void connection_control_enable();
    void connection_control_disable();
    void connection_control_settings_enable();
    void connection_control_settings_disable();
    void connection_control_set_status_label( const char * );
    void connection_control_set_connect_button_label( const char * );

    // Reactions
    ///////////

    void toggle_connection();
    void update_displaying_events();
    void toggle_play_pause();

    /// Changes persistency settings.
    void update_persistency_settings( Long_t nEvents );

    /// Changes timing settings of periodical update.
    void change_periodical_update_timing( Long_t mSecDelay );
};

# endif  /* H_STROMA_V_EVENT_DISPLAY_CONTROL_GUI_SHUTTER_H */

