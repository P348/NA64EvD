/*
 * Copyright (c) 2016 Renat R. Dusaev <crank@qcrypt.org>
 * Author: Renat R. Dusaev <crank@qcrypt.org>
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy of
 * this software and associated documentation files (the "Software"), to deal in
 * the Software without restriction, including without limitation the rights to
 * use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
 * the Software, and to permit persons to whom the Software is furnished to do so,
 * subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
 * FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
 * COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
 * IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
 * CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

# ifndef H_STROMA_V_EVENT_DISPLAY_DRAWABLE_POINT_SET_H
# define H_STROMA_V_EVENT_DISPLAY_DRAWABLE_POINT_SET_H

# include <StromaV/alignment/TrackingGroup.hpp>
# include <StromaV/alignment/HitGraphics.hpp>

# include "app/app.h"

# include <TGLObject.h>
# include <TEveElement.h>
# include <TEveVector.h>
# include <TNamed.h>
# include <TAtt3D.h>
# include <TAttBBox.h>
# include <TGedFrame.h>
# include <RQ_OBJECT.h>

# include <unordered_map>

/**@file ed_DrawableTrackingGroup.hpp
 * @brief A header file describing drawable tracking group classes
 * for track visualization and basics for track reconstruction.
 * */

class TGLViewer;
class TGLScene;

namespace sV {
namespace evd {
namespace aux {

class OGLTrackMarkersPainter;

template<typename CacheType>
class PointCoordinatesCache {
public:
    typedef CacheType Cache;
    //typedef alignment::TrackReceptiveVolume<2>::LocalCoordinates LC2D;
    //typedef alignment::TrackReceptiveVolume<3>::LocalCoordinates LC3D;
protected:
    /// A points caches mapping for points in receptive volumes.
    std::unordered_map< const void *, Cache *> _pointsCache;
    /// A points cache instances storage.
    std::list<Cache *> _freeCaches;
public:
    PointCoordinatesCache( size_t n ) {
        for( size_t i = 0; i < n; ++i ) {
            auto c = new Cache;
            bzero( c, sizeof(Cache) );
            _freeCaches.push_back( c );
        }
    }

    ~PointCoordinatesCache() {
        for( auto c : _freeCaches ) {
            delete c;
        }
        for( auto cp : _pointsCache ) {
            delete cp.second;
        }
    }

    template<uint8_t nD>
    Cache * get_cache_for( typename alignment::TrackReceptiveVolume<nD>::LocalCoordinates * lc ) {
        auto it = _pointsCache.find( lc );
        if( _pointsCache.end() != it ) {
            return it->second;
        }
        return rent_new_cache_for<nD>( lc );
    }

    template<uint8_t nD>
    Cache * rent_new_cache_for( typename alignment::TrackReceptiveVolume<nD>::LocalCoordinates * lc ) {
        Cache * cachePtr = nullptr;
        if( _freeCaches.empty() ) {
            cachePtr = new Cache;
        } else {
            cachePtr = _freeCaches.front();
            _freeCaches.pop_front();
        }
        _pointsCache.emplace( lc, cachePtr );
        return cachePtr;
    }

    template<uint8_t nD>
    Cache * free_cache_of( typename alignment::TrackReceptiveVolume<nD>::LocalCoordinates * lc ) {
        auto it = _pointsCache.find( lc );
        if( _pointsCache.end() == it ) {
            // TODO: why does this happen?
            //emraise( badState, "No cache for point %p found. Check your code.", lc );
            sV_logw( "No cache found for point %p. Check your code.\n", lc );
            return nullptr;
        } else {
            auto cachePtr = it->second;
            _freeCaches.push_back( cachePtr );
            _pointsCache.erase( it );
            return cachePtr;
        }
    }
};  // class PointCoordinatesCache

}  // namespace aux

/// ROOT's TEve-wrapper for alignment::TrackingGroup.
class DrawableTrackingGroup : public TEveElement,
                              public TNamed,
                              public TAtt3D,
                              public TAttBBox,
                              public alignment::TrackingGroup {
RQ_OBJECT("DrawableTrackingGroup")  // XXX?
private:
    DrawableTrackingGroup(const DrawableTrackingGroup &) = delete;               // unimplemented
    DrawableTrackingGroup & operator=(const DrawableTrackingGroup & ) = delete;  // unimplemented
protected:
    Color_t     _TEveColor;
    std::list<const TAttBBox *> _BBoxes;

    /// Should perform reconstruction and produce graphical markers to be drawn.
    /// Returns true if got something to be updated on scene.
    virtual bool _V_reconstruct_tracks( sV::aux::HitGraphicsTraits::HitMarkers & )
        { return false; }

    /// Aux method that handles changes in the tracking volumes
    /// and sets this group to be redrawn.
    static bool _invalidate_graphics( alignment::AbstractTrackReceptiveVolume * vol, void * point, void * this_ );
public:
    /// Dynamic ctr accepting parameters in versatile property tree.
    DrawableTrackingGroup( const std::string & name, const alignment::TrackingGroupFactory::PTree & );
    virtual ~DrawableTrackingGroup();

    /// Overrides TrackingGroup::include() in order to
    /// know the pointer to bounding box.
    virtual void include( const Placement & ) override;

    /// ROOT's interfacing method --- returns TObject base ptr.
    virtual TObject * GetObject( const TEveException & ) const override {
        return const_cast<TObject*>(static_cast<const TObject *>(this)); }
    /// ROOT's interfacing method --- bounding box calculation method.
    virtual void ComputeBBox() override;
    /// ROOT's interfacing method --- TEve browser painter.
    virtual void Paint( Option_t * option="" ) override;

    /// Invokes interface method DrawableTrackingGroup::_V_reconstruct_tracks().
    bool reconstruct_tracks( sV::aux::HitGraphicsTraits::HitMarkers & hm ) {
            return _V_reconstruct_tracks(hm); }

    ClassDefOverride( DrawableTrackingGroup, 0 );  // Class for GL visualization of variadic point sets.
};  // class DrawableTrackingGroup

# if 0
class TrackingGroupEditor : public TGedFrame {
protected:
    TrackingGroupEditor( const TrackingGroupEditor & ) = delete;
    TrackingGroupEditor & operator=( const TrackingGroupEditor & ) = delete;
private:
    TrackingGroup * _modelPtr;
protected:
    virtual void _V_set_model( TrackingGroup * ) = 0;
public:
    TrackingGroupEditor( const std::string & title,
                         const TGWindow* p=0, Int_t width=170, Int_t height=30,
                         UInt_t options=kChildFrame, Pixel_t back=GetDefaultFrameBackground() );
    ~TrackingGroupEditor();
    virtual void SetModel( TObject * obj ) override;
};  // class TrackingGroupEditor
# endif

/**@class DrawableTrackingGroupEditor
 * @brief UI editor for tracking groups.
 */
class DrawableTrackingGroupEditor : public TGedFrame {
//RQ_OBJECT("DrawableTrackingGroupEditor")  // XXX?
    // ...
private:
    DrawableTrackingGroup * _modelPtr;
protected:
    virtual void _V_set_model( DrawableTrackingGroup * ) {}
public:
    DrawableTrackingGroupEditor( const std::string & title="",
                                 const TGWindow* p=0, Int_t width=170, Int_t height=30,
                                 UInt_t options=kChildFrame, Pixel_t back=GetDefaultFrameBackground() );

    virtual ~DrawableTrackingGroupEditor();

    virtual void SetModel( TObject * obj ) override;

    ClassDefOverride( DrawableTrackingGroupEditor, 0 );  // Editor widget for drawable tracking groups.
};  // class DrawableTrackingGroupEditor


///  ROOT's TEve-wrapper for drawing evd::DrawableTrackingGroup.
class DrawableTrackingGroupGL : public TGLObject {
private:
    DrawableTrackingGroupGL(const DrawableTrackingGroupGL &) = delete;
    DrawableTrackingGroupGL & operator=(const DrawableTrackingGroupGL &) = delete;
    static sV::evd::aux::OGLTrackMarkersPainter * _painter;
protected:
    mutable DrawableTrackingGroup * fM;  // Model object.
public:
    DrawableTrackingGroupGL();
    virtual ~DrawableTrackingGroupGL() {}

    /// ROOT's interfacing method --- sets the model (in terms of MVC) for drawing.
    virtual Bool_t SetModel(TObject* obj, const Option_t* opt=0) override;
    /// ROOT's interfacing method --- delegates setting of bounding box to upper context.
    virtual void SetBBox() override;
    /// ROOT's interfacing method --- performs actual drawing in OpenGL.
    virtual void DirectDraw(TGLRnrCtx & rnrCtx) const override;

    static void set_painter( sV::evd::aux::OGLTrackMarkersPainter * );

    ClassDefOverride(DrawableTrackingGroupGL, 0); // GL renderer class for DrawableTrackingGroup.
}; // class DrawableTrackingGroupGL


namespace aux {
/// OpenGL painter class for evd::DrawableTrackingGroup.
class OGLTrackMarkersPainter : public ::sV::aux::iHitsPainter {
public:
    using ::sV::aux::iHitsPainter::IsotropicMarkers;
    using ::sV::aux::iHitsPainter::AnisotropicMarkers;
    using ::sV::aux::iHitsPainter::LineMarkers;


    // Drawing methods:
    virtual void   draw_isotropic_points_markers( const IsotropicMarkers &,
                                                        IsotropicPointMarkerType ) override;
    virtual void draw_anisotropic_points_markers( const AnisotropicMarkers & ) override;
    virtual void draw_line_markers( const LineMarkers & ) override;
private:
    /// Store supported point size range and size increments
    /// (min, max, granularity).
    static float _GLPoinSizes[3];
    static void _init_point_size_granularity();

    static std::unordered_map< ::sV::aux::HitGraphicsTraits::IsotropicPointMarkerParameters,
           std::pair<unsigned short, ::sV::aux::HitGraphicsTraits::IsotropicPointPainter> > *
        _isotropicPointPainters;
};  // class OGLMarkerPainter

}  // namespace ::sV::evd::aux
}  // namespace ::sV::evd
}  // namespace sV

# endif  // H_STROMA_V_EVENT_DISPLAY_DRAWABLE_POINT_SET_H

