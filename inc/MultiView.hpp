/*
 * Copyright (c) 2016 Renat R. Dusaev <crank@qcrypt.org>
 * Author: Renat R. Dusaev <crank@qcrypt.org>
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy of
 * this software and associated documentation files (the "Software"), to deal in
 * the Software without restriction, including without limitation the rights to
 * use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
 * the Software, and to permit persons to whom the Software is furnished to do so,
 * subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
 * FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
 * COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
 * IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
 * CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

# ifndef H_EVENT_DISPLAY_MULTIVIEW_H
# define H_EVENT_DISPLAY_MULTIVIEW_H

/**@file ed_MultiView.hpp
 * @brief Header file for MultiView class managing projected views.
 *
 * The implementation is based on standard ROOT TEve example
 * originally written by Matevz Tadel (tutorials/eve/MultiView.C).
 */

// fwd
class TEveViewer;
class TEveScene;

namespace sV {
namespace evd {

namespace aux {

/// Wrapper for TEve stuff aggregating single viewer.
class ViewWrapper {
private:
    /// TEveViewer instance pointer.
    TEveViewer * _viewerPtr;
    /// List of scenes that are to be demonstrated.
    std::list<TEveScene *> _scenes;
    /// Windows pack's slot for this viewer.
    TEveWindowSlot * _slot;
public:
    ViewWrapper();
    ~ViewWrapper();
};

}  // namespace aux

class MultiView {
private:
    std::list<aux::ViewWrapper*> _views;
};  // class MultiView

# if 0
class MultiView {
    TEveProjectionManager * _rhoPhiManagerPtr,
                          * _rhoZManagerPtr;

    TEveViewer * _viewPerspective,
               * _viewRhoPhi,
               * _viewRhoZ
               ;

    TEveScene * _sceneRhoPhi,
              * fRhoZGeomScene, 
              * fRPhiEventScene,
              * fRhoZEventScene
              ;

    /// Creates required scenes, projection managers  and GL viewers.
    MultiView() {
        // Scenes
        //========
        _sceneRhoPhi  = gEve->SpawnNewScene("RPhi Geometry",
                                            "Scene holding projected geometry for the RPhi view.");
        fRhoZGeomScene  = gEve->SpawnNewScene("RhoZ Geometry",
                                              "Scene holding projected geometry for the RhoZ view.");
        fRPhiEventScene = gEve->SpawnNewScene("RPhi Event Data",
                                              "Scene holding projected event-data for the RPhi view.");
        fRhoZEventScene = gEve->SpawnNewScene("RhoZ Event Data",
                                              "Scene holding projected event-data for the RhoZ view.");


        // Projection managers
        //=====================
        _rhoPhiManagerPtr = new TEveProjectionManager(TEveProjection::kPT_RPhi);
        gEve->AddToListTree(_rhoPhiManagerPtr, kFALSE); {
            TEveProjectionAxes* a = new TEveProjectionAxes(_rhoPhiManagerPtr);
            a->SetMainColor(kWhite);
            a->SetTitle("R-Phi");
            a->SetTitleSize(0.05);
            a->SetTitleFont(102);
            a->SetLabelSize(0.025);
            a->SetLabelFont(102);
            _sceneRhoPhi->AddElement(a);
        }

        _rhoZManagerPtr = new TEveProjectionManager(TEveProjection::kPT_RhoZ);
        gEve->AddToListTree(_rhoZManagerPtr, kFALSE); {
            TEveProjectionAxes* a = new TEveProjectionAxes(_rhoZManagerPtr);
            a->SetMainColor(kWhite);
            a->SetTitle("Rho-Z");
            a->SetTitleSize(0.05);
            a->SetTitleFont(102);
            a->SetLabelSize(0.025);
            a->SetLabelFont(102);
            fRhoZGeomScene->AddElement(a);
        }
        // Viewers
        //=========
        TEveWindowSlot *slot = 0;
        TEveWindowPack *pack = 0;

        slot = TEveWindow::CreateWindowInTab(gEve->GetBrowser()->GetTabRight());
        pack = slot->MakePack();
        pack->SetElementName("Multi View");
        pack->SetHorizontal();
        pack->SetShowTitleBar(kFALSE);
        pack->NewSlot()->MakeCurrent();
        _viewPerspective = gEve->SpawnNewViewer("3D View", "");
        _viewPerspective->AddScene(gEve->GetGlobalScene());
        _viewPerspective->AddScene(gEve->GetEventScene());

        pack = pack->NewSlot()->MakePack();
        pack->SetShowTitleBar(kFALSE);
        pack->NewSlot()->MakeCurrent();
        _viewRhoPhi = gEve->SpawnNewViewer("RPhi View", "");
        _viewRhoPhi->GetGLViewer()->SetCurrentCamera(TGLViewer::kCameraOrthoXOY);
        _viewRhoPhi->AddScene(_sceneRhoPhi);
        _viewRhoPhi->AddScene(fRPhiEventScene);

        pack->NewSlot()->MakeCurrent();
        _viewRhoZ = gEve->SpawnNewViewer("RhoZ View", "");
        _viewRhoZ->GetGLViewer()->SetCurrentCamera(TGLViewer::kCameraOrthoXOY);
        _viewRhoZ->AddScene(fRhoZGeomScene);
        _viewRhoZ->AddScene(fRhoZEventScene);
    }
    //---------------------------------------------------------------------------
    void SetDepth(Float_t d) {
        // Set current depth on all projection managers.
        _rhoPhiManagerPtr->SetCurrentDepth(d);
        _rhoZManagerPtr->SetCurrentDepth(d);
    }
    //---------------------------------------------------------------------------
    void ImportGeomRPhi(TEveElement* el) {
        _rhoPhiManagerPtr->ImportElements(el, _sceneRhoPhi);
    }

    void ImportGeomRhoZ(TEveElement* el) {
        _rhoZManagerPtr->ImportElements(el, fRhoZGeomScene);
    }

    void ImportEventRPhi(TEveElement* el) {
        _rhoPhiManagerPtr->ImportElements(el, fRPhiEventScene);
    }

    void ImportEventRhoZ(TEveElement* el) {
        _rhoZManagerPtr->ImportElements(el, fRhoZEventScene);
    }
    //---------------------------------------------------------------------------
    void DestroyEventRPhi() {
        fRPhiEventScene->DestroyElements();
    }

    void DestroyEventRhoZ() {
        fRhoZEventScene->DestroyElements();
    }
};
# endif

}  // namespace evd
}  // namespace sV


# endif  // H_EVENT_DISPLAY_MULTIVIEW_H

