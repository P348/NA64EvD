#ifdef __CINT__

#pragma link off all globals;
#pragma link off all classes;
#pragma link off all functions;

#pragma link C++ nestedclasses;

//#pragma link C++ namespace sV;
//#pragma link C++ namespace sV::evd;
#pragma link C++ class sV::evd::DrawableTrackingGroup+;
#pragma link C++ class sV::evd::DrawableTrackingGroupGL+;
#pragma link C++ class sV::evd::DrawableTrackingGroupEditor+;
#pragma link C++ class sV::evd::aux::PointCoordinatesCache<sV::aux::HitGraphicsTraits::IsotropicPointMarkerParameters>+;
//#pragma link C++ defined_in ed_ControlGUI.h;

#endif

