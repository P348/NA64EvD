/*
 * Copyright (c) 2016 Renat R. Dusaev <crank@qcrypt.org>
 * Author: Renat R. Dusaev <crank@qcrypt.org>
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy of
 * this software and associated documentation files (the "Software"), to deal in
 * the Software without restriction, including without limitation the rights to
 * use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
 * the Software, and to permit persons to whom the Software is furnished to do so,
 * subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
 * FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
 * COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
 * IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
 * CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

# ifndef H_STROMA_V_EVENT_DISPLAY_EVENT_RECEIVER_H
# define H_STROMA_V_EVENT_DISPLAY_EVENT_RECEIVER_H

# include <array>
# include <unordered_map>
# include <boost/circular_buffer.hpp>
# include <boost/thread/mutex.hpp>
# include <memory>

# include <StromaV/net/mCastReceiver.hpp>
# include "recvFSM.tcc"

namespace sV {
namespace evd {

namespace aux {

# define RECEIVER_CIRCULAR_BUFFER_BLOCK_SIZE (1024*1024)

/**@class LeasingPool
 * Current leasing pool design does not support resizing.
 * */
class LeasingPool {
public:
    typedef std::array<unsigned char, RECEIVER_CIRCULAR_BUFFER_BLOCK_SIZE + sizeof(size_t)> Block;
private:
    boost::mutex _bufMtx;
    boost::circular_buffer<Block> _circBuffer;
    boost::condition_variable _conditionGot;
public:
    LeasingPool( size_t nBlocks );
    ~LeasingPool();

    virtual bool put( const unsigned char *, size_t );
    virtual size_t consume( unsigned char * );
    bool empty() const { return _circBuffer.empty(); }
    size_t size() const { return _circBuffer.size(); }
    size_t capacity() const { return _circBuffer.capacity(); }
    /// Hangs invokation thread until at leas one event appear in queue.
    void wait_for_events();
}; // class LeasingPool

/**@class MessageReceiver
 * @brief Manages network connection.
 */
class MessageReceiver : public net::iMulticastEventReceiver {
public:
    //typedef bool (*MessageHandler)( const unsigned char *, size_t ) ;
    typedef boost::function<bool(const unsigned char *, size_t)> MessageHandler;
private:
    boost::asio::io_service * _ioServicePtr;
    bool _ownIOService;
    MessageHandler _handler;

    boost::mutex _receivingMutex;
    boost::thread * _receivingThreadPtr;
    boost::asio::io_service::work * _receivingWorkPtr;
protected:
    virtual bool _V_treat_incoming_message( const unsigned char *, size_t ) override;
public:
    MessageReceiver(
            MessageHandler handler,
            const boost::asio::ip::address & listenAddress,
            const boost::asio::ip::address & multicastAddress,
            boost::asio::io_service * ioServicePtr=nullptr,
            int portNo=30001,
            size_t bufferLength=1024*1024
        );
    ~MessageReceiver();
};  // class MessageReceiver

}  // namespace aux


class EventReceiver : public ::sV::aux::fsm::ReceiverFSM<aux::MessageReceiver *> {
public:
    struct StateManifestingAdapter {
        virtual void _V_on_idle() = 0 ;
        virtual void _V_on_listening() = 0 ;
        virtual void _V_on_pause() = 0 ;
        virtual void _V_on_play() = 0 ;
        virtual void _V_resolve_connection_payload( aux::MessageReceiver * const & ) = 0;
        virtual void _V_on_malfunction( const goo::Exception & details ) = 0 ;
    };
private:
    aux::LeasingPool _pool;
    boost::asio::io_service _ioService;
    aux::MessageReceiver * _messageReceiverPtr;
    StateManifestingAdapter * _adapter;
protected:
    virtual void _V_on_idle() override {
        if( _adapter ){ _adapter->_V_on_idle(); } }
    virtual void _V_on_listening() override  {
        if( _adapter ){ _adapter->_V_on_listening(); } }
    virtual void _V_on_pause() override  {
        if( _adapter ){ _adapter->_V_on_pause(); } }
    virtual void _V_on_play() override {
        if( _adapter ){ _adapter->_V_on_play(); } }
    virtual void _V_resolve_connection_payload( aux::MessageReceiver * const & pl ) override  {
        if( _adapter ){ _adapter->_V_resolve_connection_payload( pl ); } }
    virtual void _V_on_malfunction( const goo::Exception & details ) override  {
        if( _adapter ){ _adapter->_V_on_malfunction( details ); } }

    void _message_received( const unsigned char *, size_t );
public:
    EventReceiver( size_t poolSize,
                   StateManifestingAdapter * adapterPtr );
    ~EventReceiver();

    void set_adapter( StateManifestingAdapter * ptr ) { _adapter = ptr; }

    aux::LeasingPool & pool() { return _pool; }

    /// Queues the transition to connected state.
    void connect( const std::string & hostAddr,
                  int portNo,
                  const std::string & mCastAddr );
    /// Queues the transition to paused state.
    void pause();
    /// Queues the transition to listening state.
    void play();
    /// Queues the transition to idle state.
    void disconnect();
    /// Returns true, if pool is empty.
    bool is_event_queue_empty() const { return _pool.empty(); }
    /// Writes out pool's front block.
    size_t pop_oldest_event( aux::LeasingPool::Block & );
    /// Size getters for leasing pool.
    size_t buffer_occupancy() const { return _pool.size(); }
    /// Capacity getter for leasing pool.
    size_t buffer_capacitance() const { return _pool.capacity(); }
};  // class EventReceiver

}  // namespace evd
}  // namespace sV

# endif  // H_STROMA_V_EVENT_DISPLAY_EVENT_RECEIVER_H

