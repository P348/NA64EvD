# ifndef H_EVENT_DISPLAY_APPLICATION_H
# define H_EVENT_DISPLAY_APPLICATION_H

/*
 * Copyright (c) 2016 Renat R. Dusaev <crank@qcrypt.org>
 * Author: Renat R. Dusaev <crank@qcrypt.org>
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy of
 * this software and associated documentation files (the "Software"), to deal in
 * the Software without restriction, including without limitation the rights to
 * use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
 * the Software, and to permit persons to whom the Software is furnished to do so,
 * subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
 * FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
 * COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
 * IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
 * CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

# include "p348g4_config.h"

//# ifndef ANALYSIS_ROUTINES
//#   error "Event display utils couldn't be built without analysis utils support."
//# endif  // ANALYSIS_ROUTINES

# include "app/app.hpp"
# include "ed_detector.hpp"
# include "p348g4_detector_ids.h"

namespace p348 {
namespace evd {

struct DetectorPlacement {
    union {
        float r[3];
        struct { float x, y, z; } byName;
    } position;
    union {
        float byAxis[3];
        struct { float x, y, z; } byAxisName;
    } rotation;
};

class Application : public mixins::RootApplication {
public:
    typedef AbstractDetector * (*abstract_detector_constructor)( );
protected:
    virtual std::vector<po::options_description> _V_get_options() const override;
    virtual void _V_configure_concrete_app() override;
    virtual int _V_run() override;
protected:
    /// List of detectors to be accessed by name.
    std::unordered_map<std::string, AbstractDetector *> _detectors;

    /// Detector placement information.
    std::unordered_multimap<std::string, DetectorPlacement> _placements;

    /// Detector constructors registered in application.
    static std::unordered_map<std::string, abstract_detector_constructor> * _detConstructors;

    /// Pushes back detector pointer into dictionaries of registered detectors
    /// filling name info, hit routineg info, etc.
    void add_detector_instance( AbstractDetector * );
public:
    Application( po::variables_map * vmPtr );
    virtual ~Application();

    /// Fills list of placement information added by placement info
    /// functions.
    void get_placements_by_constructor_name( std::list<DetectorPlacement> & ) const;

    /**@brief Template name preprocessor.
     *
     * Following chars in name will be considered as special:
     *  $ - detector number in sequence.
     */
    std::string form_detector_name( const std::string & templateName );

    // Constructor registering functions
    // (used by P348G4_REGISTER_DETECTOR_DISPLAYING_CLASS)
    static void add_detector_constructor( const std::string &, abstract_detector_constructor );
    static abstract_detector_constructor get_detector_constructor( const std::string & );

    // Placement info functions. To be fullfilled before run function
    // invokation.
    static void add_detector_placement( const std::string &, const DetectorPlacement & );
    static const DetectorPlacement & detector_placement( const std::string & );

    /// Prints list of registered detector constructors to given stream.
    static void list_detector_constructors( std::ostream & );

    friend class AbstractDetector;
};  // class Application


/// Detector constructor registering macro. Should be placed inside of
/// implementation file with (non-quoted) detector constructor name (to
/// be referenced in placement info routines), and targeting class
/// name.
# define P348G4_REGISTER_DETECTOR_DISPLAYING_CLASS( detectorCtrName, className )  \
static AbstractDetector * __detector_ ## detectorCtrName ## _constructor        \
        () { return new className( # className ); }                             \
static void __ctr_register_evd_detector ## className () __attribute__(( __constructor__(156) )); \
static void __ctr_register_evd_detector ## className () {                       \
    p348::evd::Application::add_detector_constructor( # detectorCtrName,        \
                    __detector_ ## detectorCtrName ## _constructor ); }

}  // namespace evd
}  // namespace p348

# endif  /* H_EVENT_DISPLAY_APPLICATION_H */

