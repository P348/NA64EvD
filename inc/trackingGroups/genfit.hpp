/*
 * Copyright (c) 2016 Renat R. Dusaev <crank@qcrypt.org>
 * Author: Renat R. Dusaev <crank@qcrypt.org>
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy of
 * this software and associated documentation files (the "Software"), to deal in
 * the Software without restriction, including without limitation the rights to
 * use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
 * the Software, and to permit persons to whom the Software is furnished to do so,
 * subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
 * FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
 * COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
 * IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
 * CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

# ifndef H_STROMA_V_EVENT_DISPLAY_GENFIT_TRACKING_GROUP_H
# define H_STROMA_V_EVENT_DISPLAY_GENFIT_TRACKING_GROUP_H

# include "DrawableTrackingGroup.hpp"
# include "na64_detector_ids.h"

# include <list>
# include <boost/shared_ptr.hpp>

namespace genfit {
class AbsKalmanFitter;
class Track;
class AbsTrackRep;
class AbsMeasurement;
class PlanarMeasurement;
class DetPlane;
}

namespace sV {
namespace evd {

class GenfitTrackingGroup
          : public DrawableTrackingGroup {
public:
    typedef ::sV::alignment::TrackingGroupFactory::PTree PTree;
private:
    genfit::AbsKalmanFitter * _fitter;
    genfit::Track           * _reentrantTrackPtr;
    genfit::AbsTrackRep     * _trackRep;

    // Queue of measurements (of length = persistency).
    std::list<genfit::AbsMeasurement *> _measurements;

    // Genfit planes:
    std::unordered_map<const alignment::AbstractTrackReceptiveVolume *,
                       boost::shared_ptr<genfit::DetPlane> > _planes;
protected:
    virtual bool _V_reconstruct_tracks( sV::aux::HitGraphicsTraits::HitMarkers & ) override;
public:
    /// Ctr for genfit; creates the fitter, (TODO).
    GenfitTrackingGroup( const std::string &, const PTree & );
    /// Persistency setter --- sets, how many tracks is to be kept for displaying.
    virtual void persistency( size_t ) override;
    /// Interfacing function providing assignment of detector objects.
    virtual void include( const Placement & ) override;
    // ...
};  // class GenfitTrackingGroup

}  // namespace evd
}  // namespace sV

# endif  // H_STROMA_V_EVENT_DISPLAY_GENFIT_TRACKING_GROUP_H

