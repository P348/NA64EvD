/*
 * Copyright (c) 2016 Renat R. Dusaev <crank@qcrypt.org>
 * Author: Renat R. Dusaev <crank@qcrypt.org>
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy of
 * this software and associated documentation files (the "Software"), to deal in
 * the Software without restriction, including without limitation the rights to
 * use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
 * the Software, and to permit persons to whom the Software is furnished to do so,
 * subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
 * FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
 * COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
 * IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
 * CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

# ifndef H_STROMA_V_EVENT_DISPLAY_POINTS_TRACKING_GROUP_H
# define H_STROMA_V_EVENT_DISPLAY_POINTS_TRACKING_GROUP_H

# include "DrawableTrackingGroup.hpp"

namespace sV {
namespace evd {

/**@class PointSetTrackingGroup
 * @brief Simple track points visualization group.
 *
 * This class implements only hits visualization facility without
 * any actual track reconstruction. It, however, provides support
 * for circular buffered receptive volume according to our
 * persistency scheme.
 *
 * @ingroup evd
 * */
class PointSetTrackingGroup
          : public DrawableTrackingGroup,
            public aux::PointCoordinatesCache<typename sV::aux::HitGraphicsTraits
                                              ::IsotropicPointMarkerParameters> {
public:
    typedef ::sV::alignment::TrackingGroupFactory::PTree PTree;
    typedef aux::PointCoordinatesCache<typename sV::aux::HitGraphicsTraits
                                                         ::IsotropicPointMarkerParameters>
            CachesContainer;
    typedef alignment::AbstractTrackReceptiveVolume Volume;
private:
    size_t _persistency;
    unsigned char _alphaDecStep;
protected:
    virtual bool _V_reconstruct_tracks( sV::aux::HitGraphicsTraits::HitMarkers & ) override;
    /// Performs decrement of the persistent point marker parameter according
    /// to current persistency settings.
    virtual void decrement_display( Cache * ) const;
protected:
    static bool free_cache_2D( Volume * vol, void * point, void * this_ );
    static bool free_cache_3D( Volume * vol, void * point, void * this_ );
public:
    PointSetTrackingGroup( const std::string &, const PTree & );

    /// Persistency setter. Note that persistency settings above of 255 will
    /// not be supported in a correct way (markers will remain on scene but
    /// their opacity will be set to 0).
    virtual void persistency( size_t ) override;

    /// Forwards execution to parent and then registers cache freeing function.
    virtual void include( const Placement & ) override;

    ClassDefOverride( PointSetTrackingGroup, 0 );  // Points set tracking group.
};  // class LinearTrackingGroup


class PointSetTrackingGroupEditor : public DrawableTrackingGroupEditor {
    //RQ_OBJECT("PointSetTrackingGroupEditor")  // XXX?
protected:
    virtual void _V_set_model( DrawableTrackingGroup * ) override {}
public:
    PointSetTrackingGroupEditor( const TGWindow* p=0, Int_t width=170, Int_t height=30,
                                 UInt_t options=kChildFrame, Pixel_t back=GetDefaultFrameBackground() );

    ClassDefOverride( PointSetTrackingGroupEditor, 0 );  // Editor widget for points set tracking group.
};  // class DrawableTrackingGroupEditor

}  // namespace evd
}  // namespace sV

# endif  // H_STROMA_V_EVENT_DISPLAY_POINTS_TRACKING_GROUP_H

