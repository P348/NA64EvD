/*
 * Copyright (c) 2016 Renat R. Dusaev <crank@qcrypt.org>
 * Author: Renat R. Dusaev <crank@qcrypt.org>
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy of
 * this software and associated documentation files (the "Software"), to deal in
 * the Software without restriction, including without limitation the rights to
 * use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
 * the Software, and to permit persons to whom the Software is furnished to do so,
 * subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
 * FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
 * COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
 * IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
 * CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

/**@file genfit.cpp
 * @brief Track reconstruction based on genfit package.
 *
 * This dedicated tracking group summarizes integration with genfit (generic track
 * reconstruction framework, https://github.com/GenFit/GenFit ) software.
 * */

# include "trackingGroups/genfit.hpp"
# include "app/mixins/alignment.hpp"
# include "alignment/DetectorsSet.hpp"

// genfit:

# include <genfit/KalmanFitterRefTrack.h>
# include <genfit/ConstField.h>
# include <genfit/PlanarMeasurement.h>
# include <genfit/RKTrackRep.h>
# include <genfit/Track.h>
# include <genfit/FieldManager.h>
# include <genfit/MaterialEffects.h>
# include <genfit/TGeoMaterialInterface.h>

# include <TDatabasePDG.h>
# include <TRotation.h>
# include <TEveManager.h>
# include <TGeoManager.h>

namespace sV {
namespace evd {

GenfitTrackingGroup::GenfitTrackingGroup( const std::string & nm, const PTree & pt ) :
            DrawableTrackingGroup( nm, pt ),
            _fitter(nullptr),
            _reentrantTrackPtr(nullptr),
            _trackRep(nullptr) {
}

void
GenfitTrackingGroup::persistency( size_t ) {
    // TODO
    // ...
}

void
GenfitTrackingGroup::include( const Placement & pl ) {
    DrawableTrackingGroup::include( pl );
    auto atvPtr = dynamic_cast
        <::sV::alignment::AbstractTrackReceptiveVolume *>
        ( const_cast<Placement &>(pl).detector() );
    if( !atvPtr ) {
        sV_loge( "Detector %s couldn't be part of a tracking group as "
                     "does not implement AbstractTrackReceptiveVolume interface.\n",
                     pl.detector_name() );
        return;
    }

    // Spatial transformation for origin and local CS (U,V)
    TVector3 o( pl.position().r[0],
                pl.position().r[1],
                pl.position().r[2] ),  // origin vector
             u(1, 0, 0),
             v(0, 1, 0)
             ;
    // - rotate U,V:
    TRotation rot;
    rot.RotateX( pl.rotation().angle[0] );
    rot.RotateY( pl.rotation().angle[1] );
    rot.RotateZ( pl.rotation().angle[2] );

    u.Transform( rot );
    v.Transform( rot );

    if( 2 == atvPtr->n_dimensions() ) {
        // Planar detector
        _planes.emplace( atvPtr, boost::shared_ptr<genfit::DetPlane>(new genfit::DetPlane( o, u, v )) );
    } else {
        sV_loge( "(UNIMPLEMENTED) Unable to include detector volume "
                     "%s in genfit consideration.\n",
                     pl.detector_name() );  // TODO
    }
    # if 0
    if( 2 == atvPtr->n_dimensions() ) {
        atvPtr->add_updater_on_remove( free_cache_2D, this );
    } else {
        atvPtr->add_updater_on_remove( free_cache_3D, this );
    }
    # endif
}

/**@brief Invokes fitting procedure and produces track markers to be drawn.
 *
 * This method does all the actual work with genfit to fit the track. Produces
 * markers to be drawn.
 **/
bool
GenfitTrackingGroup::_V_reconstruct_tracks( sV::aux::HitGraphicsTraits::HitMarkers & /*tMarkers*/ ) {
    if( !_fitter ) {
        // todo: select fitter according to pt.
        _fitter = new genfit::KalmanFitterRefTrack();
        // todo: use pt
        Int_t pdg = TDatabasePDG::Instance()->GetParticle( "e-" )->PdgCode();
        _trackRep = new genfit::RKTrackRep( pdg );  // TODO: is for pion hypothesis; were to take this code?
        // TODO: may be lazy init will be more suitable here as it will allow
        // to place initial track seed closer to real track position?
        _reentrantTrackPtr = new genfit::Track( _trackRep,
                                                TVector3(0, 0, 0),
                                                TVector3(0, 0, 3)
                                        );

        auto geom = gEve->GetDefaultGeometry();
        assert( geom );
        geom->CloseGeometry();
        genfit::MaterialEffects::getInstance()->init(new genfit::TGeoMaterialInterface());
        genfit::FieldManager::getInstance()->init(new genfit::ConstField(0. ,10., 0.)); // 1 T
    }

    int hitID = 0;
    // TODO: persistency
    // Iterating among all the volumes, all the sets, all the points
    //size_t nMarkersToDraw = 0;
    for( const alignment::AbstractTrackReceptiveVolume * vol : volumes() ) {
        // Obtain unique identifier for physical detector corresponding to
        // this tracking entity. This relation may appear being not so trivial,
        // however this is done via the chain of cast-caches, so should be probably
        // not as inefficient as dynamic_cast<>(). TODO: cache them here.
        AFR_DetMjNo detMjID = goo::app<mixins::AlignmentApplication>()
                    .detectors()
                    .major_number(&(placement(vol)))
                    ;
        //placement(vol).detector();
        if( 2 == vol->n_dimensions() ) {
            const alignment::TrackReceptiveVolume<2> & vol2DRef = vol->as<2>();
            if( vol2DRef.most_recent_set_is_empty() ) {
                continue;  // Last set has no points, ignore.
            }

            // TODO: make this a property of detector.
            TMatrixDSym hitCov(2);
            {   // TODO: resolution of planar detectors
                double detectorResolution = 1e-3;
                hitCov.UnitMatrix();
                hitCov *= detectorResolution*detectorResolution;
            }
            auto gPlPtr = _planes[vol];
            for( auto point : *(vol2DRef.sets().front()) ) {
                TVectorD hitCoords( 2 );
                hitCoords[0] = point->r[0];
                hitCoords[1] = point->r[1];
                genfit::PlanarMeasurement * m = new genfit::PlanarMeasurement(
                            hitCoords,
                            hitCov,
                            detMjID,
                            ++hitID,
                            NULL );
                m->setPlane( gPlPtr );
                // add measurement:
                _reentrantTrackPtr->insertPoint(new genfit::TrackPoint(m, _reentrantTrackPtr));
            }
        } else {
            const alignment::TrackReceptiveVolume<3> & vol3DRef = vol->as<3>();
            if( vol3DRef.most_recent_set_is_empty() ) {
                continue;  // Last set has no points, ignore.
            }

            sV_logw( "Tracking for 3D volume unimplemented. Omitting vol %p.\n",
                            vol );
        }
    }

    if( !hitID ) {
        // No hits to track found in last event. track->checkConsistency() won't check
        // for empty measurements, so it will fail if no measurements provided for
        // _processTrack() within track instance.
        return true;
    }

    assert(_reentrantTrackPtr->checkConsistency());
    // Do track fitting now.
    _fitter->processTrack( _reentrantTrackPtr );
    // print fit result
    _reentrantTrackPtr->getFittedState().Print();  // XXX
    //check
    assert(_reentrantTrackPtr->checkConsistency());

    // TODO: make the markers

    return true;
}

StromaV_REGISTER_TRACKING_ALGORITHM( "genfit", GenfitTrackingGroup )

}  // namespace evd
}  // namespace sV

