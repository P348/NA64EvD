/*
 * Copyright (c) 2016 Renat R. Dusaev <crank@qcrypt.org>
 * Author: Renat R. Dusaev <crank@qcrypt.org>
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy of
 * this software and associated documentation files (the "Software"), to deal in
 * the Software without restriction, including without limitation the rights to
 * use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
 * the Software, and to permit persons to whom the Software is furnished to do so,
 * subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
 * FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
 * COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
 * IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
 * CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

# include "trackingGroups/points.hpp"

# include "Application.hpp"
# include "alignment/DrawableDetector.hpp"
# include "app/app.h"

# include <goo_app.hpp>

# include <TEveGeoShape.h>
# include <TEveTrans.h>
# include <TEveRGBAPalette.h>
# include <TGedFrame.h>

ClassImp( sV::evd::PointSetTrackingGroup )
ClassImp( sV::evd::PointSetTrackingGroupEditor )

namespace sV {
namespace evd {

PointSetTrackingGroup::PointSetTrackingGroup(
                const std::string & nm,
                const PTree & pt ) : 
        DrawableTrackingGroup( nm, pt ),
        CachesContainer(1024) {
    static_cast<TNamed *>(this)->SetName("Track points");
}

bool
PointSetTrackingGroup::free_cache_2D( Volume * vol, void * point, void * this_ ) {
    DrawableTrackingGroup::_invalidate_graphics( vol, point, this_ );
    PointSetTrackingGroup * me = reinterpret_cast<PointSetTrackingGroup *>(this_);
     alignment::TrackReceptiveVolume<2>::LocalCoordinates * pt =
        reinterpret_cast< alignment::TrackReceptiveVolume<2>::LocalCoordinates *>(point);
    me->free_cache_of<2>(pt);
    return true;
}

bool
PointSetTrackingGroup::free_cache_3D( Volume * vol, void * point, void * this_ ) {
    DrawableTrackingGroup::_invalidate_graphics( vol, point, this_ );
    PointSetTrackingGroup * me = reinterpret_cast<PointSetTrackingGroup *>(this_);
    alignment::TrackReceptiveVolume<3>::LocalCoordinates * pt =
        reinterpret_cast< alignment::TrackReceptiveVolume<3>::LocalCoordinates *>(point);
    me->free_cache_of<3>(pt);
    return true;
}

void
PointSetTrackingGroup::decrement_display( Cache * isoPMarker ) const {
    if( isoPMarker->rgba[3] > _alphaDecStep ) {
        isoPMarker->rgba[3] -= _alphaDecStep;
    }
}

void
PointSetTrackingGroup::persistency( size_t n ) {
    DrawableTrackingGroup::persistency( n );
    if( n ) {
        float decStep = 255./n;
        if( decStep < 1. ) {
            _alphaDecStep = 1.;
        } else {
            _alphaDecStep = decStep;
        }
    } else {
        _alphaDecStep = 255;
    }
    sV_log2("Dec. step set to %d.\n", (int) _alphaDecStep);  // XXX
}

void
PointSetTrackingGroup::include( const Placement & pl ) {
    DrawableTrackingGroup::include( pl );
    auto atvPtr = dynamic_cast
        <::sV::alignment::AbstractTrackReceptiveVolume *>
        ( const_cast<Placement &>(pl).detector() );
    if( !atvPtr ) {
        sV_loge( "Detector %s couldn't be part of a tracking group as "
                     "does not implement AbstractTrackReceptiveVolume interface.\n",
                     pl.detector_name() );
        return;
    }
    if( 2 == atvPtr->n_dimensions() ) {
        atvPtr->add_updater_on_remove( free_cache_2D, this );
    } else {
        atvPtr->add_updater_on_remove( free_cache_3D, this );
    }
}

/**@brief Puts markers to be drawn at appropriate positions.
 *
 * Just puts the points of all tracking points found in particular volume. Does not
 * perform any track reconstruction at all.
 *
 * Point size is for error;
 *   - Color corresponds to deposited amplitude;
 *   - When persistency enabled, alpha channel corresponds to relative event number,
 * causing evnts to gracially disappear.
 **/
bool
PointSetTrackingGroup::_V_reconstruct_tracks( sV::aux::HitGraphicsTraits::HitMarkers & hm ) {
    sV::aux::XForm<2, float>::Float coordinates[3];
    // If persistency enabled, iterate among all active point markers
    // decreasing their  persitency display:
    if( _persistency ) {
        for( auto it = _pointsCache.begin(); _pointsCache.end() != it; ++it ) {
            decrement_display( it->second );
        }
    }
    // Iterating among all the volumes, all the sets, all the points
    size_t nMarkersToDraw = 0;
    for( const alignment::AbstractTrackReceptiveVolume * vol : volumes() ) {
        // Get transformation matrix for this detector:
        const alignment::DetectorPlacement & pl = this->placement( vol );
        const TEveTrans & detTr = const_cast<alignment::DrawableDetector::Container*>(
                pl.detector()->drawable().top_ptr() )
                                  ->RefMainTrans();
        auto updaterPtr = goo::app<evd::Application>().palette_for( vol );
        bool paletteIsLocked = updaterPtr->is_locked();
        //TEveRGBAPalette & palette = *pp.second;
        if( 2 == vol->n_dimensions() ) {
            const alignment::TrackReceptiveVolume<2> & vol2DRef = vol
                    ->as<2>();
            for( auto & set : vol2DRef.sets() ) {
                for( auto point : *set ) {
                    // find cache
                    auto mpPtr = CachesContainer::get_cache_for<2>( point );
                    sV::aux::HitGraphicsTraits::IsotropicPointMarkerParameters & mp = *mpPtr;
                    // 1. Denormalize point coordinates:
                    coordinates[0] = point->r[0];
                    coordinates[1] = point->r[1];
                    coordinates[2] = 0.;
                    const sV::aux::XForm<2, float> & xf = vol2DRef;
                    xf.renorm( coordinates );
                    // 2. Transform local coordinates to global:
                    detTr.MultiplyIP( coordinates, 1 );
                    # ifndef NO_WORKARAOUND_TWICE_TRANSFORM_BUG
                    detTr.MultiplyIP( coordinates, 1 );
                    # endif
                    // 3. Form marker parameters:
                    if( !paletteIsLocked ) {
                        //goo::app<evd::Application>().update_common_palette( palette, point->amplitude );
                        updaterPtr->extend_to( point->amplitude );
                    }
                    updaterPtr->palette().ColorFromValue( point->amplitude, mp.rgba );
                    mp.position[0] = coordinates[0];
                    mp.position[1] = coordinates[1];
                    mp.position[2] = coordinates[2];
                    mp.size = 40;  // TODO
                    // 4. Push back marker parameters:
                    //sV_log3( "Push back %e, %e, %e.\n",
                    //        mp.position[0], mp.position[1], mp.position[2] );  // XXX
                    hm.isotropicMarkers.push_back( mpPtr );
                    //sV_logw( "Considered! #1\n" );  // XXX
                    //global_coordinates_of( vol2DRef, *point, coordinates );
                    //pointsConsidered |= true;
                    //BBoxCheckPoint( coordinates );
                    ++nMarkersToDraw;
                }
            }
        } else {
            const alignment::TrackReceptiveVolume<3> & vol3DRef = vol
                    ->as<3>();
            for( auto & set : vol3DRef.sets() ) {
                for( auto point : *set ) {
                    auto mpPtr = CachesContainer::get_cache_for<3>( point );
                    sV::aux::HitGraphicsTraits::IsotropicPointMarkerParameters & mp = *mpPtr;
                    // 1. Denormalize point coordinates:
                    coordinates[0] = point->r[0];
                    coordinates[1] = point->r[1];
                    coordinates[2] = point->r[2];
                    const sV::aux::XForm<3, float> & xf = vol3DRef;
                    xf.renorm( coordinates );
                    // 2. Transform local coordinates to global:
                    detTr.MultiplyIP( coordinates, 1 );
                    # ifndef NO_WORKARAOUND_TWICE_TRANSFORM_BUG
                    detTr.MultiplyIP( coordinates, 1 );
                    # endif
                    // 3. Form marker parameters:
                    if( !paletteIsLocked ) {
                        //goo::app<evd::Application>().update_common_palette( palette, point->amplitude );
                        updaterPtr->extend_to( point->amplitude );
                    }
                    updaterPtr->palette().ColorFromValue( point->amplitude, mp.rgba );
                    mp.position[0] = coordinates[0];
                    mp.position[1] = coordinates[1];
                    mp.position[2] = coordinates[2];
                    mp.size = 40;  // TODO
                    // 4. Push back marker parameters:
                    //sV_log3( "Push back %e, %e, %e.\n",
                    //        mp.position[0], mp.position[1], mp.position[2] );  // XXX
                    hm.isotropicMarkers.push_back( mpPtr );
                    //sV_logw( "Considered! #1\n" );  // XXX
                    //global_coordinates_of( vol2DRef, *point, coordinates );
                    //pointsConsidered |= true;
                    //BBoxCheckPoint( coordinates );
                    ++nMarkersToDraw;
                }
            }
        }
    }
    sV_log3( "%zu point markers inserted for group \"%s\".\n",
                 nMarkersToDraw, name() );
    return true;
}

StromaV_REGISTER_TRACKING_ALGORITHM( "hits", PointSetTrackingGroup )

// Editor
////////

PointSetTrackingGroupEditor::PointSetTrackingGroupEditor(
        const TGWindow* p, Int_t width, Int_t height,
        UInt_t options,
        Pixel_t /*back*/ ) : DrawableTrackingGroupEditor(
                "PointsSet",
                p, width, height, options) {
    sV_log3( "New instance of PointSetTrackingGroupEditor ctrd (%p).\n",
                 this );
}

}  // namespace evd
}  // namespace sV


