/*
 * Copyright (c) 2016 Renat R. Dusaev <crank@qcrypt.org>
 * Author: Renat R. Dusaev <crank@qcrypt.org>
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy of
 * this software and associated documentation files (the "Software"), to deal in
 * the Software without restriction, including without limitation the rights to
 * use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
 * the Software, and to permit persons to whom the Software is furnished to do so,
 * subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
 * FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
 * COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
 * IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
 * CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

# include <TEveArrowGL.h>
# include <TEveArrow.h>

# include <TGLRnrCtx.h>
# include <TGLIncludes.h>
# include <TGLUtil.h>
# include <TGLQuadric.h>
# include <TEveScene.h>
# include <TEveTrans.h>

# include "alignment/TrackingVolume.tcc"
# include "alignment/DrawableDetector.hpp"

# include "app/app.h"
# include "DrawableTrackingGroup.hpp"

# include <iostream>

/* Upon clicking on group element in TEve browser a SEGFAULT occurs, if
 * SpawnEditor was not invoked before.
 * Valgrind's report:
Error in <TMap::FindObject>: argument is a null pointer
==21161== Invalid read of size 8
==21161==    at 0xFF2545D: TGedEditor::ActivateEditor(TClass*, bool) (in /usr/lib64/root/libGed.so.6.04)
==21161==    by 0xFF25B96: TGedEditor::ActivateEditors(TList*, bool) (in /usr/lib64/root/libGed.so.6.04)
==21161==    by 0xFF25736: TGedEditor::SetModel(TVirtualPad*, TObject*, int) (in /usr/lib64/root/libGed.so.6.04)
==21161==    by 0x90F8AC0: TEveSelection::UserPickedElement(TEveElement*, bool) (in /usr/lib64/root/libEve.so.6.04)
==21161==    by 0x1421B04A: ???
==21161==    by 0x15AB3D20: TClingCallFunc::exec(void*, void*) const (in /usr/lib64/root/libCling.so.6.04)
==21161==    by 0x62BD979: TQConnection::ExecuteMethod(long*, int) (in /usr/lib64/root/libCore.so.6.04)
==21161==    by 0x6333F06: TQObject::Emit(char const*, long*) (in /usr/lib64/root/libCore.so.6.04)
==21161==    by 0x9878C94: TGListTree::HandleButton(Event_t*) (in /usr/lib64/root/libGui.so.6.04)
==21161==    by 0x98C2A1D: TGFrame::HandleEvent(Event_t*) (in /usr/lib64/root/libGui.so.6.04)
==21161==    by 0x991CC87: TGClient::HandleEvent(Event_t*) (in /usr/lib64/root/libGui.so.6.04)
==21161==    by 0x991CF84: TGClient::ProcessOneEvent() (in /usr/lib64/root/libGui.so.6.04)
==21161==  Address 0x0 is not stack'd, malloc'd or (recently) free'd
==21161== 
==21161== 
==21161== Process terminating with default action of signal 11 (SIGSEGV)
==21161==  Access not within mapped region at address 0x0
==21161==    at 0xFF2545D: TGedEditor::ActivateEditor(TClass*, bool) (in /usr/lib64/root/libGed.so.6.04)
==21161==    by 0xFF25B96: TGedEditor::ActivateEditors(TList*, bool) (in /usr/lib64/root/libGed.so.6.04)
==21161==    by 0xFF25736: TGedEditor::SetModel(TVirtualPad*, TObject*, int) (in /usr/lib64/root/libGed.so.6.04)
==21161==    by 0x90F8AC0: TEveSelection::UserPickedElement(TEveElement*, bool) (in /usr/lib64/root/libEve.so.6.04)
==21161==    by 0x1421B04A: ???
==21161==    by 0x15AB3D20: TClingCallFunc::exec(void*, void*) const (in /usr/lib64/root/libCling.so.6.04)
==21161==    by 0x62BD979: TQConnection::ExecuteMethod(long*, int) (in /usr/lib64/root/libCore.so.6.04)
==21161==    by 0x6333F06: TQObject::Emit(char const*, long*) (in /usr/lib64/root/libCore.so.6.04)
==21161==    by 0x9878C94: TGListTree::HandleButton(Event_t*) (in /usr/lib64/root/libGui.so.6.04)
==21161==    by 0x98C2A1D: TGFrame::HandleEvent(Event_t*) (in /usr/lib64/root/libGui.so.6.04)
==21161==    by 0x991CC87: TGClient::HandleEvent(Event_t*) (in /usr/lib64/root/libGui.so.6.04)
==21161==    by 0x991CF84: TGClient::ProcessOneEvent() (in /usr/lib64/root/libGui.so.6.04)
 */

ClassImp(sV::evd::DrawableTrackingGroup)
ClassImp(sV::evd::DrawableTrackingGroupGL)
ClassImp(sV::evd::DrawableTrackingGroupEditor)

/**@file DrawableTrackingGroup.cpp
 * @brief An inplementation file describing drawable tracking group classes
 * rotuines for track visualization and basics for track reconstruction.
 * */

namespace sV {
namespace evd {

/**@class DrawableTrackingGroup
 *
 * This class represents a tracking group instance in terms of ROOT
 * classes implementing some features required to be drawn. In terms
 * of MVC (Model-View-Controller) the instances of tracking groups
 * are models while evd::DrawableTrackingGroupGL is view.
 *
 * Descendants of this class implements particular track reconstruction
 * algorithms with error calculation, track fitting procedures and so
 * on.
 *
 * The meaningful drawable output of these subclasses are presented as
 * a set of containers depicting hits and tracks that are to be drawn
 * further.
 */

DrawableTrackingGroup::DrawableTrackingGroup(
            const std::string & name_,
            const alignment::TrackingGroupFactory::PTree & ) :
        TEveElement(_TEveColor),
        TNamed("DrawableTrackingGroup", ""),
        TAtt3D(), TAttBBox(), alignment::TrackingGroup(name_),
        _TEveColor(0) {
    InitMainTrans();
    SetSourceObject( this );
    //BBoxInit();
    //BBoxZero();
}

DrawableTrackingGroup::~DrawableTrackingGroup() {
}

void
DrawableTrackingGroup::include( const Placement & pl ) {
    alignment::TrackingGroup::include( pl );
    if( !pl.detector()->is_drawable() ) {
        sV_logw( "Detector %p \"%s:%s\" is not drawable and can not provide "
                     "a bounding box for tracking point set.\n",
                     pl.detector(),
                     pl.detector()->detector_name().c_str(),
                     pl.detector()->family_name().c_str() );
        return;
    }
    const TAttBBox * bbxoPtr = pl.detector()->drawable().bbox();
    if( !bbxoPtr ) {
        sV_logw( "Omitting bounding box for detector "
                     "%p \"%s:%s\" with unimplemented B-box yielding.\n",
                     pl.detector(),
                     pl.detector()->detector_name().c_str(),
                     pl.detector()->family_name().c_str());
        return;
    }

    // add redraw callback
    auto atvPtr = dynamic_cast
        <::sV::alignment::AbstractTrackReceptiveVolume *>
        ( const_cast<Placement &>(pl).detector() );
    atvPtr->add_updater_on_insert( _invalidate_graphics, this );  // TODO: move in drawable group?

    _BBoxes.push_back( bbxoPtr );
}

bool
DrawableTrackingGroup::_invalidate_graphics( alignment::AbstractTrackReceptiveVolume *, void *, void * this_ ) {
    DrawableTrackingGroup * me = reinterpret_cast<DrawableTrackingGroup *>(this_);
    me->ElementChanged();  // <<< todo: fine tuning of args?
    // see: https://root.cern.ch/doc/master/classTEveElement.html#ab0553e43a65c5b74633903572b40c774
    return true;
}

/** Sets bounding box accordingly to all volumes included into
 * group when they can be type-casted to ROOT's BBox.
 */
void
DrawableTrackingGroup::ComputeBBox() {
    sV_log3( "DrawableTrackingGroup::ComputeBBox() invoked.\n" );

    if( _BBoxes.empty() ) {
        BBoxZero();
        return;
    }

    BBoxInit();
    Float_t coordinates[3];
    for( const TAttBBox * childBBox_ : _BBoxes ) {
        // Note: since we do not change values here, but need to
        // call const-getter method (which is not well-const
        // qualified):
        TAttBBox * childBBox = const_cast<TAttBBox *>(childBBox_);
        assert(childBBox);
        if( !childBBox->GetBBox() ) {
            // Is it ok to ROOT to avoid lazy initialization? Why the heck!
            sV_logw( "Re-computing bounding box for object %p.\n", childBBox );
            childBBox->ComputeBBox();
        }
        coordinates[0] = childBBox->GetBBox()[0];
        coordinates[1] = childBBox->GetBBox()[2];
        coordinates[2] = childBBox->GetBBox()[4];
        BBoxCheckPoint( coordinates );

        coordinates[0] = childBBox->GetBBox()[1];
        coordinates[1] = childBBox->GetBBox()[3];
        coordinates[2] = childBBox->GetBBox()[5];
        BBoxCheckPoint( coordinates );
    }

    # if 0
    # if 1
    /*
     * This method iterates among all the points in all the volumes
     * calculating bounding box as ROOT requires it. It may be quite
     * time consuming, howver ROOT developers recommends to implement
     * it.
     */
    bool pointsConsidered = false;

    // Iterating among all the volumes, all the sets, all the points
    for( const alignment::AbstractTrackReceptiveVolume * vol : volumes() ) {
        if( 2 == vol->n_dimensions() ) {
            const alignment::TrackReceptiveVolume<2> & vol2DRef = vol
                    ->as<2>();
            for( auto & set : vol2DRef.sets() ) {
                for( auto point : *set ) {
                    sV_logw( "Considered! #1\n" );  // XXX
                    global_coordinates_of( vol2DRef, *point, coordinates );
                    pointsConsidered |= true;
                    BBoxCheckPoint( coordinates );
                }
            }
        } else {
            const alignment::TrackReceptiveVolume<3> & vol3DRef = vol
                    ->as<3>();
            for( auto & set : vol3DRef.sets() ) {
                for( auto point : *set ) {
                    sV_logw( "Considered #2!\n" );  // XXX
                    global_coordinates_of( vol3DRef, *point, coordinates );
                    pointsConsidered |= true;
                    BBoxCheckPoint( coordinates );
                }
            }
        }
    }

    if( !pointsConsidered ) {
        sV_log3( "No hits in tracking group \"%s\".\n", name() );
        BBoxZero();
    }
    # else
    // Use this to enable DirectDraw when no point in set
    // (forces TEve to draw something)
    coordinates[0] = -1000;
    coordinates[1] = -1000;
    coordinates[2] = -1000;
    BBoxCheckPoint( coordinates );

    coordinates[0] = 1000;
    coordinates[1] = 1000;
    coordinates[2] = 1000;
    BBoxCheckPoint( coordinates );
    # endif
    # endif
}

void
DrawableTrackingGroup::Paint(Option_t * /*option*/) {
    // Paint object --- only direct rendering supported.
    PaintStandard( this );
}


// Drawing class
///////////////

sV::evd::aux::OGLTrackMarkersPainter * DrawableTrackingGroupGL::_painter = nullptr;

DrawableTrackingGroupGL::DrawableTrackingGroupGL()
        : TGLObject() {
    // ...
}

void
DrawableTrackingGroupGL::set_painter( sV::evd::aux::OGLTrackMarkersPainter * painterPtr ) {
    if( _painter ) {
        sV_log2( "Overriding tracker painter %p with %p.\n", _painter, painterPtr );
    }
    _painter = painterPtr;
}

Bool_t
DrawableTrackingGroupGL::SetModel(TObject* obj, const Option_t* /*opt*/) {
    fM = SetModelDynCast<DrawableTrackingGroup>(obj);
    return kTRUE;
}

void
DrawableTrackingGroupGL::SetBBox() {
    SetAxisAlignedBBox(((DrawableTrackingGroup*)fExternalObj)->AssertBBox());
}

void
DrawableTrackingGroupGL::DirectDraw(TGLRnrCtx & /*rnrCtx*/) const {
    sV::aux::HitGraphicsTraits::HitMarkers hm;
    sV_log3( "Invokation of reconstruct_tracks()...\n" );  // XXX
    fM->reconstruct_tracks( hm );
    if( !_painter ) {
        sV_loge( "No painter available for drawing tracking group. Omitiing.\n" );
        return;
    }
    _painter->draw_isotropic_points_markers( hm.isotropicMarkers,
            ::sV::aux::HitGraphicsTraits::circles );
    _painter->draw_anisotropic_points_markers( hm.anisotropicMarkers );
    _painter->draw_line_markers( hm.lineMarkers );
    # if 0
    for( auto & vol : fM->volumes() ) {
        const alignment::DetectorPlacement & pl = fM->placement( vol );
        glPushMatrix(); {
            // One can still obtain placement original transformation numericals:
            // const alignment::DetectorPlacement::Position & pos = pl.position();
            // const alignment::DetectorPlacement::Rotation & rot = pl.rotation();
            // Or use already computed TEve trans for det (however, only useful
            // for drawable ones):
            // assert( pl.detector()->is_drawable() );
            const TEveTrans & detTr = const_cast<TEveScene*>( pl.detector()->drawable().scene_ptr() )
                                            ->RefMainTrans();
            //TGLMatrix tmx( detTr.Array() );
            // ?
            glLoadMatrixd( detTr.Array() );
        } glPopMatrix();
    }
    # endif
}

// Tracking group editor base
////////////////////////////

# if 0
TrackingGroupEditor::TrackingGroupEditor(
                const std::string & title,
                const TGWindow* p, Int_t width, Int_t height,
                UInt_t options, Pixel_t back ) :
                    TGedFrame( p, width, height, options | kVerticalFrame, back ),
                    _modelPtr(nullptr) {
    MakeTitle( title.c_str() );
    // ... setup additional control widgets
}

void
TrackingGroupEditor::SetModel( TObject * obj ) {
    _modelPtr = dynamic_cast<TrackingGroup *>( obj );
    _V_set_model( _modelPtr );
}

TrackingGroupEditor::~TrackingGroupEditor() {
    // TODO: could we find a workaround for this
    // bug on destruction?
}
# endif

DrawableTrackingGroupEditor::DrawableTrackingGroupEditor( const std::string & title,
                                 const TGWindow* p, Int_t width, Int_t height,
                                 UInt_t options, Pixel_t back ) :
    TGedFrame( p, width, height, options | kVerticalFrame, back ) {
    MakeTitle( title.c_str() );
    //auto k = new TGVerticalFrame(this);
    //AddFrame( k );
}

DrawableTrackingGroupEditor::~DrawableTrackingGroupEditor() {
}

void
DrawableTrackingGroupEditor::SetModel( TObject * obj ) {
    _modelPtr = dynamic_cast<DrawableTrackingGroup *>( obj );
    _V_set_model( _modelPtr );
    MapWindow();
}

namespace aux {

// TODO: make a dictionary of painters for particular
// isotropic points. First argument in pair should
// be used in glBegin( xxx ) (like GL_LINES, etc) at the
// beginning of point iteration loop while the second
// is particular drawing function which will be ivoked for
// each point (subsequently calling the glVertex3f(...) ).
std::unordered_map< OGLTrackMarkersPainter::IsotropicPointMarkerParameters,
                    std::pair<unsigned short, OGLTrackMarkersPainter::IsotropicPointPainter> > *
    OGLTrackMarkersPainter::_isotropicPointPainters = nullptr;
// TODO: not all the point sizes are allowed in OpenGL rendering.
// May be it has sense to keep sizes aligned according to these
// min/max/increment values?
float OGLTrackMarkersPainter::_GLPoinSizes[3] = {0, 0, 0};


void
OGLTrackMarkersPainter::_init_point_size_granularity() {
    glGetFloatv(GL_POINT_SIZE_RANGE, _GLPoinSizes);
    glGetFloatv(GL_POINT_SIZE_GRANULARITY, _GLPoinSizes + 2);
    sV_log3( "OpenGL point size granularity: %e-%e:%e.\n",
        _GLPoinSizes[0], _GLPoinSizes[1], _GLPoinSizes[2] );
}

//# define NRESTOREGL

void
OGLTrackMarkersPainter::draw_isotropic_points_markers
        ( const std::list<IsotropicPointMarkerParameters *> & points, IsotropicPointMarkerType mt ) {
    assert( circles == mt );  // TODO: dots for massive scatterplots?

    sV_log3( "Got %zu isotropic markers to draw.\n", points.size() );  // XXX

    # ifndef NRESTOREGL
    bool pointSmooth_wasEnabled = glIsEnabled( GL_POINT_SMOOTH ),
         lighting_wasEnabled = glIsEnabled( GL_LIGHTING ),
         blend_wasEnabled = glIsEnabled( GL_BLEND )
         ;

    if( !pointSmooth_wasEnabled ) {
        glEnable( GL_POINT_SMOOTH );
    }
    if( lighting_wasEnabled ) {
        glDisable( GL_LIGHTING );
    }
    //glEnable(GL_TEXTURE_2D);
    glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
    if( !blend_wasEnabled ) {
        glEnable( GL_BLEND );
    }
    GLfloat pointSize; glGetFloatv( GL_POINT_SIZE, &pointSize );
    # endif

    if( !_GLPoinSizes[2] ) {
        _init_point_size_granularity();
    }

    sV_log3( "OGLTrackMarkersPainter::draw_isotropic_points_markers() got %zu points to draw.\n",
                 points.size() );
    for( auto p : points ) {
        glPointSize( p->size*_GLPoinSizes[2] );
        glColor4ub( p->rgba[0], p->rgba[1], p->rgba[2], p->rgba[3] );
        glBegin( GL_POINTS );
            glVertex3f( p->position[0],
                        p->position[1],
                        p->position[2] );
        glEnd();
        //sV_log3(
        //    "IM: {%e, %e, %e} #%x%x%x%x, size %f\n",
        //    p->position[0], p->position[1], p->position[2],
        //    p->rgba[0], p->rgba[1], p->rgba[2], p->rgba[3],
        //    p->size
        //); //XXX
    }

    # ifndef NRESTOREGL
    glPointSize( pointSize );
    if( !pointSmooth_wasEnabled ) {
        glDisable( GL_POINT_SMOOTH );
    }
    if( lighting_wasEnabled ) {
        glEnable( GL_LIGHTING );
    }
    glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
    if( !blend_wasEnabled ) {
        glDisable( GL_BLEND );
    }
    # endif
}

void
OGLTrackMarkersPainter::draw_anisotropic_points_markers
        ( const std::list<AnisotropicPointMarkerParameters *> & mSet ) {
    if( mSet.empty() ) {
        return;
    }
    _TODO_  // TODO
}

void
OGLTrackMarkersPainter::draw_line_markers
        ( const std::list<LineMarkerParameters *> & lps ) {
    if( lps.empty() ) {
        return;
    }
    _TODO_  // TODO
    # if 0
    glBegin( GL_LINES );
    for( auto & lp : lps ) {
        // TODO: set color and opacitiy from lp.rgba
        // TODO: set width from lp.width
        glVertex3f( lp->positions[0][0], lp->positions[0][1], lp->positions[0][2] );
        glVertex3f( lp->positions[1][0], lp->positions[1][1], lp->positions[1][2] );
    }
    glEnd();
    # endif
}

}  // namespace aux

}  // namespace evd
}  // namespace sV

