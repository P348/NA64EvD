/*
 * Copyright (c) 2016 Renat R. Dusaev <crank@qcrypt.org>
 * Author: Renat R. Dusaev <crank@qcrypt.org>
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy of
 * this software and associated documentation files (the "Software"), to deal in
 * the Software without restriction, including without limitation the rights to
 * use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
 * the Software, and to permit persons to whom the Software is furnished to do so,
 * subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
 * FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
 * COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
 * IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
 * CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

# include "EventReceiver.hpp"
# include "goo_exception.hpp"
# include "app/app.h"

# include <boost/date_time/posix_time/posix_time.hpp>
# include <boost/thread/thread.hpp>

namespace sV {
namespace evd {

namespace aux {

// Leasing Pool
//////////////

LeasingPool::LeasingPool( size_t nBlocks ) : _circBuffer(nBlocks) {}

LeasingPool::~LeasingPool() { }

bool
LeasingPool::put( const unsigned char * src, size_t size ) {
    boost::lock_guard<boost::mutex> g( _bufMtx );
    _circBuffer.push_back();
    unsigned char * data = _circBuffer.back().begin();
    *reinterpret_cast<size_t *>(data) = size;
    memcpy( data + sizeof(size_t), src, size );
    _conditionGot.notify_one();
    return true;
}

size_t
LeasingPool::consume( unsigned char * dest ) {
    boost::lock_guard<boost::mutex> g( _bufMtx );
    if( _circBuffer.empty() ) { return 0; }
    unsigned char * data = _circBuffer.front().begin();
    size_t size = *reinterpret_cast<size_t *>( data );
    memcpy( dest, data + sizeof(size_t), size );
    _circBuffer.pop_front();
    return size;
}

void
LeasingPool::wait_for_events() {
    boost::unique_lock<boost::mutex> lock(_bufMtx);
    do {
        _conditionGot.wait( lock );
    } while( _circBuffer.empty() );
}

// MessageReceiver
/////////////////

MessageReceiver::MessageReceiver(
            MessageHandler handler,
            const boost::asio::ip::address & listenAddress,
            const boost::asio::ip::address & multicastAddress,
            boost::asio::io_service * ioServicePtr,
            int portNo,
            size_t bufferLength
        ) : net::iMulticastEventReceiver( *(_ioServicePtr = (ioServicePtr ? ioServicePtr : new boost::asio::io_service)),
                                 listenAddress,
                                 multicastAddress,
                                 portNo,
                                 bufferLength ),
            _ownIOService( !ioServicePtr ),
            _handler( handler ) {
    // Go!
    _receivingThreadPtr = new boost::thread( boost::bind(&boost::asio::io_service::run, _ioServicePtr) );
    _receivingWorkPtr   = new boost::asio::io_service::work(*_ioServicePtr);
}

MessageReceiver::~MessageReceiver() {
    // TODO: shutdown the thread.
    if( _ioServicePtr && _ownIOService ) {
        delete _ioServicePtr;
    }
}

bool
MessageReceiver::_V_treat_incoming_message( const unsigned char * msg, size_t length ) {
    if( !_handler ) {
        sV_logw( "MessageReceiver %p ignores received message of size %zu as handler is not set.\n",
                     this, length );
        return true;
    }
    //sV_log3( "MessageReceiver %p invokes handler for message of length %zu.\n",
    //                 this, length );
    boost::lock_guard<boost::mutex> lock(_receivingMutex);
    return _handler( msg, length );
}

}  // namespace aux


// Event receiver
////////////////

EventReceiver::EventReceiver( size_t poolSize, StateManifestingAdapter * adapterPtr ) :
        ::sV::aux::fsm::ReceiverFSM<aux::MessageReceiver *>( false ),
        _pool( poolSize ),
        _messageReceiverPtr(nullptr),
        _adapter(adapterPtr)
        { ::sV::aux::fsm::ReceiverFSM<aux::MessageReceiver *>::initiate_fsm(); }

EventReceiver::~EventReceiver() {
    // TODO:
}

void
EventReceiver::connect( const std::string & hostAddr,
                        int portNo,
                        const std::string & mCastAddr ) {
    _scheduler.queue_event( _pHandle,
                            boost::intrusive_ptr<EvConnect>(
        new EvConnect( _messageReceiverPtr = new aux::MessageReceiver(
                    boost::bind( &aux::LeasingPool::put, &_pool, _1, _2 ),
                    boost::asio::ip::address::from_string(hostAddr),
                    boost::asio::ip::address::from_string(mCastAddr),
                    &_ioService,
                    portNo ) )
                            )
                        );
}

void
EventReceiver::pause() {
    _scheduler.queue_event( _pHandle,
                            boost::intrusive_ptr<EvPause>( new EvPause()) );
}

void
EventReceiver::play(){  
    _scheduler.queue_event( _pHandle,
                            boost::intrusive_ptr<EvPlay>( new EvPlay()) );
}

void
EventReceiver::disconnect(){
    _scheduler.queue_event( _pHandle,
                            boost::intrusive_ptr<EvDisconnect>( new EvDisconnect()) );
}

size_t
EventReceiver::pop_oldest_event( aux::LeasingPool::Block & dstBlock ) {
    if( !_pool.empty() ) {
        return _pool.consume( dstBlock.begin() );
    }
    return 0;
}

}  // namespace evd
}  // namespace sV


# ifdef STANDALONE_BUILD
void
producer( sV::evd::LeasingPool * poolPtr ) {
    sV::evd::LeasingPool & pool = *poolPtr;
    size_t len;
    unsigned char buffer[RECEIVER_CIRCULAR_BUFFER_BLOCK_SIZE];
    for( uint32_t i = 0; i < 1e5; ++i ) {
        len = (RECEIVER_CIRCULAR_BUFFER_BLOCK_SIZE - 8)*double(rand())/RAND_MAX;
        for( unsigned char * c = buffer + sizeof(int); c < buffer + len; c += sizeof(int) ) {
            int res = rand();
            memcpy( c, &res, sizeof(int) );
        }
        *reinterpret_cast<int *>(buffer) = i;
        pool.put( buffer, len );
    }
    std::cout << "Producer done." << std::endl;  // XXX
}

void
consumer( sV::evd::LeasingPool * poolPtr, std::list<int> * evsPtr ) {
    sV::evd::LeasingPool & pool = *poolPtr;
    std::list<int> & evs = *evsPtr;
    unsigned char buffer[RECEIVER_CIRCULAR_BUFFER_BLOCK_SIZE];
    for(int sense = 1e2; sense > 0; ) {
        size_t n = pool.consume( buffer );
        if( !n ) {
            boost::this_thread::sleep(boost::posix_time::milliseconds(10));
            --sense;
            continue;
        }
        boost::this_thread::sleep(boost::posix_time::milliseconds(1));
        int nEvent = *reinterpret_cast<int*>(buffer);
        evs.push_back(nEvent);
        sense = 1e2;
    }
}

int
main( int argc, char * argv[] ) {
    sV::evd::LeasingPool pool(32);
    std::list<int> evs;

    boost::thread t1( boost::bind( consumer, &pool, &evs ) );
    boost::thread t2( boost::bind( producer, &pool ) );

    t1.join();
    t2.join();

    for( auto it = evs.begin(); it != evs.end(); ++it ) {
        std::cout << *it << ", ";
    }
    std::cout << std::endl;

    return EXIT_SUCCESS;
}
# endif  // STANDALONE_BUILD

