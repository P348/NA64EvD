/*
 * Copyright (c) 2016 Renat R. Dusaev <crank@qcrypt.org>
 * Author: Renat R. Dusaev <crank@qcrypt.org>
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy of
 * this software and associated documentation files (the "Software"), to deal in
 * the Software without restriction, including without limitation the rights to
 * use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
 * the Software, and to permit persons to whom the Software is furnished to do so,
 * subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
 * FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
 * COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
 * IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
 * CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

# include "PaletteUpdater.hpp"

# include <StromaV/utils.hpp>
# include "app/app.h"

# include <TEveRGBAPalette.h>

# include <regex>
# include <climits>

namespace sV {
namespace evd {

PaletteUpdater::Parameters::Parameters() :
        palettePtr( nullptr ),
        isSoft(-1),
        isDynamic(-1),
        rigidness(-1.),
        ownsPalette(false),
        lowerLimit( std::numeric_limits<double>::quiet_NaN() ),
        upperLimit( std::numeric_limits<double>::quiet_NaN() )
        { }

/** Accepts the string consisting of tokens separated by semicolon. Possible
 * tokens are:
 * 
 * Examples:
 *  fixed;range:[0,2500]  --- create an updater with actually disabled
 *                            updating. The associated palette will have
 *                            the range [0,2500]
 *                            and will not automatically adjust itself to new
 *                            values.
 *  dynamic;soft;tolerance:10 --- create an updater instance that will take
 *                            into consideration every incoming value with
 *                            rigidness coefficient =1/tolerance=1/10=0.1.
 *  dynamic;strict;rigidness:.8 --- create an updater instance that will
 *                            consider only near-the-average values with
 *                            rigidness coefficient=0.8
 *
 * */

bool
PaletteUpdater::Parameters::parse_optstring(
            const std::string & optString,
            Parameters & parsRef ) {
    # if 0
    {
        updeterPtr = new PaletteUpdater( new TEveRGBAPalette(5, 255) );
    }
    updeterPtr->unlock_palette();
    updeterPtr->disable_average_all();
    # endif
    {

        const std::string & s = optString;
        static std::regex dlmRx(";+"),
                          rangeRx("^range:\\[(" STROMAV_FLTNUM_ASCII_REGEX 
                                            "),"
                                            "(" STROMAV_FLTNUM_ASCII_REGEX
                                            ")\\]$"
                                 ),
                          rigidnessRx("^(rigidness|tolerance)\\:(" STROMAV_FLTNUM_ASCII_REGEX ")$" )
                          ;
        std::smatch matchResult;
        for( auto it  = std::sregex_token_iterator(s.begin(), s.end(), dlmRx, -1);
                  it != std::sregex_token_iterator(); ++it ) {
            const std::string & token = *it;
            if( token.empty() ) {
                continue;  // omit empty tokens
            }
            if( "soft" == token ) {
                parsRef.isSoft = 1;
            } else if( "strict" == token ) {
                parsRef.isSoft = 0;
            } else if( "dynamic" == token ) {
                parsRef.isDynamic = 1;
            } else if( "fixed" == token ) {
                parsRef.isDynamic = 0;
            } else if( std::regex_match(token, matchResult, rangeRx) ) {
                # if 0  // dev/debug
                std::cout << "TODO #1: " << token << std::endl;
                for( size_t nGroup = 1; nGroup < matchResult.size(); ++nGroup ) {
                    const std::string & groupStr = matchResult[nGroup];
                    if( groupStr.empty() ) continue;  // omit empty subtoken
                    std::cout << "     #1  [" << nGroup << "] : " << groupStr << std::endl;
                }
                # endif
                const std::string & rangeLowerStr = matchResult[1],
                                  & rangeUpperStr = matchResult[3];
                
                //char * strEndPtrLwr, * strEndPtrUpr;
                parsRef.lowerLimit = strtof( rangeLowerStr.c_str(), NULL /*&strEndPtrLwr*/ ),
                parsRef.upperLimit = strtof( rangeUpperStr.c_str(), NULL /*&strEndPtrUpr*/ );
                // todo: check parsing result?
            } else if( std::regex_match(token, matchResult, rigidnessRx) ) {
                # if 0  // dev/debug
                std::cout << "TODO #2: " << token << std::endl;
                for( size_t nGroup = 1; nGroup < matchResult.size(); ++nGroup ) {
                    const std::string & groupStr = matchResult[nGroup];
                    if( groupStr.empty() ) continue;  // omit empty subtoken
                    std::cout << "     #2  [" << nGroup << "] : " << groupStr << std::endl;
                }
                # endif
                const std::string & thresholdTypeStr = matchResult[1],
                                  & thresholdValue = matchResult[2];
                //char * strEndPtr
                const float thresholdVal = strtof(thresholdValue.c_str(), NULL /*strEndPtr*/);
                // todo: check parsing result?
                if( "rigidness" == thresholdTypeStr ) {
                    // value set as is
                    parsRef.rigidness = thresholdVal;
                } else if( "tolerance" == thresholdTypeStr ) {
                    parsRef.rigidness = 1./thresholdVal;
                } else {
                    emraise( badState, "Error in regex (unexpected token \"%s\").",
                            thresholdTypeStr.c_str() );
                }
            } else {
                sV_logw( "Couldn't recognize token \"%s\" in palette "
                             "updater parameters string. Skipping it.\n",
                             token.c_str() );
            }
        }
    }
    return is_valid( parsRef );
}

bool
PaletteUpdater::Parameters::is_valid( const Parameters & parsRef ) {
    // Perform some validity checks:
    bool isValid = true;
    # define PU_P_ASSERT(cond, ...) \
    if( ! (cond) ) { \
        sV_logw( __VA_ARGS__ ); \
        isValid = false; \
    }
    PU_P_ASSERT( -1 != parsRef.isDynamic,  "fixed/dynamic token is not provided." );
    PU_P_ASSERT( parsRef.lowerLimit < parsRef.upperLimit
              && std::isfinite(parsRef.lowerLimit) && std::isfinite(parsRef.upperLimit),
                 "Wrong limits value (or unset, if NaNs) for fixed palette: "
                 "[%e:%e].", parsRef.lowerLimit, parsRef.upperLimit );
    if( 1 == parsRef.isDynamic ) {
        PU_P_ASSERT( -1 != parsRef.isSoft,     "soft/strict token is not provided." );
        PU_P_ASSERT( 0 < parsRef.rigidness && parsRef.rigidness < 1.,
                 "Wrong rigidness value (or unset, if negative): "
                 "have to be > 0 and < 1, got %e.", parsRef.rigidness );
    }
    # undef PU_P_ASSERT
    return isValid;
}

std::string
PaletteUpdater::Parameters::to_string( const Parameters & parsRef ) {
    std::stringstream ss;
    ss << std::scientific;
    if( parsRef.isDynamic ) {
        if( parsRef.isSoft ) {
            ss << "soft;";
        } else {
            ss << "strict;";
        }
        ss << "dynamic;";
        ss << "rigidness:" << parsRef.rigidness << ";";
    } else {
        ss << "fixed;";
    }
    ss << "range:[" << parsRef.lowerLimit << "," << parsRef.upperLimit << "]";
    return ss.str();
}

PaletteUpdater::PaletteUpdater( double rigidness ) : _palettePtr(nullptr),
                                   _limitsLocked(true),
                                   _rangePtr(new ::goo::aux::RigidRange<double>(rigidness)),
                                   _isRangeRigid(true),
                                   _doDeletePaletteOnDtr(false) {}

PaletteUpdater::PaletteUpdater( TEveRGBAPalette * palPtr, double rigidness ) :
                                   _palettePtr(palPtr),
                                   _limitsLocked(true),
                                   _rangePtr(new ::goo::aux::RigidRange<double>(rigidness)),
                                   _isRangeRigid(true),
                                   _doDeletePaletteOnDtr(false) {}

PaletteUpdater::PaletteUpdater( const Parameters & p ) {
    _rangePtr = new ::goo::aux::RigidRange<double>( p.rigidness, !(_isRangeRigid = !p.isSoft) );
    if( 1 == p.isDynamic ) {
        _limitsLocked = false;
    } else {
        _limitsLocked = true;
    }

    if( p.palettePtr ) {
        palette( p.palettePtr );
        _doDeletePaletteOnDtr = p.ownsPalette;
    } else if( p.ownsPalette ) {
        palette( new TEveRGBAPalette( p.lowerLimit, p.upperLimit ) );
        _doDeletePaletteOnDtr = true;
    } else {
        _palettePtr = nullptr;
        _doDeletePaletteOnDtr = false;
    }
}

PaletteUpdater::~PaletteUpdater() {
    delete _rangePtr;
    if( _doDeletePaletteOnDtr ) {
        delete _palettePtr;
    }
}

void
PaletteUpdater::palette( TEveRGBAPalette * palPtr ) {
    _rangePtr->lower( palPtr->GetMinVal() );
    _rangePtr->upper( palPtr->GetMaxVal() );
    _palettePtr = palPtr;
}


bool
PaletteUpdater::extend_to( double val ) {
    if( is_locked() ) {
        return false;
    }
    assert( _rangePtr );
    if( _rangePtr->extend_to(val) ) {
        if( _rangePtr->limits_valid() ) {
            palette().SetLimitsScaleMinMax( _rangePtr->lower(),
                                            _rangePtr->upper() );
        } else {
            sV_logw( "Palette updater instance %p collected invalid limits: [%e, %e]. "
                         "Palette won't be adjusted.\n",
                _rangePtr->lower(), _rangePtr->upper() );
        }
        return true;
    }
    return false;
}

void
PaletteUpdater::set_range_rigid( double threshold ) {
    ::goo::aux::RigidRange<double> * rRangePtr = new ::goo::aux::RigidRange<double>( threshold );
    if( _rangePtr->limits_valid() ) {
        rRangePtr->Range<double>::upper( _rangePtr->upper() );
        rRangePtr->Range<double>::lower( _rangePtr->lower() );
    }
    delete _rangePtr;
    _rangePtr = rRangePtr;
    _isRangeRigid = true;
}

}  // namespace evd
}  // namespace sV


