/*
 * Copyright (c) 2016 Renat R. Dusaev <crank@qcrypt.org>
 * Author: Renat R. Dusaev <crank@qcrypt.org>
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy of
 * this software and associated documentation files (the "Software"), to deal in
 * the Software without restriction, including without limitation the rights to
 * use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
 * the Software, and to permit persons to whom the Software is furnished to do so,
 * subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
 * FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
 * COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
 * IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
 * CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

/* Note: for some reason, by setting the "Normal" window decorations according to
 * https://root.cern.ch/phpBB3/viewtopic.php?t=15374
 * one seems to be able to get rid of disappearing GUI menus bug that can be
 * achieved by: gEve->GetWindowManager()->ShowNormalEveDecorations();
 * Upd: nope. This changes nothing. However we can restore normal behaviour of
 * this X windows by forcing their introspection traversal within using of `$ xprop`.
 */

# include "Application.hpp"

# include <alignment/DetCtrDict.hpp>
# include <alignment/Placement.hpp>
# include <alignment/DetectorsSet.hpp>
# include <alignment/TrackingGroup.hpp>

# include <na64_detector_ids.hpp>  // afNA64

//# include "p348g4_utils.hpp"
//# include "sV_logging.hpp"

# include <TEveManager.h>
# include <TEveBrowser.h>
# include <TGTab.h>
# include <TApplication.h>
# include <TGLViewer.h>
# include <TGLAnnotation.h>
# include <TEveBox.h>
# include <TThread.h>

# include <TEveWindow.h>
# include <TEveViewer.h>

# include <TEveRGBAPalette.h>
# include <TEveRGBAPaletteOverlay.h>

# include <TEveProjectionManager.h>
# include <TEveScene.h>
# include <TEveProjectionAxes.h>
# include <TROOT.h>
# include <TGLIncludes.h>
# include <TGResourcePool.h>
# include <TGCommandPlugin.h>
# include <TPluginManager.h>

# include "EventReceiver.hpp"
# include "ControlGUI.hpp"
# include "DrawableTrackingGroup.hpp"
//# include "MultiView.hpp"

# include <StromaV/CommandWidget.root-plugin/P348CommandWidgetPlugin.h>

# include <goo_dict/parameters/path_parameter.hpp>

# include <fstream>
# include <regex>

namespace sV {

# if 0  // TODO

# endif

namespace evd {

// Application common
////////////////////

Application::Application( sV::AbstractApplication::Config * vmPtr ) :
        sV::AbstractApplication( vmPtr ),
        sV::mixins::RootApplication( vmPtr, "NA64 Event Display" ),
        sV::mixins::AlignmentApplication( vmPtr ),
        _cReceiver(nullptr),
        _cGUI(nullptr),
        _uiFree(true),
        _sadcPaletteUpdater(nullptr),
        _apvPaletteUpdater(nullptr),
        _isConnected(false),
        _isPlaying(false),
        _annotationPtr(nullptr),
        _cmdPluginPtr(nullptr) {
    _enable_ROOT_feature( mixins::RootApplication::enablePlugins );
    _enable_ROOT_feature( mixins::RootApplication::enableDynamicPath );

    vmPtr->insertion_proxy()
        .flag( "list-detectors",
            "Print out known detector constructors and exit." )
        .p<goo::filesystem::Path>( "layout",
            "Placements file in JSON format." )
    ;
}

Application::~Application() {
}

void
Application::_append_common_config( sV::AbstractApplication::Config & d ) {

    AbstractApplication::_append_common_config( d );

    d.insertion_proxy().bgn_sect( "NA64EvD", "NA64 event display options" )
        .p<std::string>( "palette-updating-ADC",
                "Sets palette updating method for ADC. For reference "
                "see config file comments or online documentation.",
            "dynamic;range:[0,255];rigidness:.8;soft")
        .p<std::string>( "palette-updating-APV",
                "Sets palette updating method for APV. For reference "
                "see config file comments or online documentation.",
            "dynamic;range:[0,255];rigidness:.8;soft" )
        ;
}

PaletteUpdater *
Application::_new_palette_updater_by_optstring( const std::string & optString ) const {
    PaletteUpdater::Parameters p;
    if( !PaletteUpdater::Parameters::parse_optstring( optString, p ) ) {
        emraise( badParameter, "Have a problems with palette updater "
                               "parameter string \"%s\" (see log above).",
                               optString.c_str() );
    }
    if( verbosity() > 2 ) {
        const std::string stringified = PaletteUpdater::Parameters::to_string( p );
        sV_log3( "Parsed palette updater parameters: \"%s\" -> \"%s\".\n",
                      optString.c_str(), stringified.c_str() );
    }
    p.ownsPalette = true;
    return new PaletteUpdater( p );
}

void
Application::_V_concrete_app_configure() {
    AbstractApplication::_V_concrete_app_configure();
    if( app_option<bool>("list-detectors") ) {
        alignment::DetectorConstructorsDict::list_constructors( std::cout );
        _immediateExit = true;
    }
    _sadcPaletteUpdater = _new_palette_updater_by_optstring(
                cfg_option<std::string>("NA64EvD.palette-updating-ADC") );
    _apvPaletteUpdater = _new_palette_updater_by_optstring(
                cfg_option<std::string>("NA64EvD.palette-updating-APV") );
    auto * mapsDirPath = common_co()
                .probe_parameter( "analysis.na64.ddd.map-dir" )
                ;
    if( !mapsDirPath ) {
        sV_loge( "The afNA64 loadable module/library did not supply the map "
            "directory parameter. Please, ensure it was linked/loaded and "
            "had been built with support for analysis routines.\n" );
    } else {
        na64::aux::DetectorMapping::self().dump_table( std::cout );
    }
}

int
Application::_V_run() {
    const std::string customCmdLPlName = "TGCommandPlugin" /*"TGMainFrame"*/;
    const std::string commandLineFont = "-*-fixed-medium-*-*-*-14-*-*-*-*-*-*-*" ;
    // TODO: config option ^^^

    if( _immediateExit ) {
        return EXIT_SUCCESS;
    }

    // Open file (or fetch it via network, TODO)
    const std::string layoutsFilePath = app_option<goo::filesystem::Path>( "layout" );

    std::list<alignment::DetectorPlacement> placements;
    TrackingMembersNames trackingMembersNames;

    if( layoutsFilePath.empty() ) {
        emraise( fileNotReachable, "No placements file provided by config.\n" );
    }
    std::ifstream plStream( layoutsFilePath );
    _parse_experimental_layout_settings( plStream, placements, trackingMembersNames );

    # if 0
    TEveManager::Create( kTRUE, "FVI" );
    # else
    {
        TPluginHandler * ph;
        ph = gROOT->GetPluginManager()->FindHandler( customCmdLPlName.c_str() );
        if( ph && -1 != ph->LoadPlugin() ) {
            TEveManager::Create( kTRUE, "FV" );
            gEve->GetBrowser()->StartEmbedding(2);
            Long_t rc = ph->ExecPlugin( 0, gClient->GetRoot(), 200, 500 );
            gEve->GetBrowser()->StopEmbedding();
            if( rc ) {
                TObject * p = (TObject *) rc;
                _cmdPlPtr = static_cast<P348CommandWidgetPlugin *>( p );
                _cmdPlPtr->OverrideTempDir("/dev/shm/");
                _lBuffer.add_target( &(_cmdPlPtr->GetStream()), false );
                _eBuffer.add_target( &(_cmdPlPtr->GetStream()), false );

                const TGFont * font = (TGClient::Instance())->GetFont( commandLineFont.c_str() );
                if( !font ) {
                    font = (TGClient::Instance())->GetResourcePool()->GetDefaultFont();
                }
                FontStruct_t labelfont = font->GetFontStruct();
                _cmdPlPtr->SetFont( labelfont );
            } else {
                sV_loge( "Failed to create custom command widget. "
                    "Plugin was loaded, but ctr returned NULL.\n" );
            }
        } else {
            _cmdPlPtr = NULL;
            sV_loge( "Failed to locate or load plugin \"%s\". "
                         "Custom command line plugin won't be used.\n",
                        customCmdLPlName.c_str() );
            TEveManager::Create( kTRUE, "FVI" );
            {   // Get the command line widget and set font:
                TGFrameElement * fe = static_cast<TGFrameElement *>(
                        gEve
                        ->GetBrowser()
                        ->GetTabBottom()
                        ->GetTabContainer(0)
                        ->GetList()
                        ->First()
                    );
                _cmdPluginPtr = static_cast<TGCommandPlugin *>(fe->fFrame);
                ::sV::aux::set_font_of_TGCommandPlugin(
                    _cmdPluginPtr,
                    commandLineFont.c_str() );     // TODO: configurable
                //_lBuffer.bind_cmdl_plugin( _cmdPluginPtr, false );  // TODO: override implementation
                //_eBuffer.bind_cmdl_plugin( _cmdPluginPtr, false );
                // TODO: the reason why the widget refuses to display warning/error messages
                // is unclear for me.
            }
        }
    }
    # endif

    //gEve->GetBrowser()->SetTitle( "Overview" );
    //gEve->GetDefaultGLViewer()->SetName( "Overview" );
    gEve->GetBrowser()->GetTabRight()->SetText( "Overview" );
    //gEve->GetDefaultViewer()->SetName( "Global viewer" );
    gEve->GetDefaultViewer()->SetElementName( "Global viewer" );
    gEve->GetDefaultViewer()->SetTitle( "Global perspective viewer" );

    //_geoTopNode = gGeoManager->GetTopNode();
    update_last_redraw_time();

    {
        _annotationPtr = new TGLAnnotation( gEve->GetDefaultGLViewer(), "No events", 0.025, 0.975 );
        _annotationPtr->SetTextSize(.035);
    }

    if( placements.empty() ) {
        emraise( badState, "No detectors to be placed on scene." );
    }
    for( auto it  = placements.begin();
              it != placements.end(); ++it ) {
        detectors().construct_detector( *it );
    }

    detectors().draw_detectors( gEve );

    gEve->GetBrowser()->GetTabRight()->SetTab(1);
    gEve->GetDefaultGLViewer()->SetClearColor(kGray+3);
    gEve->Redraw3D(kTRUE);
    {
        Double_t dollyCtr[] = {0, 0, 0};
        gEve->GetDefaultGLViewer()->CurrentCamera().Configure(
                90,
                5000,
                dollyCtr,
                0, 0);
    }
    
    TEveRGBAPaletteOverlay * po = new TEveRGBAPaletteOverlay(
        &(_sadcPaletteUpdater->palette()), 0.55, 0.1, 0.4, 0.05 );
    gEve->GetDefaultGLViewer()->AddOverlayElement(po);
    po = new TEveRGBAPaletteOverlay(
        &(_apvPaletteUpdater->palette()), 0.55, 0.2, 0.4, 0.05 );
    gEve->GetDefaultGLViewer()->AddOverlayElement(po);

    gEve->GetBrowser()
        ->GetMainFrame()
        ->Connect("CloseWindow()", "TApplication", gApplication, "Terminate()");

    //gEve->GetDefaultGLViewer()->SetStyle(TGLRnrCtx::kOutline);

    // todo: additional menus:
    // gEve->GetBrowser()->GetMenuBar()

    # if 0
        TEveWindowSlot * slot = TEveWindow::CreateWindowInTab( gEve->GetBrowser()->GetTabRight() );
        TEveWindowPack * pack = slot->MakePack();
        pack->SetElementName("Multi View");
        pack->SetHorizontal();
        pack->SetShowTitleBar(kFALSE);
        pack->NewSlot()->MakeCurrent();
        auto f3DView = gEve->SpawnNewViewer("3D View", "");
        f3DView->AddScene(gEve->GetGlobalScene());
        f3DView->AddScene(gEve->GetEventScene());
    # endif

    # if 0
    {  // Draw a circle marker:
        //gEve->GetDefaultGLViewer()->GetRnrCtx

        glPushMatrix( );
        glTranslated( 0, 0, 0 );
        gluSphere( gEve->GetDefaultGLViewer()->GetRnrCtx()->GetGluQuadric(), 4, 6, 6 );
        glPopMatrix( );
    }
    # endif

    gEve->GetBrowser()->StartEmbedding( TRootBrowser::kLeft ); {
        TGMainFrame * frm = new TGMainFrame( gClient->GetRoot() );
        frm->SetWindowName( "Event display control" );
        frm->SetCleanup( kDeepCleanup );

        _cGUI = new ControlGUI(frm);

        frm->MapSubwindows();
        frm->SetMWMHints( kMWMDecorAll,
                          kMWMFuncAll,
                          kMWMInputModeless );
        frm->Resize();
        frm->MapWindow();
    } gEve->GetBrowser()->StopEmbedding();
    gEve->GetBrowser()->SetTabTitle( "Display control", 0 );

    //get_TApplication().ProcessLine(
    //            "#include \"/home/crank/Projects/CERN/P-348/p348g4/utils/eventDisplay/inc/ed_ControlGUI.hpp\"",
    //            kTRUE );  // XXX

    //connect( "0.0.0.0", "239.255.0.1", 30001 );  // XXX

    //gEve->GetBrowser()
    //    ->GetMainFrame()
    //    ->Connect("Draw()", "ControlGUI", _cGUI, "invoke_connect()");
    //auto lst = gEve->GetBrowser()->GetListOfSignals();
    //lst->Print();

    {
        boost::mutex::scoped_lock lock( ui_mutex() );
        _uiFree = false;
        _cGUI->set_initial_state(
                /* TODO: config parameters */
            );
        _cReceiver = new EventReceiver(
                /* TODO: config parameters */
                100,
                this
            );
        while( !ui_free() ) { ui_condition().wait( lock ); }
    }

    if( !trackingMembersNames.empty() ) {
        sV_log3( "Track reconstruction groups initialization...\n" );
        for( auto it  = trackingMembersNames.begin();
             trackingMembersNames.end() != it; ++it ) {
            alignment::TrackingGroup * gPtr = it->first;
            for( auto & detName : it->second ) {
                try {
                    gPtr->include( detectors().placement(detName) );
                } catch ( goo::Exception & e ) {
                    if( goo::Exception::notFound == e.code() ) {
                        sV_loge( "Omitting non-existing detector \"%s\" from tracking group \"%s\".\n",
                                      detName.c_str(), gPtr->name() );
                        continue;
                    } else {
                        throw;
                    }
                }
            }
            DrawableTrackingGroup * drawableGPtr = dynamic_cast<DrawableTrackingGroup *>(gPtr);
            if( !drawableGPtr ) {
                sV_logw( "Failed to cast tracking group %p (%s) to drawable type. Skipping it.\n",
                    gPtr, it->first->name() );
            } else {
                gEve->AddElement( drawableGPtr );
                // TODO: why spawning of an editor worx at run-time, but here
                // causes a segfault which is very similar to one when we just
                // click on entity?
                //drawableGPtr->SpawnEditor();
                sV_log1( "Track visualization group \"%s\" added to scene.\n",
                             it->first->name() );
            }
        }
        DrawableTrackingGroupGL::set_painter( new sV::evd::aux::OGLTrackMarkersPainter(/* todo:confs?*/) );
        sV_log3( "... track reconstruction facility initialized.\n" );
    } else {
        sV_log2( "No track reconstruction configured.\n" );
    }

    # if 0
    {
        double xs[] = {-10, -10,  10,  10},
               ys[] = {-20,  20,  20, -20}
               ;
        TEveLine * poly = new TEveLine( 4 );
        for( uint8_t i = 0; i < 4; ++i ) {
            poly->SetPoint( i, xs[i]*100, ys[i]*100, 0. );
        }
        poly->SetMainColor( kRed );
        poly->SetLineStyle( 2 );
        poly->SetLineWidth( 3 );
        gEve->GetGlobalScene()->AddElement( poly );
    }
    # endif

    // TODO: !!!
    // TGLViewer* v = viewer->GetGLViewer();
    // v->SetCurrentCamera(TGLViewer::kCameraOrthoXOY);

    // Projection viewer:
    # if 0
    {
        TEveViewer * projViewer = gEve->SpawnNewViewer( "ProjectedViewer" );
        TEveScene * projScene = gEve->SpawnNewScene( "ProjectedScene" );
        projViewer->AddScene(projScene);
        projViewer->AddScene( gEve->GetGlobalScene() );  // XXX?
        {
            TGLViewer * v = projViewer->GetGLViewer();
            v->SetCurrentCamera( TGLViewer::kCameraOrthoXOY );
        }
        TEveProjectionManager * mng = new TEveProjectionManager(TEveProjection::kPT_RPhi);
        projScene->AddElement( mng );
        TEveProjectionAxes * axes = new TEveProjectionAxes( mng );
        projScene->AddElement( axes );
        gEve->AddToListTree( axes, kTRUE );
        gEve->AddToListTree( mng, kTRUE );
        //mng->ImportElements( cmp );
    }
    # endif

    // NOTE: <w>, <r>, <t> keys controls wireframe-surface-outline appearance!
    //       <e> controls background color

    # if 1
    { // XXX:
        # if 0
        char bf[128];
        snprintf( bf, sizeof(bf), ".x plugins4evd/projections.C((TEveScene *) %p)",
                  detectors().placement("ECAL0"). scene() );
        gROOT->ProcessLine(bf);
        # else
        gROOT->ProcessLine( ".L plugins4evd/projections.C" );
        # endif
        auto dedDetScenesLstPtr = new TEveSceneList("Scenes-standalone",
                                                    "Scenes dedicated to particular detector");
        gEve->AddToListTree( dedDetScenesLstPtr, kTRUE );
        auto dedDetViewersLstPtr = new TEveViewerList("Viewers-standalone",
                                                      "Viewers dedicated to particular detector");
        gEve->AddToListTree( dedDetViewersLstPtr, kTRUE );
    }
    # endif
    # if 1
    { // XXX:
        ::goo::aux::iApp::add_handler(
            ::goo::aux::iApp::_SIGABRT,
            ::goo::aux::iApp::attach_gdb,
            "Attaches gdb to a process after SIGABRT."
            );
    }
    # endif

    sV_log3( "TApplication::Run() will now grab the main execution thread...\n" );
    get_TApplication().Run(kTRUE);
    sV_log3( "...TApplication::Run() returned execution thread.\n" );

    return EXIT_SUCCESS;
}

// Application specific
//////////////////////

void
Application::_V_on_idle() {
    _isConnected = false;
    _cGUI->set_status_label("Disconnected.", 0);
    _cGUI->connection_control_set_status_label( "disconnected" );
    _cGUI->connection_control_set_connect_button_label( "  Conne&ct  " );
    // freeze playback frame
    _cGUI->playback_control_disable();
    // unfreeze connection frame
    _cGUI->connection_control_enable();
    sV_log1( "Application asynchroneous state machine emerged.\n" );
    _uiFree = true;
    _uiCondition.notify_one();
}

void
Application::_V_on_listening() {
    _isConnected = true;
    _cGUI->connection_control_set_connect_button_label( "Disconne&ct" );
    _cGUI->set_status_label("Listening...", 0);
    _cGUI->connection_control_set_status_label( "Receiving" );
    // unfreeze playback frame
    _cGUI->playback_control_enable();
    {  // TODO: should be controlled by fine playback state(s)
        //_cGUI->playback_play_button_set_picture_pause();
        _cGUI->playback_previous_disable();
        _cGUI->playback_next_disable();
    }
    // freeze connection frame
    _cGUI->connection_control_settings_disable();
    //_cGUI->toggle_connection_button( false );
    _uiFree = true;
    _uiCondition.notify_one();
}

void
Application::_update_event_navigation_buttons() {
    // TODO: have events in queue before current event
    if( false ) {
        _cGUI->playback_previous_enable();
    } else {
        _cGUI->playback_previous_disable();
    }
    // TODO: have events in queue after current event
    if( false ) {
        _cGUI->playback_next_enable();
    } else {
        _cGUI->playback_next_disable();
    }
}

void
Application::_V_on_pause() {
    _isPlaying = false;
    _cGUI->stop_periodical_update();
    _cGUI->set_status_label("Paused.", 1);
    _cGUI->playback_play_button_set_picture_play();
    _update_event_navigation_buttons();
    _uiFree = true;
    _uiCondition.notify_one();
}

void
Application::_V_on_play() {
    _isPlaying = true;
    _cGUI->start_periodical_update( _cGUI->min_delay_msec() );
    _cGUI->set_status_label("Playing.", 1);
    _cGUI->playback_play_button_set_picture_pause();
    _cGUI->playback_next_disable();
    _cGUI->playback_previous_disable();
    _uiFree = true;
    _uiCondition.notify_one();
}

void
Application::_V_resolve_connection_payload( sV::evd::aux::MessageReceiver * const & ) {
    _cGUI->set_status_label("Resolving connection...", 0);
    _uiFree = true;
    _uiCondition.notify_one();
}

void
Application::_V_on_malfunction( const goo::Exception & /*details*/ ) {
    _cGUI->set_status_label("Malfunction.", 0);
    // TODO: print out error details
    _uiFree = true;
    _uiCondition.notify_one();
}


bool
Application::has_queued_events() const {
    return !_cReceiver->is_event_queue_empty();
}

bool
Application::_emplace_representative_event() {
    const ::sV::events::MulticastMessage & msg = _reentrantMessageInstance;
    if( msg.has_status() ) {
        boost::lock_guard<boost::mutex> g( ui_mutex() );
        if( ::sV::events::MulticastMessage_SenderStatusMessage::QUENCHING ==
                msg.status().senderstatus() ) {
            // notify that destination is quenching now
            sV_log1("Source status: quenching.\n");
        } else if( ::sV::events::MulticastMessage_SenderStatusMessage::OPERATING ==
                msg.status().senderstatus() ) {
            // notify that destination is just arised.
            sV_log1("Source status: arised.\n");
        } else if( ::sV::events::MulticastMessage_SenderStatusMessage::IDLE ==
                msg.status().senderstatus() ) {
            // notify that destination is idle (heartbeat signal).
            sV_log1("Source status: idling.\n");
        } else if( ::sV::events::MulticastMessage_SenderStatusMessage::MALFUNCTION ==
                msg.status().senderstatus() ) {
            // notify destination malfunction.
            sV_logw("Source status: malfunction.\n");
        } else if( ::sV::events::MulticastMessage_SenderStatusMessage::UNKNOWN ==
                msg.status().senderstatus() ) {
            sV_loge("Couldn't interpret source status code.\n");
        } else {
            sV_loge( "Got unexpected status message from multicast server (%d).\n",
                    (int) msg.status().senderstatus() );
        }
        return false;
    }
    if( msg.has_event() && msg.event().has_displayableinfo() ) {
        if( detectors().placements_list().empty() ) {
            sV_logw( "Has no detectors in current setup. Omitting events drawing.\n" );
            return true;
        }
        boost::lock_guard<boost::mutex> g( ui_mutex() );
        gEve->DisableRedraw();

        bool doneDraw = detectors().reset_hits();
        doneDraw |= detectors().dispatch_hits( msg.event().displayableinfo() );
        /*
         * TODO:
        if( has_annotation() && msg.event().has_experimental() ) {
            annotate( msg.event().experimental() );
        }
        */
        if( doneDraw ) { gEve->DoRedraw3D(); }
        gEve->EnableRedraw();
        update_last_redraw_time();
        return doneDraw;
    } else {
        sV_log3( "Event has no preprocessed information.\n" );
        return false;
    }
}

void
Application::emplace_new_events() {
    bool wasDrawn = false;
    if( !last_redraw_foul() ) {
        return;
    }
    do {
        size_t mLength = _cReceiver->pop_oldest_event(_reentrantSerializedMessageBuffer);
        if( !mLength ) {
            sV_loge( "Unexpected state of receiver events queue: returned message of null length.\n" );
            continue;
        }
        // Try to deserialize MulticastMessage and invoke drawing.
        if( !_reentrantMessageInstance.ParseFromArray(
                    _reentrantSerializedMessageBuffer.begin(), mLength ) ) {
            sV_loge( "Got deserialization error of message %d bytes size.\n",
                         (int) mLength );
            continue;
        }
        wasDrawn = _emplace_representative_event();
    } while( !wasDrawn && has_queued_events() );
    if( !wasDrawn ) {
        // manifest some info for user (todo: GUI?)
        sV_log1( "Got nothing to draw in the last queue.\n" );
    }
}

bool
Application::last_redraw_foul() const {
    if( !_cGUI ) {
        return true;
    }
    auto dt = boost::posix_time::microsec_clock::local_time() - _lastRedrawTime;
    return dt.total_milliseconds() > _cGUI->min_delay_msec();
}

void
Application::update_last_redraw_time() {
    _lastRedrawTime = boost::posix_time::microsec_clock::local_time();
}



bool
Application::is_playing() const {
    return _isPlaying;
}

void
Application::play() {
    receiver().play();
}

void
Application::pause() {
    receiver().pause();
}



bool
Application::is_connected() const {
    return _isConnected;
}

void
Application::connect() {
    std::string hostAddr = _cGUI->connection_host_address(),
               mCastAddr = _cGUI->connection_multicast_address()
            ;
    int portNo = _cGUI->connection_port_no();
    sV_log3( "Trying to resolve connection to %s:%d/%s.\n",
                hostAddr.c_str(),
                portNo,
                mCastAddr.c_str()
            );
    receiver().connect( hostAddr, portNo, mCastAddr );
}

void
Application::disconnect() {
    receiver().disconnect();
}


bool
Application::do_draw_averaged() const {
    return _cGUI->do_draw_averaged();
}

size_t
Application::persistency() const {
    return _cGUI->persistency();
}

void
Application::persistency( size_t nEvents ) {
    for( auto grpPair : AlignmentApplication::tracking_groups() ) {
        grpPair.second->persistency( nEvents );
    }
}

void
Application::annotate( const std::string & txt ) {
    if( _annotationPtr ) {
        _annotationPtr->SetText( txt );
        //gEve->GetDefaultGLViewer()->RequestDraw();  //?
    }
}

void
Application::annotate( const ::na64::events::ExperimentalEvent_Payload & expEve ) {
    char annotationStr[512],
         timeBf[32]
         ;
    time_t timeStruct;
    memcpy( &timeStruct, expEve.timestruct().c_str(), sizeof(time_t) );

    strftime( timeBf, sizeof(timeBf),
              "%Y-%m-%d %H:%M:%S",
              localtime(&timeStruct) );
    snprintf( annotationStr, sizeof(annotationStr),
              "Run #%d / spill #%d / event #%d (%d in spill)\n"
              "Time %s:%d",
              expEve.runno(), expEve.spillno(), expEve.evnoinrun(), expEve.evnoinspill(),
              timeBf, expEve.timenum()
              );
    annotate( annotationStr );
}

//
// Palettes

# if 0
TEveRGBAPalette &
Application::palette_common_SADC() {
    if( !_commonSADCPalette ) {
        _commonSADCPalette = new TEveRGBAPalette(5, 255);
    }
    return *_commonSADCPalette;
}

const TEveRGBAPalette &
Application::palette_common_SADC() const {
    return goo::app<Application>().palette_common_SADC();
}

void
Application::update_palette_common_SADC( double newMax ) {
    TEveRGBAPalette & p = palette_common_SADC();
    if( p.GetMaxVal() < newMax && !_commonSADCPalette_isLocked ) {
        double lowerCutOffValue = newMax/1e2;  // TODO: configurable option
        p.SetLimitsScaleMinMax( lowerCutOffValue, newMax );
        sV_log3( "Common SADC palette set to %f, %f.\n", lowerCutOffValue, p.GetMaxVal() );
    }
}



TEveRGBAPalette &
Application::palette_common_APV() {
    if( !_commonAPVPalette ) {
        _commonAPVPalette = new TEveRGBAPalette(5, 255);
    }
    return *_commonAPVPalette;
}

const TEveRGBAPalette &
Application::palette_common_APV() const {
    return goo::app<Application>().palette_common_APV();
}

void
Application::update_palette_common_APV( double newMax ) {
    TEveRGBAPalette & p = palette_common_APV();
    if( p.GetMaxVal() < newMax && !_commonAPVPalette_isLocked ) {
        double lowerCutOffValue = newMax/1e2;  // TODO: configurable option
        p.SetLimitsScaleMinMax( lowerCutOffValue, newMax );
        sV_log3( "Common APV palette set to %f, %f.\n", lowerCutOffValue, p.GetMaxVal() );
    }
}
# endif

PaletteUpdater *
Application::palette_for( const alignment::AbstractTrackReceptiveVolume * detPtr ) {
    auto it = _det2palette.find( detPtr );
    if( _det2palette.end() == it ) {
        emraise( notFound,
                "Event display receptive track entity %p has no associated palette.",
                detPtr );
    }
    return it->second;
}

void
Application::associate_palette_with_tracking_detector( const alignment::AbstractTrackReceptiveVolume * volPtr,
                                                       PaletteUpdater * palPtr ) {
    _det2palette.emplace( volPtr, palPtr );
}

}  // namespace evd
}  // namespace sV

