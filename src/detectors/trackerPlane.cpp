/*
 * Copyright (c) 2016 Renat R. Dusaev <crank@qcrypt.org>
 * Author: Renat R. Dusaev <crank@qcrypt.org>
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy of
 * this software and associated documentation files (the "Software"), to deal in
 * the Software without restriction, including without limitation the rights to
 * use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
 * the Software, and to permit persons to whom the Software is furnished to do so,
 * subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
 * FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
 * COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
 * IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
 * CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

# if 0
# include <TEvePointSet.h>

# include <goo_exception.hpp>

# include "Application.hpp"
# include "detectors/trackerPlane.hpp"
# include "alignment/DetCtrDict.hpp"

namespace sV {
namespace evd {

/// Static information about GEM and MuMegas sizes.
const std::map<const std::string, std::array<float, 2> >
TrackerPlane::_dtrSizeConst = {
    {"Micromega",   {{ 80., 80.   }} },
    {"GEM",         {{ 700., 700. }} },  // << TODO: correct this values
};

bool
TrackerPlane::_V_next_point( const Hit & hit, Point & pointRef ) {
    if( !hit.has_points() || _lastPointIdx >= (uint32_t) hit.points().clusters_size() )
        return false;
    const events::TrackPoint & mPoint = hit.points().clusters(_lastPointIdx).trackpoint();
    // range [0..1]
    pointRef.r[0] = mPoint.x();
    pointRef.r[1] = mPoint.y();
    pointRef.amplitude = mPoint.amplitude();
    ++_lastPointIdx;
    return true;
}

TrackerPlane::TrackerPlane( const std::string & ctrName,
                            const std::string & detectorName,
                            PaletteUpdater * palettePtr ) :
        iDetector( ctrName, detectorName,   /*drawable*/  true,
                                            /*receptive*/ true,
                                            /*compound*/  true ),
        iCoordinatePlane( ctrName, detectorName,
                          _dtrSizeConst.find(ctrName)->second.data(), true ),
        alignment::CompoundDetector( ctrName, detectorName,
                {   detectorName.c_str(),
                    detector_name_by_code (
                        (EnumScope::MajorDetectorsCode) AFR_UniqueDetectorID( detector_complementary_Y_id(
                            detector_major_by_name( detectorName.c_str() )
                        ) ).byNumber.major
                    )
                } ) {
    // Associate palette for track points:
    {
        goo::app<evd::Application>().associate_palette_with_tracking_detector(
                this, palettePtr );
    }
    # if 0
    // XXX:
    # warning "Delete lines below as they're just to check transformations."
    n_sets( 1 ); // XXX
    open_new_set();  //XXX
    update_point( {  1, {  0,   0 } } );
    update_point( {  2, {  0,   1 } } );
    update_point( {  3, {  1,   1 } } );
    update_point( {  4, {  1,   0 } } );
    # endif
}

bool
TrackerPlane::_V_reset_hits() {
    _lastPointIdx = 0;
    return iCoordinatePlane::_V_reset_hits();
}

namespace _1 {
REGISTER_EVD_CONSTRUCTOR( TrackerPlane, "Micromega" )
}

namespace _2 {
REGISTER_EVD_CONSTRUCTOR( TrackerPlane, "GEM" )
}

}  // namespace evd
}  // namespace sV
# endif
