/*
 * Copyright (c) 2016 Renat R. Dusaev <crank@qcrypt.org>
 * Author: Renat R. Dusaev <crank@qcrypt.org>
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy of
 * this software and associated documentation files (the "Software"), to deal in
 * the Software without restriction, including without limitation the rights to
 * use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
 * the Software, and to permit persons to whom the Software is furnished to do so,
 * subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
 * FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
 * COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
 * IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
 * CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

# if 0
# include "detectors/iCoordPlanar.hpp"
# include "Application.hpp"

/**@file iCoordPlanar.cpp
 * @brief Implementation file for iCoordinatePlane class.
 * @ingroup evd
 *
 * Describes coordinate plane detector class interface class which
 * depends on alignment routines. Provides basic drawing of coordinate
 * plane boundaries.
 * */

# include <TEvePointSet.h>
# include <TEveLine.h>
# include <TEveGeoShape.h>

namespace sV {
namespace evd {

void
iCoordinatePlane::_V_draw_detector( DrawableDetector::Container * sc ) {
    // Clockwise:
    const float sFact[5][2] = {
            {-.5, -.5}, {-.5,  .5},
            { .5,  .5}, { .5, -.5},
            {-.5, -.5}
        };
    _boundingLine = new TEveLine( 5 );
    for( uint8_t i = 0; i < 5; ++i ) {
        _boundingLine->SetPoint( i,
            sFact[i][0]*width(),
            sFact[i][1]*height(),
            0. );
    }
    // Visual assets:
    _boundingLine->SetMainColor( kRed );
    _boundingLine->SetLineStyle( 2 );
    _boundingLine->SetLineWidth( 3 );

    _set_label( detector_name().c_str(), 0., 1.2*height()/2, 0. );

    sc->AddElement( _boundingLine );

    # if 0
    std::stringstream ss;
    ss << *this;
    sV_log3( "iCoordinatePlane::_V_draw_detector() called for XForm %ex%e:\n%s\n",
                width(), height(),
                ss.str().c_str() );
    # endif
}

bool
iCoordinatePlane::_V_draw_hit() {
    return !most_recent_set_is_empty();
}

bool
iCoordinatePlane::_V_treat_new_hit( const Hit & cHit ) {
    bool gotOne = false;
    Point point;
    while( _V_next_point( cHit, point ) ) {
        if( !_isHitNormed ) {
            // norm point using XForm:
            norm( point.r );
        }
        update_point( point );
        gotOne = true;
        //break;  // XXX XXX !!!
    }
    return gotOne;
}

bool
iCoordinatePlane::_V_reset_hits() {
    bool hadPoints = !most_recent_set_is_empty();
    open_new_set();
    return hadPoints;
}

const TAttBBox *
iCoordinatePlane::_V_bbox() const {
    return _boundingLine;
}

}  // namespace evd
}  // namespace sV
# endif

