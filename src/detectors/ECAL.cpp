/*
 * Copyright (c) 2016 Renat R. Dusaev <crank@qcrypt.org>
 * Author: Renat R. Dusaev <crank@qcrypt.org>
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy of
 * this software and associated documentation files (the "Software"), to deal in
 * the Software without restriction, including without limitation the rights to
 * use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
 * the Software, and to permit persons to whom the Software is furnished to do so,
 * subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
 * FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
 * COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
 * IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
 * CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

# include "alignment/DetCtrDict.hpp"
# include "detectors/ECAL.hpp"
# include "Application.hpp"

# include <TEveFrameBox.h>
# include <TEveManager.h>
# include <TEveBoxSet.h>
# include <TRandom.h>
# include <TStyle.h>
# include <TEveTrans.h>
# include <TEveText.h>
# include <TEveGeoShape.h>

namespace sV {
namespace evd {

# define UNIFORM_INIT 1 // XXX

ECAL::ECAL_Cell::ECAL_Cell(
                const std::string & ctrName,
                const std::string & detectorName,
                TEveDigitSet::DigitBase_t * digitPtr,
                float * cAmplitudePtr,
                float originX, float originY) :
        iDetector( ctrName, detectorName, false, false, false ),
        Parent::ReceptivePart( ctrName, detectorName ) {
    _TODO_  // TODO
}

bool
ECAL::ECAL_Cell::_V_recache_summary( const ::sV::events::DetectorSummary & summary_ ) {
    _TODO_  // TODO
}

TEveElement *
ECAL::_draw_preshower( double width, double height, double depth,
                       double cellSideX, double cellSideY ) {
    const size_t nCellsX = width/cellSideX,
                 nCellsY = height/cellSideY
                 ;
    # ifdef UNIFORM_INIT
    TRandom r(0);
    # endif
    gStyle->SetPalette(1, 0);
    TEveFrameBox* frm = new TEveFrameBox();
    frm->SetAABoxCenterHalfSize(0, 0, 0, width/2, height/2, depth/2);
    frm->SetFrameColor( kWhite );
    //frm->SetBackColorRGBA( 120, 120, 120, 15 );
    //frm->SetDrawBack(kTRUE);
    TEveBoxSet* q = new TEveBoxSet("ECAL-preshower");

    q->SetMainTransparency(80);  // XXX?

    _boxSetPreshower = q;
    q->SetPalette( &goo::app<evd::Application>().common_SADC_palette_updater().palette() );
    q->SetFrame(frm);
    //q->Reset(TEveBoxSet::kBT_AABoxFixedDim, kFALSE, 64);
    q->Reset(TEveBoxSet::kBT_AABox, kFALSE, 64);
    q->SetDefWidth( cellSideX );
    q->SetDefHeight( cellSideY );
    q->SetDefDepth( depth );
    {
        for( UByte i = 0; i < nCellsX; ++i ) {
            for( UByte j = 0; j < nCellsY; ++j ) {
                q->AddBox(
                        -width/2  + i*cellSideX,
                        -height/2 + j*cellSideY,
                        -depth/2,
                        cellWidth_mm,
                        cellHeight_mm,
                        depthPreshower
                    );
                # ifdef UNIFORM_INIT
                q->DigitValue( r.Uniform(0, 255) );  // TODO: set to NULL?
                # else
                q->DigitValue( 0 );
                # endif
            }
        }
    }
    q->RefitPlex();
    // Uncomment these two lines to get internal highlight / selection.
    q->SetPickable(1);
    q->SetAlwaysSecSelect(1);

    // Associate palette for track points:
    {
        evd::Application & app = goo::app<evd::Application>();
        app.associate_palette_with_tracking_detector(
                this, &(app.common_SADC_palette_updater()) );
    }

    return q;
}

TEveElement *
ECAL::_draw_ECAL( double width,
                  double height,
                  double depth,
                  double cellSideX,
                  double cellSideY ) {
    const size_t nCellsX = width/cellSideX,
                 nCellsY = height/cellSideY
                 ;
    # ifdef UNIFORM_INIT
    TRandom r(0);
    # endif
    gStyle->SetPalette(1, 0);
    TEveFrameBox* frm = new TEveFrameBox();
    frm->SetAABoxCenterHalfSize(0, 0, 0, width/2, height/2, depth/2);
    frm->SetFrameColor( kCyan );
    //frm->SetBackColorRGBA( 120, 120, 120, 255 );
    //frm->SetDrawBack(kTRUE);
    TEveBoxSet* q = new TEveBoxSet("ECAL");
    _boxSetECAL = q;
    q->SetPalette( &goo::app<evd::Application>().common_SADC_palette_updater().palette() );
    q->SetFrame(frm);
    //q->Reset(TEveBoxSet::kBT_AABoxFixedDim, kFALSE, 64);
    q->Reset(TEveBoxSet::kBT_AABox, kFALSE, 64);
    q->SetDefWidth( cellSideX );
    q->SetDefHeight( cellSideY );
    q->SetDefDepth( depth );
    {
        for( UByte i = 0; i < nCellsX; ++i ) {
            for( UByte j = 0; j < nCellsY; ++j ) {
                q->AddBox(
                        -width/2  + i*cellSideX,
                        -height/2 + j*cellSideY,
                        -depth/2,
                        cellWidth_mm,
                        cellHeight_mm,
                        depthECAL
                    );
                # ifdef UNIFORM_INIT
                q->DigitValue( r.Uniform(0, 255) );  // TODO: set to NULL?
                # else
                q->DigitValue( 0 );
                # endif
            }
        }
    }
    q->RefitPlex();
    //TEveTrans& t = q->RefMainTrans();
    //t.SetPos(x, y, z);  // TODO!
    // Uncomment these two lines to get internal highlight / selection.
    q->SetPickable(1);
    q->SetAlwaysSecSelect(1);
    return q;
}

void
ECAL::_V_draw_detector( DrawableDetector::Container * sc ) {
    _draw_preshower( width, height, depthPreshower,
                     cellWidth_mm, cellHeight_mm );
    TEveTrans & t1 = _boxSetPreshower->RefMainTrans();
    t1.SetPos( 0.,
               0.,
               0. - depthECAL/2
            );
    sc->AddElement( _boxSetPreshower );

    _draw_ECAL( width, height, depthECAL,
                cellWidth_mm, cellHeight_mm );
    TEveTrans & t2 = _boxSetECAL->RefMainTrans();
    t2.SetPos( 0.,
               0.,
               0. + depthPreshower/2
            );
    sc->AddElement( _boxSetECAL );

    _set_label( detector_name().c_str(),
                0., 1.2*height/2, 0. + depthPreshower/2 );

    {
        uint8_t goddamnCounter = 0;
        for( UByte i = 0; i < 6; ++i ) {
            for( UByte j = 0; j < 6; ++j, ++goddamnCounter ) {
                TEveDigitSet::DigitBase_t * dgtPtr;
                ECAL_Cell * t1 = new ECAL_Cell(
                        "", "",  // TODO
                        dgtPtr = _boxSetPreshower->GetDigit( goddamnCounter ),
                        &(_cHitCachePreshower.amplitudes[i][j]),
                        ((TEveBoxSet::BAABox_t *) dgtPtr)->fA,
                        ((TEveBoxSet::BAABox_t *) dgtPtr)->fB );

                ECAL_Cell * t2 = new ECAL_Cell(
                        "", "",  // TODO
                        dgtPtr = _boxSetECAL->GetDigit( goddamnCounter ),
                        &(_cHitCacheECAL.amplitudes[i][j]),
                        ((TEveBoxSet::BAABox_t *) dgtPtr)->fA,
                        ((TEveBoxSet::BAABox_t *) dgtPtr)->fB );

                {
                    AFR_UniqueDetectorID did(
                        compose_cell_identifier( EnumScope::d_ECAL0, i, j ) );
                    mutable_parts().emplace( did.wholenum, t1 );
                }
                {
                    AFR_UniqueDetectorID did(
                        compose_cell_identifier( EnumScope::d_ECAL1, i, j ) );
                    mutable_parts().emplace( did.wholenum, t2 );
                }
            }
        }
    }
}

void
ECAL::_update_point_in( bool isInPreshower, float x, float y, float edep ) {
    assert( _hasHitInPreshower || _hasHitInMainPart );

    if( x > 1. ) {          x =  1.;
    } else if( x < -1 ) {   x = -1.;}

    if( y > 1. ) {          y =  1.;
    } else if( y < -1. ) {  y = -1.;}

    if( std::isnan(_otherPartXCache) ) {
        _otherPartXCache = x;
        _otherPartYCache = y;
        _otherPartEDep = edep;
        return;  // wait for additional info
    } else {
        // Got additional info. Now, re-compute coordinates and
        // store them in _otherWhatever fields to push back them
        // into caches container further, at event treatment
        // finale.
        float relWeight;
        // z will now store E_{ECAL}/(E_{Preshower} + E_{ECAL})
        if( isInPreshower ) {
            // edep corresponds to preshower:
            relWeight = _lastHitCachedZ = _otherPartEDep/(edep + _otherPartEDep);
        } else {
            // edep corresponds to ECAL main part:
            _lastHitCachedZ = edep/(_otherPartEDep + edep);
            relWeight = 1 - _lastHitCachedZ;
        }
        // As relWeight now stores energy ratio, one can use it to determine
        // relative energy contribution of two parts. Most reliable
        // one is part yielding greater deposited energy.
        x += relWeight*_otherPartXCache;
        y += relWeight*_otherPartYCache;
        _otherPartXCache = x;
        _otherPartYCache = y;
        _otherPartEDep += edep;
    }
}

void
ECAL::_V_hit_setting_finale() {
    if( !(_hasHitInPreshower && _hasHitInMainPart) ) {
        if( _hasHitInPreshower ) {
            // set z to the front face of ECAL:
            _lastHitCachedZ = -1.;
        } else if( _hasHitInMainPart ) {
            // set z to the back face of ECAL:
            _lastHitCachedZ =  1.;
        } else {
            return;  // no hits in ECAL in this event.
        }
    }

    // since track receptive volume's XForm expects coordinates to be in range [0:1]
    _otherPartXCache = (_otherPartXCache+1)/2.;
    _otherPartYCache = (_otherPartYCache+1)/2.;
    alignment::TrackReceptiveVolume<3>::update_point( { 
        _otherPartEDep/(2*(nCellsX + nCellsY)*(nCellsX + nCellsY)),
        {_otherPartXCache, _otherPartYCache, _lastHitCachedZ} } );
}

float
ECAL::_cache_current_hit( bool doAveraging,
                          HitCache & cHitCache,
                          std::list<HitCache> & caches ) {
    // 0. Re-compute point:
    float sumX[6],
          sumY[6],
          sum = 0.,
          mwX, mwY
          ;
    for( unsigned char i = 0; i < 6; ++i ) {
        sumX[i] = sumY[i] = 0.;
        for( unsigned char j = 0; j < 6; ++j ) {
            sum += cHitCache.amplitudes[i][j];
            sumX[i] += cHitCache.amplitudes[i][j];
            sumY[i] += cHitCache.amplitudes[j][i];
        }
    }
    mwX = (        - sumX[0]/sum - (2/3.)*sumX[1]/sum - (1/3.)*sumX[2]/sum
            + (1/3.)*sumX[3]/sum + (2/3.)*sumX[4]/sum        + sumX[5]/sum );
    mwY = (        - sumY[0]/sum - (2/3.)*sumY[1]/sum - (1/3.)*sumY[2]/sum
            + (1/3.)*sumY[3]/sum + (2/3.)*sumY[4]/sum        + sumY[5]/sum );

    if( goo::app<evd::Application>().persistency() ) {
        _update_point_in( &_cHitCachePreshower == &cHitCache, mwX, mwY, sum );
    }

    float avMax = 0.;
    if( doAveraging ) {
        // 1. Push back last cache:
        caches.push_back( cHitCache );
        if(caches.size() < 2) {
            return avMax;
        }
        // 2. Calculate averaged values:
        size_t nEvents = 0;
        auto lastIt = caches.end();
        --lastIt;
        for( auto it = caches.begin(); it != lastIt &&
             nEvents < goo::app<sV::evd::Application>().persistency() ; ++it, ++nEvents ) {
            for( unsigned char i = 0; i < 6; ++i ) {
                for( unsigned char j = 0; j < 6; ++j ) {
                    cHitCache.amplitudes[i][j] += it->amplitudes[i][j];
                }
            }
        }
        if( nEvents ) {
            for( unsigned char i = 0; i < 6; ++i ) {
                for( unsigned char j = 0; j < 6; ++j ) {
                    cHitCache.amplitudes[i][j] /= nEvents;
                    if( cHitCache.amplitudes[i][j] < 0. ) {
                        sV_logw( "Negative averaged value on SADC %e: apparently "
                                     "due to bad pedestals calculation. Corrected to zero.\n",
                                     cHitCache.amplitudes[i][j] );
                        cHitCache.amplitudes[i][j] = 0.;
                    }
                    if( avMax < cHitCache.amplitudes[i][j] ) {
                        avMax = cHitCache.amplitudes[i][j];
                    }
                }
            }
        }
    } else {
        // 2. Push back last cache:
        if( caches.size() ) {  // TODO: wouldn't it be more reasonable to check persistency settings?
            caches.push_back( cHitCache );
        }
    }
    // 3. Erase front cached if need:
    int excess = caches.size() - goo::app<sV::evd::Application>().persistency();
    auto lastIt = caches.begin();
    std::advance( lastIt, excess );
    if( excess > 0 ) {
        caches.erase( caches.begin(), lastIt );
    }
    return avMax;
}

bool
ECAL::_V_draw_hit() {
    # if 1
    return false; // TODO
    # else
    //assert( EnumScope::fam_ECAL == detector_family_num( hit().detectorid() ) );
    bool wasDrawn = false;
    bool doDrawAveraged = goo::app<sV::evd::Application>().do_draw_averaged();

    for( uint8_t i = 0; i < hit().subgroup().summary_size(); ++i ) {
        const events::DetectorSummary & thisSummary = hit().subgroup().summary(i);
        AFR_UniqueDetectorID id(thisSummary.detectorid());

        # if 1  // TODO: support for ECALSUM
        if( EnumScope::d_ECALSUM == id.byNumber.major ) {
            sV_log3( "ECAL: ignoring unsupported ECALSUM (%d) value in hit.\n",
                        (int) id.byNumber.minor );
            continue;
        }
        # endif

        auto digitIt = _pmtMap.find( id.wholenum );
        if( _pmtMap.end() != digitIt ) {
            *(digitIt->second.cAmplitudePtr) = thisSummary.edep().amplitude();
            digitIt->second.digitPtr->fValue
                        = thisSummary.edep().amplitude();
            wasDrawn = true;
        } else {
            char detNameBf[64];
            snprintf_detector_name( detNameBf, sizeof(detNameBf), id );
            sV_loge( "ECAL: broken digits mapping! "
                         "Couldn't find digit for 0x%x (%x,%s:%x) detector.\n",
                         id.wholenum,
                         id.byNumber.major,
                         detNameBf,
                         id.byNumber.minor);
            continue;
        }
    }

    float avMax = std::numeric_limits<float>::quiet_NaN();
    if( _hasHitInPreshower ) {
        avMax = _cache_current_hit( doDrawAveraged,
                                    _cHitCachePreshower,
                                    _prevHitsPreshower );
    }
    if( _hasHitInMainPart ) {
        const float foundAverageMax = _cache_current_hit( doDrawAveraged,
                                                          _cHitCacheECAL,
                                                          _prevHitsECAL );
        if( std::isfinite(foundAverageMax) && (   avMax < foundAverageMax
                                               || !std::isfinite(avMax) ) ) {
            avMax = foundAverageMax;
        }
    }
    

    float valMax = hit().subgroup().maxdep().amplitude();
    if( std::isfinite(avMax) && avMax > valMax ) {
        valMax = avMax;
    }

    goo::app<evd::Application>().common_SADC_palette_updater().extend_to( valMax );

    if( doDrawAveraged ) {
        for( uint8_t i = 0; i < hit().subgroup().summary_size(); ++i ) {
            const events::DetectorSummary & thisSummary = hit().subgroup().summary(i);
            AFR_UniqueDetectorID id(thisSummary.detectorid());
            auto digitIt = _pmtMap.find( id.wholenum );

            # if 1  // TODO: support for ECALSUM
            if( EnumScope::d_ECALSUM == id.byNumber.major ) {
                sV_log3( "ECAL: ignoring unsupported ECALSUM (%d) value in hit.\n",
                            (int) id.byNumber.minor );
                continue;
            }
            # endif

            if( _pmtMap.end() == digitIt ) {
                char detNameBf[64];
                snprintf_detector_name( detNameBf, sizeof(detNameBf), id );
                sV_loge( "ECAL: broken digits mapping! "
                             "Couldn't find digit for 0x%x (%x,%s:%x) detector. "
                             "Averaged values won't be displayed.\n",
                             id.wholenum,
                             id.byNumber.major,
                             detNameBf,
                             id.byNumber.minor);
                continue;
            }
            if( goo::app<sV::evd::Application>().persistency() > 1 ) {
                if( !avMax || !std::isfinite(avMax) ) {
                    sV_logw( "Incorrect amplitudes averaged in ECAL representation "
                                 "--- bad averaged maximal value %e.\n", avMax );
                    avMax = goo::app<evd::Application>().common_SADC_palette_updater().palette().GetMaxVal();
                }
                // Active persistency while averaging means that we need to draw
                // several events besides of just averaged value, so one more
                // way of indication should be involved: box size, opacity, etc.
                TEveBoxSet::BAABox_t * box = (TEveBoxSet::BAABox_t *) digitIt->second.digitPtr;
                double relSize = *(digitIt->second.cAmplitudePtr)
                                       / avMax;
                if( relSize < 0. ) {
                    relSize = 0.;
                } else {
                    // todo: which one is better?
                    //relSize = sqrt( std::fabs(relSize) );
                    relSize *= relSize;
                }
                // missync!
                if( relSize > 1. ) {
                    sV_logw( "Bad relative size. Malformed picture expected.\n" );
                    continue;
                }
                box->fW = cellWidth_mm*relSize;
                box->fH = cellHeight_mm*relSize;
                box->fA = digitIt->second.originX + cellWidth_mm*(1 - relSize)/2;
                box->fB = digitIt->second.originY + cellHeight_mm*(1 - relSize)/2;
                //std::cout << "Rel. size: " << relSize << std::endl;  // XXX
                //box->fC =
            } else {
                digitIt->second.digitPtr->fValue
                            = *(digitIt->second.cAmplitudePtr);
            }
        }
    } else if( !std::isfinite(avMax) ) {
        sV_logw( "Incorrect amplitudes averaged in ECAL representation "
                     "--- non-finite averaged maximal value %e.\n", avMax );
    }

    if( wasDrawn ) {
        if( _hasHitInMainPart ) {
            _boxSetECAL->ElementChanged();
        }
    }
    return wasDrawn;
    # endif
}

# if 0
bool
ECAL::_V_treat_new_hit( const ReceptiveDetector::Hit & hitCRef ) {
    // Here we check, if hit to be treat has hits in preshower and main part
    // and thats it.
    for( uint8_t i = 0; i < hitCRef.payload().subgroup().summary_size()
                   && !(_hasHitInPreshower && _hasHitInMainPart); ++i ) {
        const events::DetectorSummary & thisSummary = hitCRef.payload().subgroup().summary(i);
        AFR_UniqueDetectorID id(thisSummary.detectorid());
        if( EnumScope::d_ECAL0 == id.byNumber.major ) {
            _hasHitInPreshower = true;
        } else if( EnumScope::d_ECAL1 == id.byNumber.major ) {
            _hasHitInMainPart = true;
        } else {
            sV_logw( "Detector code 0x%x of ECAL family couldn't be treated by "
                         "this representation.\n", (int) id.wholenum );
        }
    }
    return _hasHitInPreshower || _hasHitInMainPart;
}
# endif

bool
ECAL::_V_reset_summary() {
    bool digitsChanged = false;
    for( auto it = mutable_parts().begin();
            mutable_parts().end() != it; ++it ) {
        digitsChanged |= static_cast<ECAL_Cell*>(it->second)->reset();
    }
    _hasHitInPreshower = _hasHitInMainPart = false;
    bzero( &_cHitCachePreshower, sizeof(_cHitCachePreshower) );
    bzero( &_cHitCacheECAL,      sizeof(_cHitCacheECAL) );
    _otherPartXCache = _otherPartYCache
                     = _otherPartEDep
                     = std::numeric_limits<float>::quiet_NaN();
    _hasHitInPreshower = _hasHitInMainPart = false;
    open_new_set();
    return digitsChanged;
}

REGISTER_EVD_CONSTRUCTOR( ECAL, "ECAL" )

}  // namespace evd
}  // namespace sV

