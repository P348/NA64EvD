/*
 * Copyright (c) 2016 Renat R. Dusaev <crank@qcrypt.org>
 * Author: Renat R. Dusaev <crank@qcrypt.org>
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy of
 * this software and associated documentation files (the "Software"), to deal in
 * the Software without restriction, including without limitation the rights to
 * use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
 * the Software, and to permit persons to whom the Software is furnished to do so,
 * subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
 * FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
 * COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
 * IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
 * CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

# if 0
# include "detectors/box.hpp"
# include "alignment/DetCtrDict.hpp"
# include "goo_exception.hpp"

# include <TEveBox.h>
# include <TEveGeoShape.h>

namespace sV {
namespace evd {

void
Box::_V_draw_detector( DrawableDetector::Container * sc ) {
    const double sizeX_mm = 600,
                 sizeY_mm = 600,
                 sizeZ_mm = 30
                 ;

    _box = new TEveBox( detector_name().c_str(),
                        "Box detector representation." );

    _box->SetVertex(0, - sizeX_mm/2, - sizeY_mm/2, - sizeZ_mm/2);
    _box->SetVertex(1, - sizeX_mm/2,   sizeY_mm/2, - sizeZ_mm/2);
    _box->SetVertex(2,   sizeX_mm/2,   sizeY_mm/2, - sizeZ_mm/2);
    _box->SetVertex(3,   sizeX_mm/2, - sizeY_mm/2, - sizeZ_mm/2);
    _box->SetVertex(4, - sizeX_mm/2, - sizeY_mm/2, + sizeZ_mm/2);
    _box->SetVertex(5, - sizeX_mm/2,   sizeY_mm/2, + sizeZ_mm/2);
    _box->SetVertex(6,   sizeX_mm/2,   sizeY_mm/2, + sizeZ_mm/2);
    _box->SetVertex(7,   sizeX_mm/2, - sizeY_mm/2, + sizeZ_mm/2);

    sc->AddElement( _box );
    _set_label( detector_name().c_str(),
                0, 1.1*sizeY_mm/2, 0 );
}

bool
Box::_V_draw_hit() {
    _TODO_  // TODO
}

bool
Box::_V_treat_new_hit( const Hit & ) {
    _TODO_  // TODO
}

bool
Box::_V_reset_hits() {
    _TODO_  // TODO
}

REGISTER_EVD_CONSTRUCTOR( Box, "Box" )

}  // namespace evd
}  // namespace sV
# endif

