/*
 * Copyright (c) 2016 Renat R. Dusaev <crank@qcrypt.org>
 * Author: Renat R. Dusaev <crank@qcrypt.org>
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy of
 * this software and associated documentation files (the "Software"), to deal in
 * the Software without restriction, including without limitation the rights to
 * use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
 * the Software, and to permit persons to whom the Software is furnished to do so,
 * subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
 * FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
 * COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
 * IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
 * CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

# include "detectors/HCAL.hpp"

# if 0
# include "alignment/DetCtrDict.hpp"
# include "Application.hpp"

# include <goo_exception.hpp>

# include <TEveFrameBox.h>
# include <TEveManager.h>
# include <TEveBoxSet.h>
# include <TEvePointSet.h>
# include <TRandom.h>
# include <TStyle.h>
# include <TEveTrans.h>
# include <TEveText.h>
# include <TEveGeoShape.h>

namespace sV {
namespace evd {

# define UNIFORM_INIT 1 // XXX

void
HCAL::_V_draw_detector( DrawableDetector::Container * sc ) {
    const size_t nLayers = 48,
                 nCellsPerLayerX = 3,
                 nCellsPerLayerY = 3
                ;
    const double layerFeDepth_mm = 8,
                 layerScnDepth_mm = 25,
                 cellSizeX_mm = _cst_cellSizeX_mm = 200,
                 cellSizeY_mm = _cst_cellSizeY_mm = 200,
                 fullLayerDepth_mm = layerScnDepth_mm + layerFeDepth_mm,
                 width = _cst_width = nCellsPerLayerX*cellSizeX_mm,
                 height = _cst_height = nCellsPerLayerY*cellSizeY_mm,
                 depth = _cst_depth = nLayers*fullLayerDepth_mm
                ;

    # ifdef UNIFORM_INIT
    TRandom r(0);
    # endif
    gStyle->SetPalette(1, 0);
    TEveFrameBox* frm = new TEveFrameBox();
    frm->SetAABoxCenterHalfSize(0, 0, 0, width/2, height/2, depth/2);
    frm->SetFrameColor( kWhite );
    //frm->SetBackColorRGBA( 255, 255, 255, 255 );
    //frm->SetDrawBack(kTRUE);

    TEveBoxSet * q = new TEveBoxSet(detector_name().c_str());  // TODO: detector name
    _boxSetHCAL = q;
    q->SetPalette( &goo::app<evd::Application>().common_SADC_palette_updater().palette() );
    //q->SetEditMainTransparency( kTRUE );
    //q->SetMainTransparency( 80 );
    q->SetFrame(frm);
    //q->Reset(TEveBoxSet::kBT_AABoxFixedDim, kFALSE, 64);
    q->Reset(TEveBoxSet::kBT_AABox, kFALSE, 64);
    //q->SetDefWidth( cellSizeX_mm );
    //q->SetDefHeight( cellSizeY_mm );
    //q->SetDefDepth( depth );
    {
        for( UByte i = 0; i < nCellsPerLayerX; ++i ) {
            for( UByte j = 0; j < nCellsPerLayerY; ++j ) {
                q->AddBox(
                        -width/2  + i*cellSizeX_mm,
                        -height/2 + j*cellSizeY_mm,
                        -depth/2,
                        cellSizeX_mm,
                        cellSizeY_mm,
                        depth
                    );
                # ifdef UNIFORM_INIT
                q->DigitValue( r.Uniform(0, 255) );
                # else
                q->DigitValue( 0 );
                # endif
            }
        }
    }
    q->RefitPlex();

    _pointSetHCAL = new TEvePointSet(); {
        _pointSetHCAL->SetOwnIds( kTRUE );
        _pointSetHCAL->SetNextPoint( 0., 0., -_cst_depth/2 );
        //
        _pointSetHCAL->SetMarkerColor(2);
        _pointSetHCAL->SetMarkerSize(9);
        _pointSetHCAL->SetMarkerStyle(5);
        _boxSetHCAL->AddElement( _pointSetHCAL );
        //PointDigitCache t;
        //t.digitPtr = _pointSetHCAL->GetDigit(0);
        //_pointsCache.push_back( t );
    }

    // Uncomment these two lines to get internal highlight / selection.
    q->SetPickable(1);
    q->SetAlwaysSecSelect(1);
    sc->AddElement( _boxSetHCAL );

    _set_label( detector_name().c_str(), 0., 1.2*height/2, 0. );

    {
        uint8_t goddamnCounter = 0;
        for( UByte i = 0; i < nCellsPerLayerX; ++i ) {
            for( UByte j = 0; j < nCellsPerLayerY; ++j, ++goddamnCounter ) {
                DECLTYPE(_pmtMap)::mapped_type t;
                t.digitPtr = q->GetDigit( goddamnCounter );  // fuck this!
                t.cAmplitudePtr = &(_cHitCache.amplitudes[i][j]);
                t.originX = ((TEveBoxSet::BAABox_t *) t.digitPtr)->fA;
                t.originY = ((TEveBoxSet::BAABox_t *) t.digitPtr)->fB;
                AFR_UniqueDetectorID dummy( compose_cell_identifier( EnumScope::d_HCAL0, i, j ) );
                _pmtMap.emplace( dummy.byNumber.minor, t );
            }
        }
    }
}

float
HCAL::_cache_current_hit( bool doAveraging ) {
    // 0. Re-compute point:
    float avMax = 0.;
    float sumX[3],
          sumY[3],
          sum = 0.;
    for( unsigned char i = 0; i < 3; ++i ) {
        sumX[i] = sumY[i] = 0.;
        for( unsigned char j = 0; j < 3; ++j ) {
            sum += _cHitCache.amplitudes[i][j];
            sumX[i] += _cHitCache.amplitudes[i][j];
            sumY[i] += _cHitCache.amplitudes[j][i];
        }
    }
    //for( unsigned char i = 0; i < 3; ++i ) {
    _cHitCache.mwX = (-sumX[0]/sum /*+ sumX[1]/sum*/ + sumX[2]/sum);
    _cHitCache.mwY = (-sumY[0]/sum /*+ sumY[1]/sum*/ + sumY[2]/sum);
    //}

    // Set first point:
    {
        float * pXYZ = _pointSetHCAL->GetP();
        pXYZ[0] = _cHitCache.mwX*_cst_width/2;
        pXYZ[1] = _cHitCache.mwY*_cst_height/2;
    }

    if( doAveraging && _prevHits.size() > 2 ) {
        // 1. Push back last cache:
        _prevHits.push_back( _cHitCache );
        // 2. Calculate averaged values:
        size_t nEvents = 0;
        auto itLast = _prevHits.end();
        --itLast;
        for( auto it = _prevHits.begin(); it != itLast &&
             nEvents < goo::app<sV::evd::Application>().persistency() ; ++it, ++nEvents ) {
            for( unsigned char i = 0; i < 3; ++i ) {
                for( unsigned char j = 0; j < 3; ++j ) {
                    _cHitCache.amplitudes[i][j] += it->amplitudes[i][j];
                }
            }
        }
        for( unsigned char i = 0; i < 3; ++i ) {
            for( unsigned char j = 0; j < 3; ++j ) {
                _cHitCache.amplitudes[i][j] /= nEvents;
                if( avMax < _cHitCache.amplitudes[i][j] ) {
                    avMax = _cHitCache.amplitudes[i][j];
                }
            }
        }
    } else {
        // 2. Push back last cache:
        if( _prevHits.size() ) {
            _prevHits.push_back( _cHitCache );
        }
    }
    // 3. Erase front cached if need:
    int excess = _prevHits.size() - goo::app<sV::evd::Application>().persistency();
    auto lastIt = _prevHits.begin();
    std::advance( lastIt, excess );
    if( excess > 0 ) {
        _prevHits.erase( _prevHits.begin(), lastIt );
    }
    return avMax;
}

bool
HCAL::_V_draw_hit() {
    assert( EnumScope::fam_HCAL == detector_family_num( hit().detectorid() ) );
    bool wasDrawn = false;

    // Set all which is not react to zero.
    //for( auto it = _pmtMap.begin(); it != _pmtMap.end(); ++it ) {
    //    it->second.digitPtr->fValue = 0.;
    //}

    bool doDrawAveraged = goo::app<sV::evd::Application>().do_draw_averaged();

    for( uint8_t i = 0; i < hit().subgroup().summary_size(); ++i ) {
        const events::DetectorSummary & thisSummary = hit().subgroup().summary(i);
        AFR_UniqueDetectorID id(thisSummary.detectorid());
        auto digitIt = _pmtMap.find( id.byNumber.minor );
        if( _pmtMap.end() != digitIt ) {
            *(digitIt->second.cAmplitudePtr) = thisSummary.edep().amplitude();
            digitIt->second.digitPtr->fValue
                        = thisSummary.edep().amplitude();
            wasDrawn = true;
        } else {
            sV_loge( "HCAL --- broken digits mapping!\n" );
        }
    }

    const float avMax = _cache_current_hit( doDrawAveraged );
    float valMax = hit().subgroup().maxdep().amplitude();
    if( std::isfinite(avMax) && avMax > valMax ) {
        valMax = avMax;
    }

    goo::app<evd::Application>().common_SADC_palette_updater().extend_to( valMax );

    if( doDrawAveraged && std::isfinite(avMax) ) {
        for( uint8_t i = 0; i < hit().subgroup().summary_size(); ++i ) {
            const events::DetectorSummary & thisSummary = hit().subgroup().summary(i);
            AFR_UniqueDetectorID id(thisSummary.detectorid());
            auto digitIt = _pmtMap.find( id.byNumber.minor );
            if( goo::app<sV::evd::Application>().persistency() > 1 ) {
                // Active persistency while averaging means that we need to draw
                // several events besides of just averaged value, so one more
                // way of indication should be involved: box size, opacity, etc.
                TEveBoxSet::BAABox_t * box = (TEveBoxSet::BAABox_t *) digitIt->second.digitPtr;
                const double relSize = *(digitIt->second.cAmplitudePtr)
                                       /goo::app<evd::Application>().common_SADC_palette_updater().palette().GetMaxVal();
                box->fW = _cst_cellSizeX_mm*relSize;
                box->fH = _cst_cellSizeY_mm*relSize;
                box->fA = digitIt->second.originX + _cst_cellSizeX_mm*(1 - relSize)/2;
                box->fB = digitIt->second.originY + _cst_cellSizeY_mm*(1 - relSize)/2;
                //box->fC =
            } else {
                digitIt->second.digitPtr->fValue
                        = *(digitIt->second.cAmplitudePtr);
            }
        }
    }

    if( wasDrawn ) {
        _boxSetHCAL->ElementChanged();
        _pointSetHCAL->ElementChanged();
    }
    return wasDrawn;
}

bool
HCAL::_V_recache_summary( const PackedSummary & cs ) {
    return alignment
         ::CachedPayloadReceptiveDetector<na64::events::Measurement>
         ::_V_recache_summary(cs) ;
}

bool
HCAL::_V_reset_hits() {
    bool digitsChanged = alignment
         ::CachedPayloadReceptiveDetector<na64::events::Measurement>
         ::_V_reset_hits()
         ;
    for( auto it = _pmtMap.begin(); _pmtMap.end() != it; ++it ) {
        *(it->second.cAmplitudePtr) = 0.;
        if( it->second.digitPtr->fValue ) {
            it->second.digitPtr->fValue = 0.;
            digitsChanged |= true;
        }
    }
    bzero( &_cHitCache, sizeof(_cHitCache) );
    return digitsChanged;
}

REGISTER_EVD_CONSTRUCTOR( HCAL, "HCAL" )

}  // namespace evd
}  // namespace sV

# endif

