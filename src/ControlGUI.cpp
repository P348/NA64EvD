/*
 * Copyright (c) 2016 Renat R. Dusaev <crank@qcrypt.org>
 * Author: Renat R. Dusaev <crank@qcrypt.org>
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy of
 * this software and associated documentation files (the "Software"), to deal in
 * the Software without restriction, including without limitation the rights to
 * use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
 * the Software, and to permit persons to whom the Software is furnished to do so,
 * subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
 * FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
 * COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
 * IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
 * CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

# include <TGFrame.h>
# include <TGButton.h>
# include <TGNumberEntry.h>
# include <TGLabel.h>
# include <TGTextEntry.h>
# include <TGProgressBar.h>
# include <TEveManager.h>
# include <TGStatusBar.h>
# include <TTimer.h>

# include "Application.hpp"

# include "ControlGUI.hpp"

ControlGUI::ControlGUI( TGMainFrame * frm ) :
            _updatingTimer(nullptr),
            _picPlayPause{ gClient->GetPicture("../../p348g4/assets/images/icons/play.png"),
                           gClient->GetPicture("../../p348g4/assets/images/icons/pause.png") } {
    _layout_all( frm );
}

ControlGUI::~ControlGUI() {
}

void
ControlGUI::_layout_all( TGMainFrame * frm ) {
    sV_log1( "Control GUI layout ran.\n" );
    _frmVWrapperPtr = new TGVerticalFrame( frm );
    _frmVWrapperPtr->SetName("frmControlWidgetsWrapper"); {
        // Buffer occupancy indicator
        _gfrmHBufferOccupancy = new TGGroupFrame(_frmVWrapperPtr, "Buffer Occupancy" );
        _frmHBufferOccupancy = new TGHorizontalFrame( _gfrmHBufferOccupancy ); {
            _pbarBufferOccupancyPtr = new TGHProgressBar( _frmHBufferOccupancy, TGProgressBar::kFancy, 50 );
            _pbarBufferOccupancyPtr->ShowPosition( kTRUE, kFALSE, "%4.f" );
            _pbarBufferOccupancyPtr->SetRange(0, 100);
            _pbarBufferOccupancyPtr->SetPosition(50);
            _frmHBufferOccupancy->AddFrame( _pbarBufferOccupancyPtr,
                            new TGLayoutHints(kLHintsExpandX | kLHintsExpandY, 5, 5, 5, 5) );
        } _gfrmHBufferOccupancy->AddFrame( _frmHBufferOccupancy,
                                           new TGLayoutHints(kLHintsExpandX | kLHintsExpandY, 5, 5, 5, 5) );
        // Display group
        _gfrmDisplayControlPtr = new TGGroupFrame(_frmVWrapperPtr, "Display Settings");
        _frmVDisplayControlPtr = new TGVerticalFrame( _gfrmDisplayControlPtr );
        _layout_event_display_widgets();
        //_gfrmDisplayControlPtr->SetLayoutManager(new TGVerticalLayout(_gfrmDisplayControlPtr));
        _gfrmDisplayControlPtr->AddFrame( _frmVDisplayControlPtr,
                                          new TGLayoutHints(kLHintsExpandX | kLHintsExpandY, 5, 5, 5, 5) );

        // Playback group:)
        _gfrmPlaybackControlPtr = new TGGroupFrame(_frmVWrapperPtr, "Playback Control");
        _frmVPlayback = new TGVerticalFrame( _gfrmPlaybackControlPtr );
        _layout_playback_widgets();
        _gfrmPlaybackControlPtr->AddFrame( _frmVPlayback,
                                           new TGLayoutHints(kLHintsExpandX | kLHintsExpandY, 5, 5, 5, 5) );

        // Connection group
        _gfrmConnectionControlPtr = new TGGroupFrame( _frmVWrapperPtr, "Connection Control" );
        _frmVConnectionControlPtr = new TGVerticalFrame( _gfrmConnectionControlPtr );
        _layout_connection_widgets();
        _gfrmConnectionControlPtr->AddFrame( _frmVConnectionControlPtr,
                                             new TGLayoutHints(kLHintsExpandX | kLHintsExpandY, 5, 5, 5, 5) );

    }
    _frmVWrapperPtr->AddFrame( _gfrmHBufferOccupancy,     new TGLayoutHints(kLHintsExpandX) );
    _frmVWrapperPtr->AddFrame( _gfrmDisplayControlPtr,    new TGLayoutHints(kLHintsExpandX) );
    _frmVWrapperPtr->AddFrame( _gfrmPlaybackControlPtr,   new TGLayoutHints(kLHintsExpandX) );
    _frmVWrapperPtr->AddFrame( _gfrmConnectionControlPtr, new TGLayoutHints(kLHintsExpandX) );
    frm->AddFrame( _frmVWrapperPtr, new TGLayoutHints(kLHintsExpandX | kLHintsExpandY) );
}

void
ControlGUI::_layout_playback_widgets() {
    // Playback:
    _frmHPlaybackButtons = new TGHorizontalFrame( _frmVPlayback, -1, -1, kHorizontalFrame | kSunkenFrame ); {
        // Button displaying previous event:
        _btnPrevious = new TGPictureButton( _frmHPlaybackButtons,
                                            gClient->GetPicture("../../p348g4/assets/images/icons/roll_one_frame_back.png") );
        _frmHPlaybackButtons->AddFrame( _btnPrevious, new TGLayoutHints(kLHintsExpandX, 5, 0, 5, 5) );
        // Button play/pause:
        _btnPlayPause = new TGPictureButton( _frmHPlaybackButtons,
                                             _picPlayPause[0] );
        _btnPlayPause->Connect( "Clicked()", "ControlGUI",
                                    this, "toggle_play_pause()" );
        _frmHPlaybackButtons->AddFrame( _btnPlayPause, new TGLayoutHints(kLHintsExpandX, 0, 0, 5, 5) );
        // Button displaying last event:
        _btnNext = new TGPictureButton( _frmHPlaybackButtons,
                                        gClient->GetPicture("../../p348g4/assets/images/icons/roll_one_frame_forward.png") );
        _frmHPlaybackButtons->AddFrame( _btnNext, new TGLayoutHints(kLHintsExpandX, 0, 5, 5, 5) );
    } _frmVPlayback->AddFrame( _frmHPlaybackButtons, new TGLayoutHints(kLHintsExpandX, 5, 5, 5, 5) );
    // Minimal delay settings:
    _frmHMinimalDelay = new TGHorizontalFrame( _frmVPlayback ); {
        // Label:
        _lblDelayPtr = new TGLabel( _frmHMinimalDelay, "Min. delay, 0.1 sec: " );
        _frmHMinimalDelay->AddFrame( _lblDelayPtr );
        // Control widget:
        _nfldMinDelay = new TGNumberEntry(_frmHMinimalDelay,
                        (Double_t) 0, 8, -1,
                        TGNumberFormat::kNESReal,
                        TGNumberFormat::kNEAPositive,
                        TGNumberFormat::kNELLimitMinMax,
                        0, 600 );
        _nfldMinDelay->Connect( "ValueSet(Long_t)", "ControlGUI",
                                 this, "change_periodical_update_timing(Long_t)" );
        _frmHMinimalDelay->AddFrame( _nfldMinDelay );
    } _frmVPlayback->AddFrame( _frmHMinimalDelay );
}

void
ControlGUI::_layout_event_display_widgets() {
    _frmHPersistency = new TGHorizontalFrame( _frmVDisplayControlPtr ); {
        // Persistency check button:
        _cbtnEnablePersistancyPtr = new TGCheckButton(_frmHPersistency, "Persistency: ");
        _frmHPersistency->AddFrame( _cbtnEnablePersistancyPtr );
        // Persistency number:
        _nfldPersistency = new TGNumberEntry(_frmHPersistency,
                        (Double_t) 0, 8, -1,
                        TGNumberFormat::kNESReal,
                        TGNumberFormat::kNEAPositive,
                        TGNumberFormat::kNELLimitMinMax,
                        0, 100 );
        _nfldPersistency->Connect( "ValueSet(Long_t)", "ControlGUI",
                                 this, "update_persistency_settings(Long_t)" );
        _frmHPersistency->AddFrame( _nfldPersistency );
    } _frmVDisplayControlPtr->AddFrame( _frmHPersistency );
    // Do draw averaged:
    _cbtnDoDrawAveragedPtr = new TGCheckButton( _frmVDisplayControlPtr, "Draw averaged values" );
    _frmVDisplayControlPtr->AddFrame( _cbtnDoDrawAveragedPtr );
    // Do draw tracks:
    _cbtnDoDrawTracksPtr = new TGCheckButton( _frmVDisplayControlPtr, "Draw tracks" );
    _frmVDisplayControlPtr->AddFrame( _cbtnDoDrawTracksPtr );
}

void
ControlGUI::_layout_connection_widgets() {
    // Target host:
    _frmHTHostPtr = new TGHorizontalFrame( _frmVConnectionControlPtr ); {
        // Label
        _lblHostPtr = new TGLabel( _frmHTHostPtr, "Host: " );
        //_lblHostPtr->Resize(88, _lblHostPtr->GetDefaultHeight());
        _frmHTHostPtr->AddFrame( _lblHostPtr, new TGLayoutHints(kLHintsExpandX | kLHintsLeft, 2, 2, 2, 2) );
        // Field
        _inpHostAddrPtr = new TGTextEntry( _frmHTHostPtr  );
        _inpHostAddrPtr->SetMaxLength( 16 );
        _frmHTHostPtr->AddFrame( _inpHostAddrPtr );
    } _frmVConnectionControlPtr->AddFrame(_frmHTHostPtr,
                        new TGLayoutHints(kLHintsExpandX | kLHintsLeft, 3, 3, 3, 3));
    // Multicast port:
    _frmHPortPtr = new TGHorizontalFrame( _frmVConnectionControlPtr ); {
        // Label
        _lblPortPtr = new TGLabel( _frmHPortPtr, "Port: " );
        _frmHPortPtr->AddFrame( _lblPortPtr, new TGLayoutHints(kLHintsExpandX | kLHintsLeft, 2, 2, 2, 2) );
        // Field
        _nfldPort = new TGNumberEntry( _frmHPortPtr,
                        (Double_t) 0, 8, -1,
                        TGNumberFormat::kNESReal,
                        TGNumberFormat::kNEAPositive,
                        TGNumberFormat::kNELLimitMinMax,
                        0, 1e5 );
        _frmHPortPtr->AddFrame( _nfldPort );
    } _frmVConnectionControlPtr->AddFrame(_frmHPortPtr,
                        new TGLayoutHints(kLHintsExpandX | kLHintsLeft, 3, 3, 3, 3));
    // Multicast address:
    _frmHMulticastPtr = new TGHorizontalFrame( _frmVConnectionControlPtr ); {
        // Label
        _lblMCastPtr = new TGLabel( _frmHMulticastPtr, "MCast: " );
        _frmHMulticastPtr->AddFrame( _lblMCastPtr, new TGLayoutHints(kLHintsExpandX | kLHintsLeft, 2, 2, 2, 2) );
        // Field
        _inpMCastAddrPtr = new TGTextEntry( _frmHMulticastPtr );
        _frmHMulticastPtr->AddFrame( _inpMCastAddrPtr );
    } _frmVConnectionControlPtr->AddFrame(_frmHMulticastPtr,
                        new TGLayoutHints(kLHintsExpandX | kLHintsLeft, 3, 3, 3, 3));
    // Connection status and button:
    _frmHConnect = new TGHorizontalFrame( _frmVConnectionControlPtr ); {
        // button:
        _btnConnect = new TGTextButton( _frmHConnect, "Disconne&ct" );
        _btnConnect->Connect( "Clicked()", "ControlGUI", this, "toggle_connection()" );
        _frmHConnect->AddFrame( _btnConnect );
        // status label:
        _lblConnectionStatus = new TGLabel( _frmHConnect, "disconnected" );
        _frmHConnect->AddFrame( _lblConnectionStatus,
                        new TGLayoutHints(kLHintsExpandX | kLHintsLeft, 2, 2, 2, 2) );
    } _frmVConnectionControlPtr->AddFrame( _frmHConnect,
                        new TGLayoutHints(kLHintsExpandX | kLHintsLeft, 3, 3, 3, 3));
}

void
ControlGUI::set_buffer_capacity_range( size_t n ) {
    _pbarBufferOccupancyPtr->SetRange(0, n);
}

void
ControlGUI::set_initial_state(
            size_t bufferOccupancy,
            bool persistencyOn,
            unsigned short nPersistency,
            bool doDrawAveraged,
            bool doDrawTracks,
            unsigned short minDelayNSec,
            std::string defaultHost,
            int portNo,
            std::string mCastAddr
        ) {

    _pbarBufferOccupancyPtr->SetPosition( bufferOccupancy );

    _cbtnEnablePersistancyPtr->SetOn( (persistencyOn ? kTRUE : kFALSE), kFALSE );
    _nfldPersistency->SetNumber( nPersistency );
    _cbtnDoDrawAveragedPtr->SetOn( (doDrawAveraged ? kTRUE : kFALSE), kFALSE );
    _cbtnDoDrawTracksPtr->SetOn( (doDrawTracks ? kTRUE : kFALSE), kFALSE );

    _nfldMinDelay->SetNumber( minDelayNSec );

    _inpHostAddrPtr->SetText( defaultHost.c_str() );
    _nfldPort->SetNumber( portNo );
    _inpMCastAddrPtr->SetText( mCastAddr.c_str() );
}

// Updating timer
////////////////

void
ControlGUI::start_periodical_update( Long_t mSecDelay ) {
    if( !_updatingTimer ) {
        _updatingTimer = new TTimer();
        _updatingTimer->Connect( "Timeout()", "ControlGUI",
                                 this, "update_displaying_events()" );
    }
    _updatingTimer->Start( mSecDelay, kFALSE );
}

void
ControlGUI::stop_periodical_update() {
    if( _updatingTimer ) {
        _updatingTimer->Stop();
    }
}

void
ControlGUI::change_periodical_update_timing( Long_t mSecDelay ) {
    // Note: I was unable to make ROOT's signal/slot mechanics
    // submet here actual value, so here is workaround:
    if( _updatingTimer ) {
        //if( !mSecDelay ) {
            mSecDelay = min_delay_msec();
        //}
        //sV_log1( "Setting timer to: %ld.\n", mSecDelay );  // XXX
        _updatingTimer->SetTime( mSecDelay );
    }
}

Long_t
ControlGUI::min_delay_msec() {
    return _nfldMinDelay->GetNumber()*1e2;
}

//// //// //// //// //// //// //// //// //// //// //// //// //// //// //// ////
//// //// //// //// //// //// //// //// //// //// //// //// //// //// //// ////
// Drivers
/////////

void
ControlGUI::set_status_label( const char * str, unsigned char n ) {
    if( n > 1 ) { n = 1; }
    //_lblConnectionStatus->SetText( str ? str : "NULL" );
    gEve->GetStatusBar()->SetText(str ? str : "NULL", n);
}

void
ControlGUI::set_buffer_occupancy( size_t n ) {
    _pbarBufferOccupancyPtr->SetPosition(n);
}

//// //// //// //// //// //// //// //// //// //// //// //// //// //// //// ////

void
ControlGUI::playback_control_enable() {
    //_gfrmPlaybackControlPtr->SetEditable( kTRUE );  // doesn't work
    _btnPlayPause->SetEnabled( kTRUE );
    playback_previous_enable();
    playback_next_enable();
    _nfldMinDelay->SetState( kTRUE );
}

void
ControlGUI::playback_control_disable() {
    //_gfrmPlaybackControlPtr->SetEditable( kFALSE );
    _btnPlayPause->SetEnabled( kFALSE );
    playback_previous_disable();
    playback_next_disable();
    _nfldMinDelay->SetState( kFALSE );
}

void
ControlGUI::playback_previous_enable() {
    _btnPrevious->SetEnabled( kTRUE );
}

void
ControlGUI::playback_previous_disable() {
    _btnPrevious->SetEnabled( kFALSE );
}

void
ControlGUI::playback_next_enable() {
    _btnNext->SetEnabled( kTRUE );
}

void
ControlGUI::playback_next_disable() {
    _btnNext->SetEnabled( kFALSE );
}

void
ControlGUI::playback_play_button_set_picture_pause() {
    _btnPlayPause->SetPicture( _picPlayPause[1] );
}

void
ControlGUI::playback_play_button_set_picture_play() {
    _btnPlayPause->SetPicture( _picPlayPause[0] );
}


//// //// //// //// //// //// //// //// //// //// //// //// //// //// //// ////

void
ControlGUI::connection_control_enable() {
    //_gfrmConnectionControlPtr->SetEditable( kTRUE );
    connection_control_settings_enable();
    _btnConnect->SetEnabled( kTRUE );
}

void
ControlGUI::connection_control_disable() {
    //_gfrmConnectionControlPtr->SetEditable( kFALSE );
    connection_control_settings_disable();
    _btnConnect->SetEnabled( kFALSE );
}

void
ControlGUI::connection_control_settings_enable() {
    _inpHostAddrPtr->SetState( kTRUE );
    _inpMCastAddrPtr->SetState( kTRUE );
    _nfldPort->SetState( kTRUE );
}

void
ControlGUI::connection_control_settings_disable() {
    _inpHostAddrPtr->SetState( kFALSE );
    _inpMCastAddrPtr->SetState( kFALSE );
    _nfldPort->SetState( kFALSE );
}

void
ControlGUI::connection_control_set_status_label( const char * txt ) {
    _lblConnectionStatus->SetText( txt );
}

void
ControlGUI::connection_control_set_connect_button_label( const char * txt ) {
    _btnConnect->SetText( txt );
}


//// //// //// //// //// //// //// //// //// //// //// //// //// //// //// ////
//// //// //// //// //// //// //// //// //// //// //// //// //// //// //// ////
// Reactions
///////////

std::string
ControlGUI::connection_host_address() const {
    return _inpHostAddrPtr->GetText();
}

std::string
ControlGUI::connection_multicast_address() const {
    return _inpMCastAddrPtr->GetText();
}

int
ControlGUI::connection_port_no() const {
    return _nfldPort->GetNumber();
}

bool
ControlGUI::do_draw_averaged() const {
    return _cbtnDoDrawAveragedPtr->IsDown();
}

size_t
ControlGUI::persistency() const {
    if( _cbtnEnablePersistancyPtr->IsDown() ) {
        return _nfldPersistency->GetNumber();
    } else {
        return 0;
    }
}

//

void
ControlGUI::toggle_connection() {
    if( goo::app<sV::evd::Application>().is_connected() ) {
        goo::app<sV::evd::Application>().disconnect();
    } else {
        goo::app<sV::evd::Application>().connect();
    }
    /*
    std::string hostAddr = _inpHostAddrPtr->GetText(),
            mCastAddr = _inpMCastAddrPtr->GetText()
            ;
    int portNo = _nfldPort->GetNumber();
    sV_log3( "Trying to resolve connection to %s:%d/%s.\n",
                hostAddr.c_str(),
                portNo,
                mCastAddr.c_str()
            );
    goo::app<sV::evd::Application>()
        .receiver().connect( hostAddr, portNo, mCastAddr );
    */
}

void
ControlGUI::update_persistency_settings( Long_t ) {
    goo::app<sV::evd::Application>().persistency( persistency() );
}

void
ControlGUI::update_displaying_events() {
    // this method is supposed to be triggered by timer, but can be quite
    // time consuming as it tries to deplete events queue until something
    // representative will be appeared to be drawn, so it is may be needed
    // to be protect it from repeatative invokations.
    sV_log3( "ControlGUI::update_displaying_events() triggered.\n" );
    sV::evd::Application & app = goo::app<sV::evd::Application>();
    if( app.has_queued_events() ) {
        app.emplace_new_events();
    } /*else {
        // todo: display something to user to ensure him we're
        // still alive?
        
    }*/
    else {
        // Since this method is usually invoked inside a major TApplication
        // event-treatment loop, we can not use here
        // app.receiver().pool().wait_for_events();  --
        // it will block everything. Instead, we're using here this crutch:
        if( !min_delay_msec() ) {
            // Just to avoid 100% CPU usage when there is nothing to draw.
            boost::this_thread::sleep(boost::posix_time::milliseconds(10));
        }
    }
    
    set_buffer_capacity_range( app.receiver().buffer_capacitance() );
    set_buffer_occupancy( app.receiver().buffer_occupancy() );
}

void
ControlGUI::toggle_play_pause() {
    if( goo::app<sV::evd::Application>().is_playing() ) {
        goo::app<sV::evd::Application>().pause();
    } else {
        goo::app<sV::evd::Application>().play();
    }
    //start_periodical_update( min_delay_msec() );
}

